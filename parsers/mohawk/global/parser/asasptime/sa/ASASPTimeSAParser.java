// Generated from D:\Work\Masters\workspace (GradSchool)\Mohawk-T Globals\parsers\mohawk\global\parser\asasptime\sa\ASASPTimeSA.g4 by ANTLR 4.4

package mohawk.global.parser.asasptime.sa;

import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.*;

import org.apache.commons.lang3.StringUtils;
import mohawk.global.pieces.*;
import mohawk.global.helper.*;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class ASASPTimeSAParser extends Parser {
    static {
        RuntimeMetaData.checkVersion("4.4", RuntimeMetaData.VERSION);
    }

    protected static final DFA[] _decisionToDFA;
    protected static final PredictionContextCache _sharedContextCache = new PredictionContextCache();
    public static final int T__0 = 1, TRUE = 2, CONFIG = 3, GOAL = 4, CANASSIGN = 5, CANREVOKE = 6, NEG = 7, SEP = 8,
            SEP2 = 9, SPACE = 10, INT = 11, WS = 12, NL = 13;
    public static final String[] tokenNames = {"<INVALID>", "'t'", "'true'", "'CONFIG'", "'GOAL'", "'can_assign'",
            "'can_revoke'", "'-'", "SEP", "SEP2", "SPACE", "INT", "WS", "NL"};
    public static final int RULE_init = 0, RULE_config = 1, RULE_query = 2, RULE_myrule = 3, RULE_canassign = 4,
            RULE_canrevoke = 5, RULE_rolecondition = 6, RULE_myrole = 7, RULE_timeslot = 8, RULE_precondition = 9;
    public static final String[] ruleNames = {"init", "config", "query", "myrule", "canassign", "canrevoke",
            "rolecondition", "myrole", "timeslot", "precondition"};

    @Override
    public String getGrammarFileName() {
        return "ASASPTimeSA.g4";
    }

    @Override
    public String[] getTokenNames() {
        return tokenNames;
    }

    @Override
    public String[] getRuleNames() {
        return ruleNames;
    }

    @Override
    public String getSerializedATN() {
        return _serializedATN;
    }

    @Override
    public ATN getATN() {
        return _ATN;
    }

    public final Logger logger = Logger.getLogger("mohawk");

    /* Global States */
    int tabsize = 1;
    public MohawkT mohawkT = new MohawkT("Mohawk Reverse Converter: ASAPTime SA");
    public Integer claimedRoles = null;

    private void logmsg(String msg) {
        if (logger.getLevel() == Level.FINE) {
            System.out.println(StringUtils.repeat("  ", tabsize) + msg);
        }
    }

    public ASASPTimeSAParser(TokenStream input) {
        super(input);
        _interp = new ParserATNSimulator(this, _ATN, _decisionToDFA, _sharedContextCache);
    }
    public static class InitContext extends ParserRuleContext {
        public List<MyruleContext> myrule() {
            return getRuleContexts(MyruleContext.class);
        }
        public QueryContext query() {
            return getRuleContext(QueryContext.class, 0);
        }
        public MyruleContext myrule(int i) {
            return getRuleContext(MyruleContext.class, i);
        }
        public ConfigContext config() {
            return getRuleContext(ConfigContext.class, 0);
        }
        public InitContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }
        @Override
        public int getRuleIndex() {
            return RULE_init;
        }
        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof ASASPTimeSAListener) ((ASASPTimeSAListener) listener).enterInit(this);
        }
        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof ASASPTimeSAListener) ((ASASPTimeSAListener) listener).exitInit(this);
        }
    }

    public final InitContext init() throws RecognitionException {
        InitContext _localctx = new InitContext(_ctx, getState());
        enterRule(_localctx, 0, RULE_init);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(20);
                config();
                setState(21);
                query();
                setState(23);
                _errHandler.sync(this);
                _la = _input.LA(1);
                do {
                    {
                        {
                            setState(22);
                            myrule();
                        }
                    }
                    setState(25);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                } while (_la == CANASSIGN || _la == CANREVOKE);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ConfigContext extends ParserRuleContext {
        public Token r;
        public Token t;
        public TerminalNode INT(int i) {
            return getToken(ASASPTimeSAParser.INT, i);
        }
        public TerminalNode SPACE(int i) {
            return getToken(ASASPTimeSAParser.SPACE, i);
        }
        public TerminalNode CONFIG() {
            return getToken(ASASPTimeSAParser.CONFIG, 0);
        }
        public List<TerminalNode> SPACE() {
            return getTokens(ASASPTimeSAParser.SPACE);
        }
        public List<TerminalNode> INT() {
            return getTokens(ASASPTimeSAParser.INT);
        }
        public ConfigContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }
        @Override
        public int getRuleIndex() {
            return RULE_config;
        }
        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof ASASPTimeSAListener) ((ASASPTimeSAListener) listener).enterConfig(this);
        }
        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof ASASPTimeSAListener) ((ASASPTimeSAListener) listener).exitConfig(this);
        }
    }

    public final ConfigContext config() throws RecognitionException {
        ConfigContext _localctx = new ConfigContext(_ctx, getState());
        enterRule(_localctx, 2, RULE_config);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(27);
                match(CONFIG);
                setState(28);
                match(SPACE);
                setState(29);
                ((ConfigContext) _localctx).r = match(INT);
                setState(30);
                match(SPACE);
                setState(31);
                ((ConfigContext) _localctx).t = match(INT);

                claimedRoles = new Integer(
                        (((ConfigContext) _localctx).r != null ? ((ConfigContext) _localctx).r.getText() : null));

            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class QueryContext extends ParserRuleContext {
        public Token role_ref;
        public Token timeslot_ref;
        public TerminalNode INT(int i) {
            return getToken(ASASPTimeSAParser.INT, i);
        }
        public TerminalNode SPACE(int i) {
            return getToken(ASASPTimeSAParser.SPACE, i);
        }
        public TerminalNode GOAL() {
            return getToken(ASASPTimeSAParser.GOAL, 0);
        }
        public List<TerminalNode> SPACE() {
            return getTokens(ASASPTimeSAParser.SPACE);
        }
        public List<TerminalNode> INT() {
            return getTokens(ASASPTimeSAParser.INT);
        }
        public QueryContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }
        @Override
        public int getRuleIndex() {
            return RULE_query;
        }
        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof ASASPTimeSAListener) ((ASASPTimeSAListener) listener).enterQuery(this);
        }
        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof ASASPTimeSAListener) ((ASASPTimeSAListener) listener).exitQuery(this);
        }
    }

    public final QueryContext query() throws RecognitionException {
        QueryContext _localctx = new QueryContext(_ctx, getState());
        enterRule(_localctx, 4, RULE_query);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(34);
                match(GOAL);
                setState(35);
                match(SPACE);
                setState(36);
                ((QueryContext) _localctx).role_ref = match(INT);
                setState(37);
                match(SPACE);
                setState(38);
                ((QueryContext) _localctx).timeslot_ref = match(INT);

                mohawkT.query._timeslot = new TimeSlot(new Integer((((QueryContext) _localctx).timeslot_ref != null
                        ? ((QueryContext) _localctx).timeslot_ref.getText()
                        : null)));
                mohawkT.query._roles.add(new Role("role" + (((QueryContext) _localctx).role_ref != null
                        ? ((QueryContext) _localctx).role_ref.getText()
                        : null)));

            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class MyruleContext extends ParserRuleContext {
        public CanrevokeContext canrevoke() {
            return getRuleContext(CanrevokeContext.class, 0);
        }
        public CanassignContext canassign() {
            return getRuleContext(CanassignContext.class, 0);
        }
        public MyruleContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }
        @Override
        public int getRuleIndex() {
            return RULE_myrule;
        }
        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof ASASPTimeSAListener) ((ASASPTimeSAListener) listener).enterMyrule(this);
        }
        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof ASASPTimeSAListener) ((ASASPTimeSAListener) listener).exitMyrule(this);
        }
    }

    public final MyruleContext myrule() throws RecognitionException {
        MyruleContext _localctx = new MyruleContext(_ctx, getState());
        enterRule(_localctx, 6, RULE_myrule);
        try {
            setState(43);
            switch (_input.LA(1)) {
                case CANASSIGN :
                    enterOuterAlt(_localctx, 1); {
                    setState(41);
                    canassign();
                }
                    break;
                case CANREVOKE :
                    enterOuterAlt(_localctx, 2); {
                    setState(42);
                    canrevoke();
                }
                    break;
                default :
                    throw new NoViableAltException(this);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class CanassignContext extends ParserRuleContext {
        public TimeslotContext atimeslot_ref;
        public PreconditionContext precond;
        public TimeslotContext timeslot_ref;
        public MyroleContext role_ref;
        public PreconditionContext precondition() {
            return getRuleContext(PreconditionContext.class, 0);
        }
        public List<TerminalNode> SEP() {
            return getTokens(ASASPTimeSAParser.SEP);
        }
        public TerminalNode CANASSIGN() {
            return getToken(ASASPTimeSAParser.CANASSIGN, 0);
        }
        public MyroleContext myrole() {
            return getRuleContext(MyroleContext.class, 0);
        }
        public TerminalNode SEP2() {
            return getToken(ASASPTimeSAParser.SEP2, 0);
        }
        public TimeslotContext timeslot(int i) {
            return getRuleContext(TimeslotContext.class, i);
        }
        public TerminalNode SPACE() {
            return getToken(ASASPTimeSAParser.SPACE, 0);
        }
        public TerminalNode SEP(int i) {
            return getToken(ASASPTimeSAParser.SEP, i);
        }
        public List<TimeslotContext> timeslot() {
            return getRuleContexts(TimeslotContext.class);
        }
        public CanassignContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }
        @Override
        public int getRuleIndex() {
            return RULE_canassign;
        }
        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof ASASPTimeSAListener) ((ASASPTimeSAListener) listener).enterCanassign(this);
        }
        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof ASASPTimeSAListener) ((ASASPTimeSAListener) listener).exitCanassign(this);
        }
    }

    public final CanassignContext canassign() throws RecognitionException {
        CanassignContext _localctx = new CanassignContext(_ctx, getState());
        enterRule(_localctx, 8, RULE_canassign);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(45);
                match(CANASSIGN);
                setState(46);
                match(SPACE);
                setState(47);
                ((CanassignContext) _localctx).atimeslot_ref = timeslot();
                setState(48);
                match(SEP);
                setState(49);
                ((CanassignContext) _localctx).precond = precondition();
                setState(50);
                match(SEP2);
                setState(51);
                ((CanassignContext) _localctx).timeslot_ref = timeslot();
                setState(52);
                match(SEP);
                setState(53);
                ((CanassignContext) _localctx).role_ref = myrole();

                Rule r = new Rule();
                r._type = RuleType.ASSIGN;
                r._adminRole = Role.allRoles();
                r._adminTimeInterval = ((CanassignContext) _localctx).atimeslot_ref.ts;
                r._preconditions = ((CanassignContext) _localctx).precond.p;
                r._roleSchedule.add(((CanassignContext) _localctx).timeslot_ref.ts);
                r._role = ((CanassignContext) _localctx).role_ref.role;

                mohawkT.canAssign.addRule(r);

            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class CanrevokeContext extends ParserRuleContext {
        public TimeslotContext atimeslot_ref;
        public PreconditionContext precond;
        public TimeslotContext timeslot_ref;
        public MyroleContext role_ref;
        public PreconditionContext precondition() {
            return getRuleContext(PreconditionContext.class, 0);
        }
        public List<TerminalNode> SEP() {
            return getTokens(ASASPTimeSAParser.SEP);
        }
        public MyroleContext myrole() {
            return getRuleContext(MyroleContext.class, 0);
        }
        public TerminalNode SEP2() {
            return getToken(ASASPTimeSAParser.SEP2, 0);
        }
        public TimeslotContext timeslot(int i) {
            return getRuleContext(TimeslotContext.class, i);
        }
        public TerminalNode SPACE() {
            return getToken(ASASPTimeSAParser.SPACE, 0);
        }
        public TerminalNode SEP(int i) {
            return getToken(ASASPTimeSAParser.SEP, i);
        }
        public TerminalNode CANREVOKE() {
            return getToken(ASASPTimeSAParser.CANREVOKE, 0);
        }
        public List<TimeslotContext> timeslot() {
            return getRuleContexts(TimeslotContext.class);
        }
        public CanrevokeContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }
        @Override
        public int getRuleIndex() {
            return RULE_canrevoke;
        }
        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof ASASPTimeSAListener) ((ASASPTimeSAListener) listener).enterCanrevoke(this);
        }
        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof ASASPTimeSAListener) ((ASASPTimeSAListener) listener).exitCanrevoke(this);
        }
    }

    public final CanrevokeContext canrevoke() throws RecognitionException {
        CanrevokeContext _localctx = new CanrevokeContext(_ctx, getState());
        enterRule(_localctx, 10, RULE_canrevoke);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(56);
                match(CANREVOKE);
                setState(57);
                match(SPACE);
                setState(58);
                ((CanrevokeContext) _localctx).atimeslot_ref = timeslot();
                setState(59);
                match(SEP);
                setState(60);
                ((CanrevokeContext) _localctx).precond = precondition();
                setState(61);
                match(SEP2);
                setState(62);
                ((CanrevokeContext) _localctx).timeslot_ref = timeslot();
                setState(63);
                match(SEP);
                setState(64);
                ((CanrevokeContext) _localctx).role_ref = myrole();

                Rule r = new Rule();
                r._type = RuleType.REVOKE;
                r._adminRole = Role.allRoles();
                r._adminTimeInterval = ((CanrevokeContext) _localctx).atimeslot_ref.ts;
                r._preconditions = ((CanrevokeContext) _localctx).precond.p;
                r._roleSchedule.add(((CanrevokeContext) _localctx).timeslot_ref.ts);
                r._role = ((CanrevokeContext) _localctx).role_ref.role;

                mohawkT.canRevoke.addRule(r);

            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class RoleconditionContext extends ParserRuleContext {
        public Role role;
        public MyroleContext r;
        public TerminalNode NEG() {
            return getToken(ASASPTimeSAParser.NEG, 0);
        }
        public MyroleContext myrole() {
            return getRuleContext(MyroleContext.class, 0);
        }
        public RoleconditionContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }
        @Override
        public int getRuleIndex() {
            return RULE_rolecondition;
        }
        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof ASASPTimeSAListener) ((ASASPTimeSAListener) listener).enterRolecondition(this);
        }
        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof ASASPTimeSAListener) ((ASASPTimeSAListener) listener).exitRolecondition(this);
        }
    }

    public final RoleconditionContext rolecondition() throws RecognitionException {
        RoleconditionContext _localctx = new RoleconditionContext(_ctx, getState());
        enterRule(_localctx, 12, RULE_rolecondition);

        boolean not = false;

        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(69);
                _la = _input.LA(1);
                if (_la == NEG) {
                    {
                        setState(67);
                        match(NEG);
                        not = true;
                    }
                }

                setState(71);
                ((RoleconditionContext) _localctx).r = myrole();

                ((RoleconditionContext) _localctx).role = new Role(((RoleconditionContext) _localctx).r.role);
                _localctx.role._not = not;

            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class MyroleContext extends ParserRuleContext {
        public Role role;
        public Token a;
        public TerminalNode INT() {
            return getToken(ASASPTimeSAParser.INT, 0);
        }
        public MyroleContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }
        @Override
        public int getRuleIndex() {
            return RULE_myrole;
        }
        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof ASASPTimeSAListener) ((ASASPTimeSAListener) listener).enterMyrole(this);
        }
        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof ASASPTimeSAListener) ((ASASPTimeSAListener) listener).exitMyrole(this);
        }
    }

    public final MyroleContext myrole() throws RecognitionException {
        MyroleContext _localctx = new MyroleContext(_ctx, getState());
        enterRule(_localctx, 14, RULE_myrole);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(74);
                ((MyroleContext) _localctx).a = match(INT);

                ((MyroleContext) _localctx).role = new Role("role"
                        + (((MyroleContext) _localctx).a != null ? ((MyroleContext) _localctx).a.getText() : null));
                mohawkT.roleHelper.add(_localctx.role);

            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class TimeslotContext extends ParserRuleContext {
        public TimeSlot ts;
        public Token t;
        public TerminalNode INT() {
            return getToken(ASASPTimeSAParser.INT, 0);
        }
        public TimeslotContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }
        @Override
        public int getRuleIndex() {
            return RULE_timeslot;
        }
        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof ASASPTimeSAListener) ((ASASPTimeSAListener) listener).enterTimeslot(this);
        }
        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof ASASPTimeSAListener) ((ASASPTimeSAListener) listener).exitTimeslot(this);
        }
    }

    public final TimeslotContext timeslot() throws RecognitionException {
        TimeslotContext _localctx = new TimeslotContext(_ctx, getState());
        enterRule(_localctx, 16, RULE_timeslot);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(77);
                match(T__0);
                setState(78);
                ((TimeslotContext) _localctx).t = match(INT);

                ((TimeslotContext) _localctx).ts = new TimeSlot(new Integer(
                        (((TimeslotContext) _localctx).t != null ? ((TimeslotContext) _localctx).t.getText() : null)));
                mohawkT.timeIntervalHelper.add(_localctx.ts);

            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class PreconditionContext extends ParserRuleContext {
        public ArrayList<Role> p;
        public RoleconditionContext a;
        public RoleconditionContext b;
        public List<RoleconditionContext> rolecondition() {
            return getRuleContexts(RoleconditionContext.class);
        }
        public RoleconditionContext rolecondition(int i) {
            return getRuleContext(RoleconditionContext.class, i);
        }
        public TerminalNode TRUE() {
            return getToken(ASASPTimeSAParser.TRUE, 0);
        }
        public TerminalNode SPACE(int i) {
            return getToken(ASASPTimeSAParser.SPACE, i);
        }
        public List<TerminalNode> SPACE() {
            return getTokens(ASASPTimeSAParser.SPACE);
        }
        public PreconditionContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }
        @Override
        public int getRuleIndex() {
            return RULE_precondition;
        }
        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof ASASPTimeSAListener) ((ASASPTimeSAListener) listener).enterPrecondition(this);
        }
        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof ASASPTimeSAListener) ((ASASPTimeSAListener) listener).exitPrecondition(this);
        }
    }

    public final PreconditionContext precondition() throws RecognitionException {
        PreconditionContext _localctx = new PreconditionContext(_ctx, getState());
        enterRule(_localctx, 18, RULE_precondition);

        ((PreconditionContext) _localctx).p = new ArrayList<Role>();

        int _la;
        try {
            setState(93);
            switch (_input.LA(1)) {
                case NEG :
                case INT :
                    enterOuterAlt(_localctx, 1); {
                    setState(81);
                    ((PreconditionContext) _localctx).a = rolecondition();

                    _localctx.p.add(((PreconditionContext) _localctx).a.role);

                    setState(89);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                    while (_la == SPACE) {
                        {
                            {
                                setState(83);
                                match(SPACE);
                                setState(84);
                                ((PreconditionContext) _localctx).b = rolecondition();

                                _localctx.p.add(((PreconditionContext) _localctx).b.role);

                            }
                        }
                        setState(91);
                        _errHandler.sync(this);
                        _la = _input.LA(1);
                    }
                }
                    break;
                case TRUE :
                    enterOuterAlt(_localctx, 2); {
                    setState(92);
                    match(TRUE);
                }
                    break;
                default :
                    throw new NoViableAltException(this);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static final String _serializedATN = "\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\17b\4\2\t\2\4\3\t"
            + "\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\3"
            + "\2\3\2\3\2\6\2\32\n\2\r\2\16\2\33\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\4\3\4"
            + "\3\4\3\4\3\4\3\4\3\4\3\5\3\5\5\5.\n\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6"
            + "\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\b\3\b\5\bH"
            + "\n\b\3\b\3\b\3\b\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13"
            + "\3\13\7\13Z\n\13\f\13\16\13]\13\13\3\13\5\13`\n\13\3\13\2\2\f\2\4\6\b"
            + "\n\f\16\20\22\24\2\2\\\2\26\3\2\2\2\4\35\3\2\2\2\6$\3\2\2\2\b-\3\2\2\2"
            + "\n/\3\2\2\2\f:\3\2\2\2\16G\3\2\2\2\20L\3\2\2\2\22O\3\2\2\2\24_\3\2\2\2"
            + "\26\27\5\4\3\2\27\31\5\6\4\2\30\32\5\b\5\2\31\30\3\2\2\2\32\33\3\2\2\2"
            + "\33\31\3\2\2\2\33\34\3\2\2\2\34\3\3\2\2\2\35\36\7\5\2\2\36\37\7\f\2\2"
            + "\37 \7\r\2\2 !\7\f\2\2!\"\7\r\2\2\"#\b\3\1\2#\5\3\2\2\2$%\7\6\2\2%&\7"
            + "\f\2\2&\'\7\r\2\2\'(\7\f\2\2()\7\r\2\2)*\b\4\1\2*\7\3\2\2\2+.\5\n\6\2"
            + ",.\5\f\7\2-+\3\2\2\2-,\3\2\2\2.\t\3\2\2\2/\60\7\7\2\2\60\61\7\f\2\2\61"
            + "\62\5\22\n\2\62\63\7\n\2\2\63\64\5\24\13\2\64\65\7\13\2\2\65\66\5\22\n"
            + "\2\66\67\7\n\2\2\678\5\20\t\289\b\6\1\29\13\3\2\2\2:;\7\b\2\2;<\7\f\2"
            + "\2<=\5\22\n\2=>\7\n\2\2>?\5\24\13\2?@\7\13\2\2@A\5\22\n\2AB\7\n\2\2BC"
            + "\5\20\t\2CD\b\7\1\2D\r\3\2\2\2EF\7\t\2\2FH\b\b\1\2GE\3\2\2\2GH\3\2\2\2"
            + "HI\3\2\2\2IJ\5\20\t\2JK\b\b\1\2K\17\3\2\2\2LM\7\r\2\2MN\b\t\1\2N\21\3"
            + "\2\2\2OP\7\3\2\2PQ\7\r\2\2QR\b\n\1\2R\23\3\2\2\2ST\5\16\b\2T[\b\13\1\2"
            + "UV\7\f\2\2VW\5\16\b\2WX\b\13\1\2XZ\3\2\2\2YU\3\2\2\2Z]\3\2\2\2[Y\3\2\2"
            + "\2[\\\3\2\2\2\\`\3\2\2\2][\3\2\2\2^`\7\4\2\2_S\3\2\2\2_^\3\2\2\2`\25\3" + "\2\2\2\7\33-G[_";
    public static final ATN _ATN = new ATNDeserializer().deserialize(_serializedATN.toCharArray());
    static {
        _decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
        for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
            _decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
        }
    }
}
