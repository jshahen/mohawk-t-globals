grammar ASASPTimeSA;

/* Code */
@header {
package mohawk.global.parser.asasptime.sa;

import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.*;

import org.apache.commons.lang3.StringUtils;
import mohawk.global.pieces.*;
import mohawk.global.helper.*;
}

@members {
	public final Logger logger = Logger.getLogger("mohawk");
	
	/* Global States */
	int tabsize = 1;
	public MohawkT mohawkT = new MohawkT("Mohawk Reverse Converter: ASAPTime SA");
	public Integer claimedRoles = null;
	
	
	private void logmsg(String msg) {
		if(logger.getLevel() == Level.FINE) {
			System.out.println(StringUtils.repeat("  ", tabsize) + msg);
		}
	}
}

/* Rules */
init
:
	config query myrule+
;

/* Can ignore the config line */
config
:
	CONFIG SPACE r=INT SPACE t=INT 
	{
		claimedRoles = new Integer($r.text);
	}
;

query
:
	GOAL SPACE role_ref = INT SPACE timeslot_ref = INT
	{
		mohawkT.query._timeslot = new TimeSlot(new Integer($timeslot_ref.text));
		mohawkT.query._roles.add(new Role("role" + $role_ref.text));
	}
;

myrule
:
	canassign
	| canrevoke
;

canassign
:
	CANASSIGN SPACE atimeslot_ref = timeslot SEP
	precond=precondition SEP2 timeslot_ref = timeslot SEP role_ref = myrole
	{
		Rule r = new Rule();
		r._type = RuleType.ASSIGN;
		r._adminRole = Role.allRoles();
		r._adminTimeInterval = $atimeslot_ref.ts;
		r._preconditions = $precond.p;
		r._roleSchedule.add($timeslot_ref.ts);
		r._role = $role_ref.role;
		
		mohawkT.canAssign.addRule(r);
	}

;

canrevoke
:
	CANREVOKE SPACE atimeslot_ref = timeslot SEP
	precond=precondition SEP2 timeslot_ref = timeslot SEP role_ref = myrole
	{
		Rule r = new Rule();
		r._type = RuleType.REVOKE;
		r._adminRole = Role.allRoles();
		r._adminTimeInterval = $atimeslot_ref.ts;
		r._preconditions = $precond.p;
		r._roleSchedule.add($timeslot_ref.ts);
		r._role = $role_ref.role;
		
		mohawkT.canRevoke.addRule(r);
	}
;

rolecondition returns [Role role] @init {
	boolean not = false;
}
:
	(
		NEG
		{not = true;}

	)? r = myrole
	{
		$role = new Role($r.role);
		$role._not = not;
	}

;

myrole returns [Role role]
:
	a = INT
	{
		$role = new Role("role" + $a.text);
		mohawkT.roleHelper.add($role);
	}

;

timeslot returns [TimeSlot ts]
:
	't' t=INT 
	{
		$ts = new TimeSlot(new Integer($t.text));
		mohawkT.timeIntervalHelper.add($ts);
	}
;

precondition returns [ArrayList<Role> p] @init {
	$p = new ArrayList<Role>();
}
:
	a=rolecondition 
	{
		$p.add($a.role);
	}
	(
		SPACE b=rolecondition
		{
			$p.add($b.role);
		}
	)*
	| TRUE
;

/* TOKENS */
TRUE
:
	'true'
;

CONFIG
:
	'CONFIG'
;

GOAL
:
	'GOAL'
;

CANASSIGN
:
	'can_assign'
;

CANREVOKE
:
	'can_revoke'
;

NEG
:
	'-'
;

SEP
:
	SPACE ',' SPACE
;

SEP2
:
	SPACE ';' SPACE
;

SPACE
:
	[ ]+
;

INT
:
	DIGIT+
;

fragment
DIGIT
:
	[0-9]
;

WS
:
	[\t\f]+ -> skip
;

NL
:
	'\r'? '\n' -> skip
;