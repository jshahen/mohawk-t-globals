grammar ASASPTimeNSA;

/* Code */
@header {
package mohawk.global.parser.asasptime.nsa;

import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.*;

import org.apache.commons.lang3.StringUtils;
import mohawk.global.pieces.*;
import mohawk.global.helper.*;
}

@members {
	public final Logger logger = Logger.getLogger("mohawk");
	
	/* Global States */
	int tabsize = 1;
	public MohawkT mohawkT = new MohawkT("Mohawk Reverse Converter: ASAPTime NSA");
	public Integer claimedRoles = null;
	
	
	private void logmsg(String msg) {
		if(logger.getLevel() == Level.FINE) {
			System.out.println(StringUtils.repeat("  ", tabsize) + msg);
		}
	}
}

/* Rules */
init
:
	config query myrule+
;

/* Can ignore the config line */
config
:
	CONFIG SPACE r=INT SPACE t=INT 
	{
		claimedRoles = new Integer($r.text);
	}
;

query
:
	GOAL SPACE role_ref = INT SPACE timeslot_ref = INT
	{
		mohawkT.query._timeslot = new TimeSlot(new Integer($timeslot_ref.text));
		mohawkT.query._roles.add(new Role("role" + $role_ref.text));
	}

;

myrule
:
	canassign
	| canrevoke
	| canenable
	| candisable
;

canassign
:
	CANASSIGN SPACE arole_ref = myroleA SEP atimeslot_ref = timeslot SEP precond =
	precondition SEP2 timeslot_ref = timeslot SEP role_ref = myrole
	{
		Rule r = new Rule();
		r._type = RuleType.ASSIGN;
		r._adminRole = $arole_ref.role;
		r._adminTimeInterval = $atimeslot_ref.ts;
		r._preconditions = $precond.p;
		r._roleSchedule.add($timeslot_ref.ts);
		r._role = $role_ref.role;
		
		mohawkT.canAssign.addRule(r);
	}

;

canrevoke
:
	CANREVOKE SPACE arole_ref = myroleA SEP atimeslot_ref = timeslot SEP precond =
	precondition SEP2 timeslot_ref = timeslot SEP role_ref = myrole
	{
		Rule r = new Rule();
		r._type = RuleType.REVOKE;
		r._adminRole = $arole_ref.role;
		r._adminTimeInterval = $atimeslot_ref.ts;
		r._preconditions = $precond.p;
		r._roleSchedule.add($timeslot_ref.ts);
		r._role = $role_ref.role;
		
		mohawkT.canRevoke.addRule(r);
	}

;

canenable
:
	CANENABLE SPACE arole_ref = myroleA SEP atimeslot_ref = timeslot SEP precond =
	precondition SEP2 timeslot_ref = timeslot SEP role_ref = myrole
	{
		Rule r = new Rule();
		r._type = RuleType.ENABLE;
		r._adminRole = $arole_ref.role;
		r._adminTimeInterval = $atimeslot_ref.ts;
		r._preconditions = $precond.p;
		r._roleSchedule.add($timeslot_ref.ts);
		r._role = $role_ref.role;
		
		mohawkT.canEnable.addRule(r);
	}

;

candisable
:
	CANDISABLE SPACE arole_ref = myroleA SEP atimeslot_ref = timeslot SEP precond
	= precondition SEP2 timeslot_ref = timeslot SEP role_ref = myrole
	{
		Rule r = new Rule();
		r._type = RuleType.DISABLE;
		r._adminRole = $arole_ref.role;
		r._adminTimeInterval = $atimeslot_ref.ts;
		r._preconditions = $precond.p;
		r._roleSchedule.add($timeslot_ref.ts);
		r._role = $role_ref.role;
		
		mohawkT.canDisable.addRule(r);
	}

;

rolecondition returns [Role role] @init {
	boolean not = false;
}
:
	(
		NEG
		{not = true;}

	)? r = myrole
	{
		$role = new Role($r.role);
		$role._not = not;
	}

;

myrole returns [Role role]
:
	a = INT
	{
		$role = new Role("role" + $a.text);
		mohawkT.roleHelper.add($role);
	}

;

myroleA returns [Role role]
:
	a = myrole
	{
		$role = $a.role;
	}

	| TRUE
	{
		$role = Role.allRoles();
	}

;

timeslot returns [TimeSlot ts]
:
	't' t = INT
	{
		$ts = new TimeSlot(new Integer($t.text));
		mohawkT.timeIntervalHelper.add($ts);
	}

;

precondition returns [ArrayList<Role> p] @init {
	$p = new ArrayList<Role>();
}
:
	a = rolecondition
	{
		$p.add($a.role);
	}

	(
		SPACE b = rolecondition
		{
			$p.add($b.role);
		}

	)*
	| TRUE
;

/* TOKENS */
TRUE
:
	'true'
;

CONFIG
:
	'CONFIG'
;

GOAL
:
	'GOAL'
;

CANASSIGN
:
	'can_assign'
;

CANREVOKE
:
	'can_revoke'
;

CANENABLE
:
	'can_enable'
;

CANDISABLE
:
	'can_disable'
;

NEG
:
	'-'
;

SEP
:
	SPACE ',' SPACE
;

SEP2
:
	SPACE ';' SPACE
;


SPACE
:
	[ ]+
;

INT
:
	DIGIT+
;

fragment
DIGIT
:
	[0-9]
;

WS
:
	[\t\f]+ -> skip
;

NL
:
	'\r'? '\n' -> skip
;