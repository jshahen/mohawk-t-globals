// Generated from D:\Work\Masters\workspace (GradSchool)\Mohawk-T Globals\parsers\mohawk\global\parser\asasptime\nsa\ASASPTimeNSA.g4 by ANTLR 4.4

package mohawk.global.parser.asasptime.nsa;

import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.*;

import org.apache.commons.lang3.StringUtils;
import mohawk.global.pieces.*;
import mohawk.global.helper.*;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;

/** This class provides an empty implementation of {@link ASASPTimeNSAListener},
 * which can be extended to create a listener which only needs to handle a subset
 * of the available methods. */
public class ASASPTimeNSABaseListener implements ASASPTimeNSAListener {
    /** {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
    */
    @Override
    public void enterInit(@NotNull ASASPTimeNSAParser.InitContext ctx) {}
    /** {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
    */
    @Override
    public void exitInit(@NotNull ASASPTimeNSAParser.InitContext ctx) {}
    /** {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
    */
    @Override
    public void enterMyroleA(@NotNull ASASPTimeNSAParser.MyroleAContext ctx) {}
    /** {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
    */
    @Override
    public void exitMyroleA(@NotNull ASASPTimeNSAParser.MyroleAContext ctx) {}
    /** {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
    */
    @Override
    public void enterQuery(@NotNull ASASPTimeNSAParser.QueryContext ctx) {}
    /** {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
    */
    @Override
    public void exitQuery(@NotNull ASASPTimeNSAParser.QueryContext ctx) {}
    /** {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
    */
    @Override
    public void enterPrecondition(@NotNull ASASPTimeNSAParser.PreconditionContext ctx) {}
    /** {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
    */
    @Override
    public void exitPrecondition(@NotNull ASASPTimeNSAParser.PreconditionContext ctx) {}
    /** {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
    */
    @Override
    public void enterCandisable(@NotNull ASASPTimeNSAParser.CandisableContext ctx) {}
    /** {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
    */
    @Override
    public void exitCandisable(@NotNull ASASPTimeNSAParser.CandisableContext ctx) {}
    /** {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
    */
    @Override
    public void enterCanrevoke(@NotNull ASASPTimeNSAParser.CanrevokeContext ctx) {}
    /** {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
    */
    @Override
    public void exitCanrevoke(@NotNull ASASPTimeNSAParser.CanrevokeContext ctx) {}
    /** {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
    */
    @Override
    public void enterMyrule(@NotNull ASASPTimeNSAParser.MyruleContext ctx) {}
    /** {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
    */
    @Override
    public void exitMyrule(@NotNull ASASPTimeNSAParser.MyruleContext ctx) {}
    /** {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
    */
    @Override
    public void enterTimeslot(@NotNull ASASPTimeNSAParser.TimeslotContext ctx) {}
    /** {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
    */
    @Override
    public void exitTimeslot(@NotNull ASASPTimeNSAParser.TimeslotContext ctx) {}
    /** {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
    */
    @Override
    public void enterCanenable(@NotNull ASASPTimeNSAParser.CanenableContext ctx) {}
    /** {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
    */
    @Override
    public void exitCanenable(@NotNull ASASPTimeNSAParser.CanenableContext ctx) {}
    /** {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
    */
    @Override
    public void enterCanassign(@NotNull ASASPTimeNSAParser.CanassignContext ctx) {}
    /** {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
    */
    @Override
    public void exitCanassign(@NotNull ASASPTimeNSAParser.CanassignContext ctx) {}
    /** {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
    */
    @Override
    public void enterRolecondition(@NotNull ASASPTimeNSAParser.RoleconditionContext ctx) {}
    /** {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
    */
    @Override
    public void exitRolecondition(@NotNull ASASPTimeNSAParser.RoleconditionContext ctx) {}
    /** {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
    */
    @Override
    public void enterMyrole(@NotNull ASASPTimeNSAParser.MyroleContext ctx) {}
    /** {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
    */
    @Override
    public void exitMyrole(@NotNull ASASPTimeNSAParser.MyroleContext ctx) {}
    /** {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
    */
    @Override
    public void enterConfig(@NotNull ASASPTimeNSAParser.ConfigContext ctx) {}
    /** {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
    */
    @Override
    public void exitConfig(@NotNull ASASPTimeNSAParser.ConfigContext ctx) {}

    /** {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
    */
    @Override
    public void enterEveryRule(@NotNull ParserRuleContext ctx) {}
    /** {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
    */
    @Override
    public void exitEveryRule(@NotNull ParserRuleContext ctx) {}
    /** {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
    */
    @Override
    public void visitTerminal(@NotNull TerminalNode node) {}
    /** {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
    */
    @Override
    public void visitErrorNode(@NotNull ErrorNode node) {}
}
