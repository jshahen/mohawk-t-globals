// Generated from D:\Work\Masters\workspace (GradSchool)\Mohawk-T Globals\parsers\mohawk\global\parser\asasptime\nsa\ASASPTimeNSA.g4 by ANTLR 4.4

package mohawk.global.parser.asasptime.nsa;

import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.*;

import org.apache.commons.lang3.StringUtils;
import mohawk.global.pieces.*;
import mohawk.global.helper.*;

import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class ASASPTimeNSALexer extends Lexer {
    static {
        RuntimeMetaData.checkVersion("4.4", RuntimeMetaData.VERSION);
    }

    protected static final DFA[] _decisionToDFA;
    protected static final PredictionContextCache _sharedContextCache = new PredictionContextCache();
    public static final int T__0 = 1, TRUE = 2, CONFIG = 3, GOAL = 4, CANASSIGN = 5, CANREVOKE = 6, CANENABLE = 7,
            CANDISABLE = 8, NEG = 9, SEP = 10, SEP2 = 11, SPACE = 12, INT = 13, WS = 14, NL = 15;
    public static String[] modeNames = {"DEFAULT_MODE"};

    public static final String[] tokenNames = {"'\\u0000'", "'\\u0001'", "'\\u0002'", "'\\u0003'", "'\\u0004'",
            "'\\u0005'", "'\\u0006'", "'\\u0007'", "'\b'", "'\t'", "'\n'", "'\\u000B'", "'\f'", "'\r'", "'\\u000E'",
            "'\\u000F'"};
    public static final String[] ruleNames = {"T__0", "TRUE", "CONFIG", "GOAL", "CANASSIGN", "CANREVOKE", "CANENABLE",
            "CANDISABLE", "NEG", "SEP", "SEP2", "SPACE", "INT", "DIGIT", "WS", "NL"};

    public final Logger logger = Logger.getLogger("mohawk");

    /* Global States */
    int tabsize = 1;
    public MohawkT mohawkT = new MohawkT("Mohawk Reverse Converter: ASAPTime NSA");
    public Integer claimedRoles = null;

    private void logmsg(String msg) {
        if (logger.getLevel() == Level.FINE) {
            System.out.println(StringUtils.repeat("  ", tabsize) + msg);
        }
    }

    public ASASPTimeNSALexer(CharStream input) {
        super(input);
        _interp = new LexerATNSimulator(this, _ATN, _decisionToDFA, _sharedContextCache);
    }

    @Override
    public String getGrammarFileName() {
        return "ASASPTimeNSA.g4";
    }

    @Override
    public String[] getTokenNames() {
        return tokenNames;
    }

    @Override
    public String[] getRuleNames() {
        return ruleNames;
    }

    @Override
    public String getSerializedATN() {
        return _serializedATN;
    }

    @Override
    public String[] getModeNames() {
        return modeNames;
    }

    @Override
    public ATN getATN() {
        return _ATN;
    }

    public static final String _serializedATN = "\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\21\u0087\b\1\4\2"
            + "\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"
            + "\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\3\2\3"
            + "\2\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5"
            + "\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3"
            + "\7\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\t\3\t"
            + "\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\13\3\13\3\13\3\13\3"
            + "\f\3\f\3\f\3\f\3\r\6\ro\n\r\r\r\16\rp\3\16\6\16t\n\16\r\16\16\16u\3\17"
            + "\3\17\3\20\6\20{\n\20\r\20\16\20|\3\20\3\20\3\21\5\21\u0082\n\21\3\21"
            + "\3\21\3\21\3\21\2\2\22\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27"
            + "\r\31\16\33\17\35\2\37\20!\21\3\2\5\3\2\"\"\3\2\62;\4\2\13\13\16\16\u0089"
            + "\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2"
            + "\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2"
            + "\2\31\3\2\2\2\2\33\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\3#\3\2\2\2\5%\3\2\2"
            + "\2\7*\3\2\2\2\t\61\3\2\2\2\13\66\3\2\2\2\rA\3\2\2\2\17L\3\2\2\2\21W\3"
            + "\2\2\2\23c\3\2\2\2\25e\3\2\2\2\27i\3\2\2\2\31n\3\2\2\2\33s\3\2\2\2\35"
            + "w\3\2\2\2\37z\3\2\2\2!\u0081\3\2\2\2#$\7v\2\2$\4\3\2\2\2%&\7v\2\2&\'\7"
            + "t\2\2\'(\7w\2\2()\7g\2\2)\6\3\2\2\2*+\7E\2\2+,\7Q\2\2,-\7P\2\2-.\7H\2"
            + "\2./\7K\2\2/\60\7I\2\2\60\b\3\2\2\2\61\62\7I\2\2\62\63\7Q\2\2\63\64\7"
            + "C\2\2\64\65\7N\2\2\65\n\3\2\2\2\66\67\7e\2\2\678\7c\2\289\7p\2\29:\7a"
            + "\2\2:;\7c\2\2;<\7u\2\2<=\7u\2\2=>\7k\2\2>?\7i\2\2?@\7p\2\2@\f\3\2\2\2"
            + "AB\7e\2\2BC\7c\2\2CD\7p\2\2DE\7a\2\2EF\7t\2\2FG\7g\2\2GH\7x\2\2HI\7q\2"
            + "\2IJ\7m\2\2JK\7g\2\2K\16\3\2\2\2LM\7e\2\2MN\7c\2\2NO\7p\2\2OP\7a\2\2P"
            + "Q\7g\2\2QR\7p\2\2RS\7c\2\2ST\7d\2\2TU\7n\2\2UV\7g\2\2V\20\3\2\2\2WX\7"
            + "e\2\2XY\7c\2\2YZ\7p\2\2Z[\7a\2\2[\\\7f\2\2\\]\7k\2\2]^\7u\2\2^_\7c\2\2"
            + "_`\7d\2\2`a\7n\2\2ab\7g\2\2b\22\3\2\2\2cd\7/\2\2d\24\3\2\2\2ef\5\31\r"
            + "\2fg\7.\2\2gh\5\31\r\2h\26\3\2\2\2ij\5\31\r\2jk\7=\2\2kl\5\31\r\2l\30"
            + "\3\2\2\2mo\t\2\2\2nm\3\2\2\2op\3\2\2\2pn\3\2\2\2pq\3\2\2\2q\32\3\2\2\2"
            + "rt\5\35\17\2sr\3\2\2\2tu\3\2\2\2us\3\2\2\2uv\3\2\2\2v\34\3\2\2\2wx\t\3"
            + "\2\2x\36\3\2\2\2y{\t\4\2\2zy\3\2\2\2{|\3\2\2\2|z\3\2\2\2|}\3\2\2\2}~\3"
            + "\2\2\2~\177\b\20\2\2\177 \3\2\2\2\u0080\u0082\7\17\2\2\u0081\u0080\3\2"
            + "\2\2\u0081\u0082\3\2\2\2\u0082\u0083\3\2\2\2\u0083\u0084\7\f\2\2\u0084"
            + "\u0085\3\2\2\2\u0085\u0086\b\21\2\2\u0086\"\3\2\2\2\7\2pu|\u0081\3\b\2" + "\2";
    public static final ATN _ATN = new ATNDeserializer().deserialize(_serializedATN.toCharArray());
    static {
        _decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
        for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
            _decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
        }
    }
}
