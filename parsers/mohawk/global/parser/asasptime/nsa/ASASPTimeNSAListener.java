// Generated from D:\Work\Masters\workspace (GradSchool)\Mohawk-T Globals\parsers\mohawk\global\parser\asasptime\nsa\ASASPTimeNSA.g4 by ANTLR 4.4

package mohawk.global.parser.asasptime.nsa;

import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.*;

import org.apache.commons.lang3.StringUtils;
import mohawk.global.pieces.*;
import mohawk.global.helper.*;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/** This interface defines a complete listener for a parse tree produced by
 * {@link ASASPTimeNSAParser}. */
public interface ASASPTimeNSAListener extends ParseTreeListener {
    /** Enter a parse tree produced by {@link ASASPTimeNSAParser#init}.
     * 
     * @param ctx the parse tree */
    void enterInit(@NotNull ASASPTimeNSAParser.InitContext ctx);
    /** Exit a parse tree produced by {@link ASASPTimeNSAParser#init}.
     * 
     * @param ctx the parse tree */
    void exitInit(@NotNull ASASPTimeNSAParser.InitContext ctx);
    /** Enter a parse tree produced by {@link ASASPTimeNSAParser#myroleA}.
     * 
     * @param ctx the parse tree */
    void enterMyroleA(@NotNull ASASPTimeNSAParser.MyroleAContext ctx);
    /** Exit a parse tree produced by {@link ASASPTimeNSAParser#myroleA}.
     * 
     * @param ctx the parse tree */
    void exitMyroleA(@NotNull ASASPTimeNSAParser.MyroleAContext ctx);
    /** Enter a parse tree produced by {@link ASASPTimeNSAParser#query}.
     * 
     * @param ctx the parse tree */
    void enterQuery(@NotNull ASASPTimeNSAParser.QueryContext ctx);
    /** Exit a parse tree produced by {@link ASASPTimeNSAParser#query}.
     * 
     * @param ctx the parse tree */
    void exitQuery(@NotNull ASASPTimeNSAParser.QueryContext ctx);
    /** Enter a parse tree produced by {@link ASASPTimeNSAParser#precondition}.
     * 
     * @param ctx the parse tree */
    void enterPrecondition(@NotNull ASASPTimeNSAParser.PreconditionContext ctx);
    /** Exit a parse tree produced by {@link ASASPTimeNSAParser#precondition}.
     * 
     * @param ctx the parse tree */
    void exitPrecondition(@NotNull ASASPTimeNSAParser.PreconditionContext ctx);
    /** Enter a parse tree produced by {@link ASASPTimeNSAParser#candisable}.
     * 
     * @param ctx the parse tree */
    void enterCandisable(@NotNull ASASPTimeNSAParser.CandisableContext ctx);
    /** Exit a parse tree produced by {@link ASASPTimeNSAParser#candisable}.
     * 
     * @param ctx the parse tree */
    void exitCandisable(@NotNull ASASPTimeNSAParser.CandisableContext ctx);
    /** Enter a parse tree produced by {@link ASASPTimeNSAParser#canrevoke}.
     * 
     * @param ctx the parse tree */
    void enterCanrevoke(@NotNull ASASPTimeNSAParser.CanrevokeContext ctx);
    /** Exit a parse tree produced by {@link ASASPTimeNSAParser#canrevoke}.
     * 
     * @param ctx the parse tree */
    void exitCanrevoke(@NotNull ASASPTimeNSAParser.CanrevokeContext ctx);
    /** Enter a parse tree produced by {@link ASASPTimeNSAParser#myrule}.
     * 
     * @param ctx the parse tree */
    void enterMyrule(@NotNull ASASPTimeNSAParser.MyruleContext ctx);
    /** Exit a parse tree produced by {@link ASASPTimeNSAParser#myrule}.
     * 
     * @param ctx the parse tree */
    void exitMyrule(@NotNull ASASPTimeNSAParser.MyruleContext ctx);
    /** Enter a parse tree produced by {@link ASASPTimeNSAParser#timeslot}.
     * 
     * @param ctx the parse tree */
    void enterTimeslot(@NotNull ASASPTimeNSAParser.TimeslotContext ctx);
    /** Exit a parse tree produced by {@link ASASPTimeNSAParser#timeslot}.
     * 
     * @param ctx the parse tree */
    void exitTimeslot(@NotNull ASASPTimeNSAParser.TimeslotContext ctx);
    /** Enter a parse tree produced by {@link ASASPTimeNSAParser#canenable}.
     * 
     * @param ctx the parse tree */
    void enterCanenable(@NotNull ASASPTimeNSAParser.CanenableContext ctx);
    /** Exit a parse tree produced by {@link ASASPTimeNSAParser#canenable}.
     * 
     * @param ctx the parse tree */
    void exitCanenable(@NotNull ASASPTimeNSAParser.CanenableContext ctx);
    /** Enter a parse tree produced by {@link ASASPTimeNSAParser#canassign}.
     * 
     * @param ctx the parse tree */
    void enterCanassign(@NotNull ASASPTimeNSAParser.CanassignContext ctx);
    /** Exit a parse tree produced by {@link ASASPTimeNSAParser#canassign}.
     * 
     * @param ctx the parse tree */
    void exitCanassign(@NotNull ASASPTimeNSAParser.CanassignContext ctx);
    /** Enter a parse tree produced by {@link ASASPTimeNSAParser#rolecondition}.
     * 
     * @param ctx the parse tree */
    void enterRolecondition(@NotNull ASASPTimeNSAParser.RoleconditionContext ctx);
    /** Exit a parse tree produced by {@link ASASPTimeNSAParser#rolecondition}.
     * 
     * @param ctx the parse tree */
    void exitRolecondition(@NotNull ASASPTimeNSAParser.RoleconditionContext ctx);
    /** Enter a parse tree produced by {@link ASASPTimeNSAParser#myrole}.
     * 
     * @param ctx the parse tree */
    void enterMyrole(@NotNull ASASPTimeNSAParser.MyroleContext ctx);
    /** Exit a parse tree produced by {@link ASASPTimeNSAParser#myrole}.
     * 
     * @param ctx the parse tree */
    void exitMyrole(@NotNull ASASPTimeNSAParser.MyroleContext ctx);
    /** Enter a parse tree produced by {@link ASASPTimeNSAParser#config}.
     * 
     * @param ctx the parse tree */
    void enterConfig(@NotNull ASASPTimeNSAParser.ConfigContext ctx);
    /** Exit a parse tree produced by {@link ASASPTimeNSAParser#config}.
     * 
     * @param ctx the parse tree */
    void exitConfig(@NotNull ASASPTimeNSAParser.ConfigContext ctx);
}
