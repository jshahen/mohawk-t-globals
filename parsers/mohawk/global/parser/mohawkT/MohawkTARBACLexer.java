// Generated from D:\Work\Masters\workspace (GradSchool)\Mohawk-T Globals\parsers\mohawk\global\parser\mohawkT\MohawkTARBAC.g4 by ANTLR 4.4

package mohawk.global.parser.mohawkT;

import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.*;

import org.apache.commons.lang3.StringUtils;
import mohawk.global.pieces.*;
import mohawk.global.helper.*;

import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class MohawkTARBACLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.4", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__1=1, T__0=2, Whitespace=3, Newline=4, BlockComment=5, LineComment=6, 
		Timeslot=7, CanAssign=8, CanRevoke=9, CanEnable=10, CanDisable=11, Query=12, 
		Expected=13, Not=14, True=15, False=16, Unknown=17, Reachable=18, Unreachable=19, 
		MyRole=20, LeftAngle=21, RightAngle=22, LeftBracket=23, RightBracket=24, 
		LeftBrace=25, RightBrace=26, AND=27, Colon=28, Comma=29, INT=30;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] tokenNames = {
		"'\\u0000'", "'\\u0001'", "'\\u0002'", "'\\u0003'", "'\\u0004'", "'\\u0005'", 
		"'\\u0006'", "'\\u0007'", "'\b'", "'\t'", "'\n'", "'\\u000B'", "'\f'", 
		"'\r'", "'\\u000E'", "'\\u000F'", "'\\u0010'", "'\\u0011'", "'\\u0012'", 
		"'\\u0013'", "'\\u0014'", "'\\u0015'", "'\\u0016'", "'\\u0017'", "'\\u0018'", 
		"'\\u0019'", "'\\u001A'", "'\\u001B'", "'\\u001C'", "'\\u001D'", "'\\u001E'"
	};
	public static final String[] ruleNames = {
		"T__1", "T__0", "Whitespace", "Newline", "BlockComment", "LineComment", 
		"Timeslot", "CanAssign", "CanRevoke", "CanEnable", "CanDisable", "Query", 
		"Expected", "Not", "True", "False", "Unknown", "Reachable", "Unreachable", 
		"MyRole", "LeftAngle", "RightAngle", "LeftBracket", "RightBracket", "LeftBrace", 
		"RightBrace", "AND", "Colon", "Comma", "DIGIT", "INT"
	};


		public final Logger logger = Logger.getLogger("mohawk");

		public boolean canassign_found = false;
		public boolean canrevoke_found = false;
		public boolean canenable_found = false;
		public boolean candisable_found = false;
		public boolean query_found = false;
		public boolean expected_found = false;
		int tabsize = 1;
		
		/* Global States */
		public MohawkT mohawkT = new MohawkT("Mohawk Converter");
		
		
		private void logmsg(String msg) {
			if(logger.getLevel() == Level.FINE) {
				System.out.println(StringUtils.repeat("  ", tabsize) + msg);
			}
		}


	public MohawkTARBACLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "MohawkTARBAC.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2 \u00f4\b\1\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \3\2"+
		"\3\2\3\3\3\3\3\4\6\4G\n\4\r\4\16\4H\3\4\3\4\3\5\3\5\5\5O\n\5\3\5\5\5R"+
		"\n\5\3\5\3\5\3\6\3\6\3\6\3\6\7\6Z\n\6\f\6\16\6]\13\6\3\6\3\6\3\6\3\6\3"+
		"\6\3\7\3\7\3\7\3\7\7\7h\n\7\f\7\16\7k\13\7\3\7\3\7\3\b\3\b\3\b\3\t\3\t"+
		"\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3"+
		"\n\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3\f\3"+
		"\f\3\f\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3\16"+
		"\3\16\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17\3\20\3\20\3\20\3\20\3\20"+
		"\3\21\3\21\3\21\3\21\3\21\3\21\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22"+
		"\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\24\3\24\3\24\3\24"+
		"\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\25\6\25\u00d8\n\25\r\25\16"+
		"\25\u00d9\3\26\3\26\3\27\3\27\3\30\3\30\3\31\3\31\3\32\3\32\3\33\3\33"+
		"\3\34\3\34\3\35\3\35\3\36\3\36\3\37\3\37\3 \6 \u00f1\n \r \16 \u00f2\3"+
		"[\2!\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35"+
		"\20\37\21!\22#\23%\24\'\25)\26+\27-\30/\31\61\32\63\33\65\34\67\359\36"+
		";\37=\2? \3\2\6\4\2\13\13\"\"\4\2\f\f\17\17\5\2\62;C\\c|\3\2\62;\u00f9"+
		"\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2"+
		"\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2"+
		"\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2"+
		"\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2"+
		"\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3"+
		"\2\2\2\2?\3\2\2\2\3A\3\2\2\2\5C\3\2\2\2\7F\3\2\2\2\tQ\3\2\2\2\13U\3\2"+
		"\2\2\rc\3\2\2\2\17n\3\2\2\2\21q\3\2\2\2\23{\3\2\2\2\25\u0085\3\2\2\2\27"+
		"\u008f\3\2\2\2\31\u009a\3\2\2\2\33\u00a0\3\2\2\2\35\u00a9\3\2\2\2\37\u00ad"+
		"\3\2\2\2!\u00b2\3\2\2\2#\u00b8\3\2\2\2%\u00c0\3\2\2\2\'\u00ca\3\2\2\2"+
		")\u00d7\3\2\2\2+\u00db\3\2\2\2-\u00dd\3\2\2\2/\u00df\3\2\2\2\61\u00e1"+
		"\3\2\2\2\63\u00e3\3\2\2\2\65\u00e5\3\2\2\2\67\u00e7\3\2\2\29\u00e9\3\2"+
		"\2\2;\u00eb\3\2\2\2=\u00ed\3\2\2\2?\u00f0\3\2\2\2AB\7/\2\2B\4\3\2\2\2"+
		"CD\7\u0080\2\2D\6\3\2\2\2EG\t\2\2\2FE\3\2\2\2GH\3\2\2\2HF\3\2\2\2HI\3"+
		"\2\2\2IJ\3\2\2\2JK\b\4\2\2K\b\3\2\2\2LN\7\17\2\2MO\7\f\2\2NM\3\2\2\2N"+
		"O\3\2\2\2OR\3\2\2\2PR\7\f\2\2QL\3\2\2\2QP\3\2\2\2RS\3\2\2\2ST\b\5\2\2"+
		"T\n\3\2\2\2UV\7\61\2\2VW\7,\2\2W[\3\2\2\2XZ\13\2\2\2YX\3\2\2\2Z]\3\2\2"+
		"\2[\\\3\2\2\2[Y\3\2\2\2\\^\3\2\2\2][\3\2\2\2^_\7,\2\2_`\7\61\2\2`a\3\2"+
		"\2\2ab\b\6\2\2b\f\3\2\2\2cd\7\61\2\2de\7\61\2\2ei\3\2\2\2fh\n\3\2\2gf"+
		"\3\2\2\2hk\3\2\2\2ig\3\2\2\2ij\3\2\2\2jl\3\2\2\2ki\3\2\2\2lm\b\7\2\2m"+
		"\16\3\2\2\2no\7v\2\2op\5? \2p\20\3\2\2\2qr\7E\2\2rs\7c\2\2st\7p\2\2tu"+
		"\7C\2\2uv\7u\2\2vw\7u\2\2wx\7k\2\2xy\7i\2\2yz\7p\2\2z\22\3\2\2\2{|\7E"+
		"\2\2|}\7c\2\2}~\7p\2\2~\177\7T\2\2\177\u0080\7g\2\2\u0080\u0081\7x\2\2"+
		"\u0081\u0082\7q\2\2\u0082\u0083\7m\2\2\u0083\u0084\7g\2\2\u0084\24\3\2"+
		"\2\2\u0085\u0086\7E\2\2\u0086\u0087\7c\2\2\u0087\u0088\7p\2\2\u0088\u0089"+
		"\7G\2\2\u0089\u008a\7p\2\2\u008a\u008b\7c\2\2\u008b\u008c\7d\2\2\u008c"+
		"\u008d\7n\2\2\u008d\u008e\7g\2\2\u008e\26\3\2\2\2\u008f\u0090\7E\2\2\u0090"+
		"\u0091\7c\2\2\u0091\u0092\7p\2\2\u0092\u0093\7F\2\2\u0093\u0094\7k\2\2"+
		"\u0094\u0095\7u\2\2\u0095\u0096\7c\2\2\u0096\u0097\7d\2\2\u0097\u0098"+
		"\7n\2\2\u0098\u0099\7g\2\2\u0099\30\3\2\2\2\u009a\u009b\7S\2\2\u009b\u009c"+
		"\7w\2\2\u009c\u009d\7g\2\2\u009d\u009e\7t\2\2\u009e\u009f\7{\2\2\u009f"+
		"\32\3\2\2\2\u00a0\u00a1\7G\2\2\u00a1\u00a2\7z\2\2\u00a2\u00a3\7r\2\2\u00a3"+
		"\u00a4\7g\2\2\u00a4\u00a5\7e\2\2\u00a5\u00a6\7v\2\2\u00a6\u00a7\7g\2\2"+
		"\u00a7\u00a8\7f\2\2\u00a8\34\3\2\2\2\u00a9\u00aa\7P\2\2\u00aa\u00ab\7"+
		"Q\2\2\u00ab\u00ac\7V\2\2\u00ac\36\3\2\2\2\u00ad\u00ae\7V\2\2\u00ae\u00af"+
		"\7T\2\2\u00af\u00b0\7W\2\2\u00b0\u00b1\7G\2\2\u00b1 \3\2\2\2\u00b2\u00b3"+
		"\7H\2\2\u00b3\u00b4\7C\2\2\u00b4\u00b5\7N\2\2\u00b5\u00b6\7U\2\2\u00b6"+
		"\u00b7\7G\2\2\u00b7\"\3\2\2\2\u00b8\u00b9\7W\2\2\u00b9\u00ba\7P\2\2\u00ba"+
		"\u00bb\7M\2\2\u00bb\u00bc\7P\2\2\u00bc\u00bd\7Q\2\2\u00bd\u00be\7Y\2\2"+
		"\u00be\u00bf\7P\2\2\u00bf$\3\2\2\2\u00c0\u00c1\7T\2\2\u00c1\u00c2\7G\2"+
		"\2\u00c2\u00c3\7C\2\2\u00c3\u00c4\7E\2\2\u00c4\u00c5\7J\2\2\u00c5\u00c6"+
		"\7C\2\2\u00c6\u00c7\7D\2\2\u00c7\u00c8\7N\2\2\u00c8\u00c9\7G\2\2\u00c9"+
		"&\3\2\2\2\u00ca\u00cb\7W\2\2\u00cb\u00cc\7P\2\2\u00cc\u00cd\7T\2\2\u00cd"+
		"\u00ce\7G\2\2\u00ce\u00cf\7C\2\2\u00cf\u00d0\7E\2\2\u00d0\u00d1\7J\2\2"+
		"\u00d1\u00d2\7C\2\2\u00d2\u00d3\7D\2\2\u00d3\u00d4\7N\2\2\u00d4\u00d5"+
		"\7G\2\2\u00d5(\3\2\2\2\u00d6\u00d8\t\4\2\2\u00d7\u00d6\3\2\2\2\u00d8\u00d9"+
		"\3\2\2\2\u00d9\u00d7\3\2\2\2\u00d9\u00da\3\2\2\2\u00da*\3\2\2\2\u00db"+
		"\u00dc\7>\2\2\u00dc,\3\2\2\2\u00dd\u00de\7@\2\2\u00de.\3\2\2\2\u00df\u00e0"+
		"\7]\2\2\u00e0\60\3\2\2\2\u00e1\u00e2\7_\2\2\u00e2\62\3\2\2\2\u00e3\u00e4"+
		"\7}\2\2\u00e4\64\3\2\2\2\u00e5\u00e6\7\177\2\2\u00e6\66\3\2\2\2\u00e7"+
		"\u00e8\7(\2\2\u00e88\3\2\2\2\u00e9\u00ea\7<\2\2\u00ea:\3\2\2\2\u00eb\u00ec"+
		"\7.\2\2\u00ec<\3\2\2\2\u00ed\u00ee\t\5\2\2\u00ee>\3\2\2\2\u00ef\u00f1"+
		"\5=\37\2\u00f0\u00ef\3\2\2\2\u00f1\u00f2\3\2\2\2\u00f2\u00f0\3\2\2\2\u00f2"+
		"\u00f3\3\2\2\2\u00f3@\3\2\2\2\n\2HNQ[i\u00d9\u00f2\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}