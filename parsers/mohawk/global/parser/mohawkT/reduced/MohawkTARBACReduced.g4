/**
 * 
 */
grammar MohawkTARBACReduced;

@header {
package mohawk.global.parser.mohawkT.reduced;

import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.*;

import org.apache.commons.lang3.StringUtils;
import mohawk.global.pieces.*;
import mohawk.global.helper.*;
}

@members {
	public final Logger logger = Logger.getLogger("mohawk");

	public boolean canassign_found = false;
	public boolean canrevoke_found = false;
	public boolean canenable_found = false;
	public boolean candisable_found = false;
	public boolean query_found = false;
	public boolean expected_found = false;
	int tabsize = 1;
	
	/* Global States */
	public MohawkT mohawkT = new MohawkT("Mohawk Converter");
	
	
	private void logmsg(String msg) {
		if(logger.getLevel() == Level.FINE) {
			System.out.println(StringUtils.repeat("  ", tabsize) + msg);
		}
	}
}

init
:
	stat stat stat stat stat stat
;

stat
:
	{canassign_found==false}?

	canassign
	{canassign_found=true;}

	|
	{canrevoke_found==false}?

	canrevoke
	{canrevoke_found=true;}

	|
	{canenable_found==false}?

	canenable
	{canenable_found=true;}

	|
	{candisable_found==false}?

	candisable
	{candisable_found=true;}

	|
	{query_found==false}?

	query
	{query_found=true;}

	|
	{expected_found==false}?

	expected
	{expected_found=true;}

;

canassign
:
	{logmsg("Entering Can Assign");tabsize++;}

	CanAssign LeftBrace
	(
		myrule [RuleType.ASSIGN]
		{
			mohawkT.canAssign.addRule($myrule.r);
		}

	)* RightBrace
	{tabsize--; logmsg("Leaving Can Assign");}

;

canrevoke
:
	{logmsg("Entering Can Revoke");tabsize++;}

	CanRevoke LeftBrace
	(
		myrule [RuleType.REVOKE]
		{
			mohawkT.canRevoke.addRule($myrule.r);
		}

	)* RightBrace
	{tabsize--; logmsg("Leaving Can Revoke");}

;

canenable
:
	{logmsg("Entering Can Enable"); tabsize++;}

	CanEnable LeftBrace
	(
		myrule [RuleType.ENABLE]
		{
			mohawkT.canEnable.addRule($myrule.r);
		}

	)* RightBrace
	{tabsize--; logmsg("Leaving Can Enable");}

;

candisable
:
	{logmsg("Entering Can Disable");tabsize++;}

	CanDisable LeftBrace
	(
		myrule [RuleType.DISABLE]
		{
			mohawkT.canDisable.addRule($myrule.r);
		}

	)* RightBrace
	{tabsize--; logmsg("Leaving Can Disable");}

;

query
:
	Query Colon ra = roleArray
	{
		mohawkT.query._timeslot = new TimeSlot(1);
		mohawkT.query._roles = $ra.r;
		mohawkT.timeIntervalHelper.add(mohawkT.query._timeslot);
	}

;

expected
:
	Expected Colon
	(
		Reachable
		{mohawkT.expectedResult = ExpectedResult.REACHABLE;}

		| Unreachable
		{mohawkT.expectedResult = ExpectedResult.UNREACHABLE;}

		| Unknown
		{mohawkT.expectedResult = ExpectedResult.UNKNOWN;}

	)
;

myrule [RuleType rt] returns [Rule r]
locals [Boolean adminAllRoles]
:
	{logmsg("Adding Rule: "); tabsize++;}

	LeftAngle
	(
		adminRole = myrole
		{$adminAllRoles=false;}

		| True
		{$adminAllRoles=true;}

	) Comma 
	pc = precondition Comma
	role = myrole RightAngle
	{
		$r = new Rule();
		$r._type = rt;
		if($adminAllRoles == true) {
			$r._adminRole = Role.allRoles();
		} else {
			$r._adminRole = $adminRole.role;
		}
		$r._adminTimeInterval = new TimeInterval(1, 1);
		$r._preconditions = $pc.p;
		tsa = new ArrayList<TimeSlot>();
		tsa.add(new TimeSlot(1));
		$r._roleSchedule = tsa;
		$r._role = $role.role;
		tabsize--; 
		logmsg($r.toString());
	}

;

precondition returns [ArrayList<Role> p] @init {
	$p = new ArrayList<Role>();
}
:
	a = rolecondition
	{$p.add($a.r);}

	(
		AND b = rolecondition
		{$p.add($b.r);}

	)*
	| True
;

rolecondition returns [Role r] @init {
	boolean not = false;
}
:
	(
		not
		{not = true;}

	)? myrole
	{
		$r = new Role($myrole.rolet, "", not);
		/* logmsg("Role: "+ $r); */
	}

;

roleArray returns [ArrayList<Role> r] @init {
	$r = new ArrayList<Role>();
}
:
	LeftBracket a = myrole
	{$r.add($a.role);}

	(
		Comma b = myrole
		{$r.add($b.role);}

	)* RightBracket
	| a = myrole
	{$r.add($a.role);}

;

myrole returns [String rolet, Role role]
:
	MyRole
	{
		$rolet = $MyRole.getText();
		$role = new Role($rolet);
		mohawkT.roleHelper.add($role);
	}

;

not
:
	Not '~'
;

/* Whitespace and Comments */
Whitespace
:
	[ \t]+ -> skip
;

Newline
:
	(
		'\r' '\n'?
		| '\n'
	) -> skip
;

BlockComment
:
	'/*' .*? '*/' -> skip
;

LineComment
:
	'//' ~[\r\n]* -> skip
;

/* Key Words */
CanAssign
:
	'CanAssign'
;

CanRevoke
:
	'CanRevoke'
;

CanEnable
:
	'CanEnable'
;

CanDisable
:
	'CanDisable'
;

/*  NOT USED ANYMORE
MaxRoles
:
	'MaxRoles'
;

MaxTimeSlots
:
	'MaxTimeSlots'
;
*/
Query
:
	'Query'
;

Expected
:
	'Expected'
;

Not
:
	'NOT'
;

True
:
	'TRUE'
;

False
:
	'FALSE'
;

Unknown
:
	'UNKNOWN'
;

Reachable
:
	'REACHABLE'
;

Unreachable
:
	'UNREACHABLE'
;

// Special Notation
/*
RoleStrict
:
	'role' INT
;
*/
/* Called MyRole because there exists a class named Role */
MyRole
:
	[A-Za-z0-9]+
;

// Literals

LeftAngle
:
	'<'
;

RightAngle
:
	'>'
;

LeftBracket
:
	'['
;

RightBracket
:
	']'
;

LeftBrace
:
	'{'
;

RightBrace
:
	'}'
;

AND
:
	'&'
;

Colon
:
	':'
;

Comma
:
	','
;

fragment
DIGIT
:
	[0-9]
;

INT
:
	DIGIT+
;