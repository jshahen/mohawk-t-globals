// Generated from D:\Work\Masters\workspace (GradSchool)\Mohawk-T Globals\parsers\mohawk\global\parser\mohawk\v1\Mohawk.g4 by ANTLR 4.4

package mohawk.global.parser.mohawk.v1;

import java.util.Vector;
import java.util.Stack;
import java.util.HashMap;
import java.util.Map;
import mohawk.global.pieces.mohawk.PreCondProcessorInt;
import mohawk.global.pieces.mohawk.CAEntry;
import mohawk.global.pieces.mohawk.CREntry;
import mohawk.global.pieces.mohawk.PreCondition;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class MohawkParser extends Parser {
    static {
        RuntimeMetaData.checkVersion("4.4", RuntimeMetaData.VERSION);
    }

    protected static final DFA[] _decisionToDFA;
    protected static final PredictionContextCache _sharedContextCache = new PredictionContextCache();
    public static final int T__8 = 1, T__7 = 2, T__6 = 3, T__5 = 4, T__4 = 5, T__3 = 6, T__2 = 7, T__1 = 8, T__0 = 9,
            Whitespace = 10, Newline = 11, BlockComment = 12, LineComment = 13, ID = 14, LANGLE = 15, RANGLE = 16,
            COMMA = 17, COND = 18, NOT = 19, SEMI = 20;
    public static final String[] tokenNames = {"<INVALID>", "'ADMIN'", "'Roles'", "'UA'", "'FALSE'", "'Users'", "'CR'",
            "'SPEC'", "'TRUE'", "'CA'", "Whitespace", "Newline", "BlockComment", "LineComment", "ID", "'<'", "'>'",
            "','", "'&'", "'-'", "';'"};
    public static final int RULE_init = 0, RULE_roles = 1, RULE_users = 2, RULE_ua = 3, RULE_ca = 4, RULE_caentry = 5,
            RULE_precondition = 6, RULE_rolecondition = 7, RULE_cr = 8, RULE_crentry = 9, RULE_admin = 10,
            RULE_spec = 11, RULE_myrole = 12;
    public static final String[] ruleNames = {"init", "roles", "users", "ua", "ca", "caentry", "precondition",
            "rolecondition", "cr", "crentry", "admin", "spec", "myrole"};

    @Override
    public String getGrammarFileName() {
        return "Mohawk.g4";
    }

    @Override
    public String[] getTokenNames() {
        return tokenNames;
    }

    @Override
    public String[] getRuleNames() {
        return ruleNames;
    }

    @Override
    public String getSerializedATN() {
        return _serializedATN;
    }

    @Override
    public ATN getATN() {
        return _ATN;
    }

    public Vector<String> vRoles;
    public Vector<String> vUsers;
    public Vector<String> vAdmin;
    public Map<Integer, String> mRoleIndex;
    public Map<String, Integer> mRole2Index;
    public Map<Integer, String> mUserIndex;
    public Map<String, Vector<Integer>> mUA;
    public Map<String, Vector<CREntry>> mCR;
    public Map<String, Vector<CAEntry>> mCA;
    public PreCondProcessorInt preCndP;
    public Stack<Integer> stackOperators;
    public Vector<String> vSpec; // This vector holds two strings - user and role that will be used in the LTL formulae

    // Indices for user and roles while parsing
    // Each user has an index corresponding to the order in which the name appears in the list.
    public int iRoleIndex = 0;
    public int iUserIndex = 0;

    // Counters
    public int numUA = 0;
    public int numCARules = 0;
    public int numCRRules = 0;

    public void initRbac() {
        vRoles = new Vector<String>();
        vUsers = new Vector<String>();
        vAdmin = new Vector<String>();
        mRoleIndex = new HashMap<Integer, String>();
        mRole2Index = new HashMap<String, Integer>();
        mUserIndex = new HashMap<Integer, String>();
        mUA = new HashMap<String, Vector<Integer>>();
        mCR = new HashMap<String, Vector<CREntry>>();
        mCA = new HashMap<String, Vector<CAEntry>>();
        vSpec = new Vector<String>();
    }

    /*
    public RBACInstance getRBAC() {
    	return new RBACInstance(vRoles, vUsers, vAdmin, mUA, mCR, mCA,vSpec);
    }
    */

    public void setUA(String strUser, String strRole) {

        Vector<Integer> vUserUA = mUA.get(strUser);
        if (vUserUA == null) {
            vUserUA = new Vector<Integer>();
            mUA.put(strUser, vUserUA);
        }

        int iRoleIndex = mRole2Index.get(strRole); // getMapKey(mRoleIndex, strRole);
        vUserUA.add(iRoleIndex);
    }

    public void addCREntry(String inStrPreCond, String inStrRole) {

        CREntry crEntry = new CREntry(inStrPreCond, inStrRole);
        Vector<CREntry> vCR = mCR.get(inStrRole);

        if (vCR == null) vCR = new Vector<CREntry>();

        vCR.add(new CREntry(inStrPreCond, inStrRole));
        mCR.put(inStrRole, vCR);
    }

    public void addCAEntry(String inStrAdminRole, PreCondition pcPreCond, String inStrRole) {

        CAEntry caEntry = new CAEntry(inStrAdminRole, pcPreCond, inStrRole);
        Vector<CAEntry> vCA = mCA.get(inStrRole);

        if (vCA == null) vCA = new Vector<CAEntry>();
        vCA.add(caEntry);
        mCA.put(inStrRole, vCA);
    }
    /*	
    private int getMapKey(Map<Integer,String> inMap, String inString) {
    	
    	for(int i=0; i<inMap.size(); i++) {
    		
    		if(inMap.get(i).equals(inString)) {
    			return i;
    		}
    	}
    	
    	System.out.println("Error - BTree::getMapIndex - Value not found in map");
    	return 0;
    }
    */

    public void addSpec(String inStrUser, String inStrRole) {

        vSpec.add(inStrUser);
        vSpec.add(inStrRole);
    }

    public MohawkParser(TokenStream input) {
        super(input);
        _interp = new ParserATNSimulator(this, _ATN, _decisionToDFA, _sharedContextCache);
    }
    public static class InitContext extends ParserRuleContext {
        public RolesContext roles() {
            return getRuleContext(RolesContext.class, 0);
        }
        public UaContext ua() {
            return getRuleContext(UaContext.class, 0);
        }
        public AdminContext admin() {
            return getRuleContext(AdminContext.class, 0);
        }
        public UsersContext users() {
            return getRuleContext(UsersContext.class, 0);
        }
        public SpecContext spec() {
            return getRuleContext(SpecContext.class, 0);
        }
        public CaContext ca() {
            return getRuleContext(CaContext.class, 0);
        }
        public CrContext cr() {
            return getRuleContext(CrContext.class, 0);
        }
        public InitContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }
        @Override
        public int getRuleIndex() {
            return RULE_init;
        }
        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MohawkListener) ((MohawkListener) listener).enterInit(this);
        }
        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MohawkListener) ((MohawkListener) listener).exitInit(this);
        }
    }

    public final InitContext init() throws RecognitionException {
        InitContext _localctx = new InitContext(_ctx, getState());
        enterRule(_localctx, 0, RULE_init);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(26);
                roles();
                setState(27);
                users();
                setState(28);
                ua();
                setState(29);
                cr();
                setState(30);
                ca();
                setState(31);
                admin();
                setState(32);
                spec();
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class RolesContext extends ParserRuleContext {
        public Token r;
        public List<TerminalNode> ID() {
            return getTokens(MohawkParser.ID);
        }
        public TerminalNode SEMI() {
            return getToken(MohawkParser.SEMI, 0);
        }
        public TerminalNode ID(int i) {
            return getToken(MohawkParser.ID, i);
        }
        public RolesContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }
        @Override
        public int getRuleIndex() {
            return RULE_roles;
        }
        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MohawkListener) ((MohawkListener) listener).enterRoles(this);
        }
        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MohawkListener) ((MohawkListener) listener).exitRoles(this);
        }
    }

    public final RolesContext roles() throws RecognitionException {
        RolesContext _localctx = new RolesContext(_ctx, getState());
        enterRule(_localctx, 2, RULE_roles);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(34);
                match(T__7);
                setState(37);
                _errHandler.sync(this);
                _la = _input.LA(1);
                do {
                    {
                        {
                            setState(35);
                            ((RolesContext) _localctx).r = match(ID);

                            vRoles.add(((RolesContext) _localctx).r.getText());
                            mRoleIndex.put(iRoleIndex, ((RolesContext) _localctx).r.getText());
                            mRole2Index.put(((RolesContext) _localctx).r.getText(), iRoleIndex);
                            iRoleIndex++;

                        }
                    }
                    setState(39);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                } while (_la == ID);
                setState(41);
                match(SEMI);

                System.out.println("[STATS] Mohawk Input Roles: " + iRoleIndex);

            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class UsersContext extends ParserRuleContext {
        public Token u;
        public List<TerminalNode> ID() {
            return getTokens(MohawkParser.ID);
        }
        public TerminalNode SEMI() {
            return getToken(MohawkParser.SEMI, 0);
        }
        public TerminalNode ID(int i) {
            return getToken(MohawkParser.ID, i);
        }
        public UsersContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }
        @Override
        public int getRuleIndex() {
            return RULE_users;
        }
        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MohawkListener) ((MohawkListener) listener).enterUsers(this);
        }
        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MohawkListener) ((MohawkListener) listener).exitUsers(this);
        }
    }

    public final UsersContext users() throws RecognitionException {
        UsersContext _localctx = new UsersContext(_ctx, getState());
        enterRule(_localctx, 4, RULE_users);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                {
                    setState(44);
                    match(T__4);
                }
                setState(47);
                _errHandler.sync(this);
                _la = _input.LA(1);
                do {
                    {
                        {
                            setState(45);
                            ((UsersContext) _localctx).u = match(ID);

                            vUsers.add(((UsersContext) _localctx).u.getText());
                            mUserIndex.put(iUserIndex, ((UsersContext) _localctx).u.getText());
                            iUserIndex++;

                        }
                    }
                    setState(49);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                } while (_la == ID);
                setState(51);
                match(SEMI);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class UaContext extends ParserRuleContext {
        public Token x;
        public Token y;
        public List<TerminalNode> LANGLE() {
            return getTokens(MohawkParser.LANGLE);
        }
        public List<TerminalNode> ID() {
            return getTokens(MohawkParser.ID);
        }
        public List<TerminalNode> RANGLE() {
            return getTokens(MohawkParser.RANGLE);
        }
        public TerminalNode SEMI() {
            return getToken(MohawkParser.SEMI, 0);
        }
        public TerminalNode LANGLE(int i) {
            return getToken(MohawkParser.LANGLE, i);
        }
        public List<TerminalNode> COMMA() {
            return getTokens(MohawkParser.COMMA);
        }
        public TerminalNode RANGLE(int i) {
            return getToken(MohawkParser.RANGLE, i);
        }
        public TerminalNode ID(int i) {
            return getToken(MohawkParser.ID, i);
        }
        public TerminalNode COMMA(int i) {
            return getToken(MohawkParser.COMMA, i);
        }
        public UaContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }
        @Override
        public int getRuleIndex() {
            return RULE_ua;
        }
        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MohawkListener) ((MohawkListener) listener).enterUa(this);
        }
        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MohawkListener) ((MohawkListener) listener).exitUa(this);
        }
    }

    public final UaContext ua() throws RecognitionException {
        UaContext _localctx = new UaContext(_ctx, getState());
        enterRule(_localctx, 6, RULE_ua);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(53);
                match(T__6);
                setState(60);
                _errHandler.sync(this);
                _la = _input.LA(1);
                do {
                    {
                        {
                            setState(54);
                            match(LANGLE);
                            setState(55);
                            ((UaContext) _localctx).x = match(ID);
                            setState(56);
                            match(COMMA);
                            setState(57);
                            ((UaContext) _localctx).y = match(ID);
                            setState(58);
                            match(RANGLE);

                            setUA(((UaContext) _localctx).x.getText(), ((UaContext) _localctx).y.getText());
                            numUA++;

                        }
                    }
                    setState(62);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                } while (_la == LANGLE);
                setState(64);
                match(SEMI);

                System.out.println("[STATS] Mohawk Input UA: " + numUA);

            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class CaContext extends ParserRuleContext {
        public TerminalNode SEMI() {
            return getToken(MohawkParser.SEMI, 0);
        }
        public List<CaentryContext> caentry() {
            return getRuleContexts(CaentryContext.class);
        }
        public CaentryContext caentry(int i) {
            return getRuleContext(CaentryContext.class, i);
        }
        public CaContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }
        @Override
        public int getRuleIndex() {
            return RULE_ca;
        }
        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MohawkListener) ((MohawkListener) listener).enterCa(this);
        }
        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MohawkListener) ((MohawkListener) listener).exitCa(this);
        }
    }

    public final CaContext ca() throws RecognitionException {
        CaContext _localctx = new CaContext(_ctx, getState());
        enterRule(_localctx, 8, RULE_ca);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(67);
                match(T__0);
                setState(71);
                _errHandler.sync(this);
                _la = _input.LA(1);
                while (_la == LANGLE) {
                    {
                        {
                            setState(68);
                            caentry();
                        }
                    }
                    setState(73);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                }
                setState(74);
                match(SEMI);

                System.out.println("[STATS] Mohawk Input CA Rules: " + numCARules);

            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class CaentryContext extends ParserRuleContext {
        public Token d;
        public PreconditionContext pre;
        public MyroleContext f;
        public TerminalNode LANGLE() {
            return getToken(MohawkParser.LANGLE, 0);
        }
        public TerminalNode ID() {
            return getToken(MohawkParser.ID, 0);
        }
        public TerminalNode RANGLE() {
            return getToken(MohawkParser.RANGLE, 0);
        }
        public PreconditionContext precondition() {
            return getRuleContext(PreconditionContext.class, 0);
        }
        public List<TerminalNode> COMMA() {
            return getTokens(MohawkParser.COMMA);
        }
        public MyroleContext myrole() {
            return getRuleContext(MyroleContext.class, 0);
        }
        public TerminalNode COMMA(int i) {
            return getToken(MohawkParser.COMMA, i);
        }
        public CaentryContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }
        @Override
        public int getRuleIndex() {
            return RULE_caentry;
        }
        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MohawkListener) ((MohawkListener) listener).enterCaentry(this);
        }
        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MohawkListener) ((MohawkListener) listener).exitCaentry(this);
        }
    }

    public final CaentryContext caentry() throws RecognitionException {
        CaentryContext _localctx = new CaentryContext(_ctx, getState());
        enterRule(_localctx, 10, RULE_caentry);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(77);
                match(LANGLE);
                setState(78);
                ((CaentryContext) _localctx).d = match(ID);
                setState(79);
                match(COMMA);
                setState(80);
                ((CaentryContext) _localctx).pre = precondition();
                setState(81);
                match(COMMA);
                setState(82);
                ((CaentryContext) _localctx).f = myrole();
                setState(83);
                match(RANGLE);

                try {
                    PreCondition pcPreCond = ((CaentryContext) _localctx).pre.p.result();
                    addCAEntry(((CaentryContext) _localctx).d.getText(), pcPreCond,
                            ((CaentryContext) _localctx).f.role);
                    numCARules++;
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class PreconditionContext extends ParserRuleContext {
        public PreCondProcessorInt p;
        public RoleconditionContext a;
        public RoleconditionContext b;
        public List<RoleconditionContext> rolecondition() {
            return getRuleContexts(RoleconditionContext.class);
        }
        public RoleconditionContext rolecondition(int i) {
            return getRuleContext(RoleconditionContext.class, i);
        }
        public TerminalNode COND(int i) {
            return getToken(MohawkParser.COND, i);
        }
        public List<TerminalNode> COND() {
            return getTokens(MohawkParser.COND);
        }
        public PreconditionContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }
        @Override
        public int getRuleIndex() {
            return RULE_precondition;
        }
        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MohawkListener) ((MohawkListener) listener).enterPrecondition(this);
        }
        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MohawkListener) ((MohawkListener) listener).exitPrecondition(this);
        }
    }

    public final PreconditionContext precondition() throws RecognitionException {
        PreconditionContext _localctx = new PreconditionContext(_ctx, getState());
        enterRule(_localctx, 12, RULE_precondition);

        ((PreconditionContext) _localctx).p = new PreCondProcessorInt(mRole2Index);

        int _la;
        try {
            setState(98);
            switch (_input.LA(1)) {
                case ID :
                case NOT :
                    enterOuterAlt(_localctx, 1); {
                    setState(86);
                    ((PreconditionContext) _localctx).a = rolecondition();

                    try {
                        if (((PreconditionContext) _localctx).a.not) {
                            _localctx.p.addNeg(((PreconditionContext) _localctx).a.name);
                        } else {
                            _localctx.p.add(((PreconditionContext) _localctx).a.name);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    setState(94);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                    while (_la == COND) {
                        {
                            {
                                setState(88);
                                match(COND);
                                setState(89);
                                ((PreconditionContext) _localctx).b = rolecondition();

                                try {
                                    if (((PreconditionContext) _localctx).b.not) {
                                        _localctx.p.addNeg(((PreconditionContext) _localctx).b.name);
                                    } else {
                                        _localctx.p.add(((PreconditionContext) _localctx).b.name);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        }
                        setState(96);
                        _errHandler.sync(this);
                        _la = _input.LA(1);
                    }
                }
                    break;
                case T__1 :
                    enterOuterAlt(_localctx, 2); {
                    setState(97);
                    match(T__1);
                }
                    break;
                default :
                    throw new NoViableAltException(this);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class RoleconditionContext extends ParserRuleContext {
        public Boolean not;
        public String name;
        public MyroleContext r;
        public TerminalNode NOT() {
            return getToken(MohawkParser.NOT, 0);
        }
        public MyroleContext myrole() {
            return getRuleContext(MyroleContext.class, 0);
        }
        public RoleconditionContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }
        @Override
        public int getRuleIndex() {
            return RULE_rolecondition;
        }
        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MohawkListener) ((MohawkListener) listener).enterRolecondition(this);
        }
        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MohawkListener) ((MohawkListener) listener).exitRolecondition(this);
        }
    }

    public final RoleconditionContext rolecondition() throws RecognitionException {
        RoleconditionContext _localctx = new RoleconditionContext(_ctx, getState());
        enterRule(_localctx, 14, RULE_rolecondition);

        ((RoleconditionContext) _localctx).not = false;

        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(102);
                _la = _input.LA(1);
                if (_la == NOT) {
                    {
                        setState(100);
                        match(NOT);
                        ((RoleconditionContext) _localctx).not = true;
                    }
                }

                setState(104);
                ((RoleconditionContext) _localctx).r = myrole();

                ((RoleconditionContext) _localctx).name = ((RoleconditionContext) _localctx).r.role;

            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class CrContext extends ParserRuleContext {
        public List<CrentryContext> crentry() {
            return getRuleContexts(CrentryContext.class);
        }
        public TerminalNode SEMI() {
            return getToken(MohawkParser.SEMI, 0);
        }
        public CrentryContext crentry(int i) {
            return getRuleContext(CrentryContext.class, i);
        }
        public CrContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }
        @Override
        public int getRuleIndex() {
            return RULE_cr;
        }
        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MohawkListener) ((MohawkListener) listener).enterCr(this);
        }
        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MohawkListener) ((MohawkListener) listener).exitCr(this);
        }
    }

    public final CrContext cr() throws RecognitionException {
        CrContext _localctx = new CrContext(_ctx, getState());
        enterRule(_localctx, 16, RULE_cr);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(107);
                match(T__3);
                setState(111);
                _errHandler.sync(this);
                _la = _input.LA(1);
                while (_la == LANGLE) {
                    {
                        {
                            setState(108);
                            crentry();
                        }
                    }
                    setState(113);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                }
                setState(114);
                match(SEMI);

                System.out.println("[STATS] Mohawk Input CR Rules: " + numCRRules);

            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class CrentryContext extends ParserRuleContext {
        public Token mm;
        public MyroleContext nn;
        public TerminalNode LANGLE() {
            return getToken(MohawkParser.LANGLE, 0);
        }
        public TerminalNode ID() {
            return getToken(MohawkParser.ID, 0);
        }
        public TerminalNode RANGLE() {
            return getToken(MohawkParser.RANGLE, 0);
        }
        public TerminalNode COMMA() {
            return getToken(MohawkParser.COMMA, 0);
        }
        public MyroleContext myrole() {
            return getRuleContext(MyroleContext.class, 0);
        }
        public CrentryContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }
        @Override
        public int getRuleIndex() {
            return RULE_crentry;
        }
        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MohawkListener) ((MohawkListener) listener).enterCrentry(this);
        }
        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MohawkListener) ((MohawkListener) listener).exitCrentry(this);
        }
    }

    public final CrentryContext crentry() throws RecognitionException {
        CrentryContext _localctx = new CrentryContext(_ctx, getState());
        enterRule(_localctx, 18, RULE_crentry);
        try {
            setState(130);
            switch (getInterpreter().adaptivePredict(_input, 8, _ctx)) {
                case 1 :
                    enterOuterAlt(_localctx, 1); {
                    setState(117);
                    match(LANGLE);
                    setState(118);
                    ((CrentryContext) _localctx).mm = match(ID);
                    setState(119);
                    match(COMMA);
                    setState(120);
                    ((CrentryContext) _localctx).nn = myrole();
                    setState(121);
                    match(RANGLE);

                    addCREntry(((CrentryContext) _localctx).mm.getText(), ((CrentryContext) _localctx).nn.role);

                }
                    break;
                case 2 :
                    enterOuterAlt(_localctx, 2); {
                    setState(124);
                    match(LANGLE);
                    setState(125);
                    match(T__5);
                    setState(126);
                    match(COMMA);
                    setState(127);
                    match(ID);
                    setState(128);
                    match(RANGLE);

                    System.out.println("[STATS] Skipping CR entry as the admin is 'FALSE'");

                }
                    break;
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class AdminContext extends ParserRuleContext {
        public Token u;
        public List<TerminalNode> ID() {
            return getTokens(MohawkParser.ID);
        }
        public TerminalNode SEMI() {
            return getToken(MohawkParser.SEMI, 0);
        }
        public TerminalNode ID(int i) {
            return getToken(MohawkParser.ID, i);
        }
        public AdminContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }
        @Override
        public int getRuleIndex() {
            return RULE_admin;
        }
        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MohawkListener) ((MohawkListener) listener).enterAdmin(this);
        }
        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MohawkListener) ((MohawkListener) listener).exitAdmin(this);
        }
    }

    public final AdminContext admin() throws RecognitionException {
        AdminContext _localctx = new AdminContext(_ctx, getState());
        enterRule(_localctx, 20, RULE_admin);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(132);
                match(T__8);
                setState(135);
                _errHandler.sync(this);
                _la = _input.LA(1);
                do {
                    {
                        {
                            setState(133);
                            ((AdminContext) _localctx).u = match(ID);

                            vAdmin.add(((AdminContext) _localctx).u.getText());

                        }
                    }
                    setState(137);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                } while (_la == ID);
                setState(139);
                match(SEMI);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class SpecContext extends ParserRuleContext {
        public Token su;
        public MyroleContext sr;
        public TerminalNode ID() {
            return getToken(MohawkParser.ID, 0);
        }
        public TerminalNode SEMI() {
            return getToken(MohawkParser.SEMI, 0);
        }
        public MyroleContext myrole() {
            return getRuleContext(MyroleContext.class, 0);
        }
        public SpecContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }
        @Override
        public int getRuleIndex() {
            return RULE_spec;
        }
        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MohawkListener) ((MohawkListener) listener).enterSpec(this);
        }
        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MohawkListener) ((MohawkListener) listener).exitSpec(this);
        }
    }

    public final SpecContext spec() throws RecognitionException {
        SpecContext _localctx = new SpecContext(_ctx, getState());
        enterRule(_localctx, 22, RULE_spec);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(141);
                match(T__2);
                setState(142);
                ((SpecContext) _localctx).su = match(ID);
                setState(143);
                ((SpecContext) _localctx).sr = myrole();
                setState(144);
                match(SEMI);

                addSpec(((SpecContext) _localctx).su.getText(), ((SpecContext) _localctx).sr.role);
                System.out.println("[STATS] Mohawk Input SPEC: user: '" + ((SpecContext) _localctx).su.getText()
                        + "' with role: '" + ((SpecContext) _localctx).sr.role + "'");

            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class MyroleContext extends ParserRuleContext {
        public String role;
        public Token a;
        public TerminalNode ID() {
            return getToken(MohawkParser.ID, 0);
        }
        public MyroleContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }
        @Override
        public int getRuleIndex() {
            return RULE_myrole;
        }
        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MohawkListener) ((MohawkListener) listener).enterMyrole(this);
        }
        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MohawkListener) ((MohawkListener) listener).exitMyrole(this);
        }
    }

    public final MyroleContext myrole() throws RecognitionException {
        MyroleContext _localctx = new MyroleContext(_ctx, getState());
        enterRule(_localctx, 24, RULE_myrole);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(147);
                ((MyroleContext) _localctx).a = match(ID);

                ((MyroleContext) _localctx).role = (((MyroleContext) _localctx).a != null
                        ? ((MyroleContext) _localctx).a.getText()
                        : null);

            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static final String _serializedATN = "\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\26\u0099\4\2\t\2"
            + "\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"
            + "\t\13\4\f\t\f\4\r\t\r\4\16\t\16\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\3\3"
            + "\3\3\3\6\3(\n\3\r\3\16\3)\3\3\3\3\3\3\3\4\3\4\3\4\6\4\62\n\4\r\4\16\4"
            + "\63\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\6\5?\n\5\r\5\16\5@\3\5\3\5\3\5"
            + "\3\6\3\6\7\6H\n\6\f\6\16\6K\13\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3"
            + "\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\b\7\b_\n\b\f\b\16\bb\13\b\3\b\5\be\n"
            + "\b\3\t\3\t\5\ti\n\t\3\t\3\t\3\t\3\n\3\n\7\np\n\n\f\n\16\ns\13\n\3\n\3"
            + "\n\3\n\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13"
            + "\5\13\u0085\n\13\3\f\3\f\3\f\6\f\u008a\n\f\r\f\16\f\u008b\3\f\3\f\3\r"
            + "\3\r\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3\16\2\2\17\2\4\6\b\n\f\16\20\22\24"
            + "\26\30\32\2\2\u0095\2\34\3\2\2\2\4$\3\2\2\2\6.\3\2\2\2\b\67\3\2\2\2\n"
            + "E\3\2\2\2\fO\3\2\2\2\16d\3\2\2\2\20h\3\2\2\2\22m\3\2\2\2\24\u0084\3\2"
            + "\2\2\26\u0086\3\2\2\2\30\u008f\3\2\2\2\32\u0095\3\2\2\2\34\35\5\4\3\2"
            + "\35\36\5\6\4\2\36\37\5\b\5\2\37 \5\22\n\2 !\5\n\6\2!\"\5\26\f\2\"#\5\30"
            + "\r\2#\3\3\2\2\2$\'\7\4\2\2%&\7\20\2\2&(\b\3\1\2\'%\3\2\2\2()\3\2\2\2)"
            + "\'\3\2\2\2)*\3\2\2\2*+\3\2\2\2+,\7\26\2\2,-\b\3\1\2-\5\3\2\2\2.\61\7\7"
            + "\2\2/\60\7\20\2\2\60\62\b\4\1\2\61/\3\2\2\2\62\63\3\2\2\2\63\61\3\2\2"
            + "\2\63\64\3\2\2\2\64\65\3\2\2\2\65\66\7\26\2\2\66\7\3\2\2\2\67>\7\5\2\2"
            + "89\7\21\2\29:\7\20\2\2:;\7\23\2\2;<\7\20\2\2<=\7\22\2\2=?\b\5\1\2>8\3"
            + "\2\2\2?@\3\2\2\2@>\3\2\2\2@A\3\2\2\2AB\3\2\2\2BC\7\26\2\2CD\b\5\1\2D\t"
            + "\3\2\2\2EI\7\13\2\2FH\5\f\7\2GF\3\2\2\2HK\3\2\2\2IG\3\2\2\2IJ\3\2\2\2"
            + "JL\3\2\2\2KI\3\2\2\2LM\7\26\2\2MN\b\6\1\2N\13\3\2\2\2OP\7\21\2\2PQ\7\20"
            + "\2\2QR\7\23\2\2RS\5\16\b\2ST\7\23\2\2TU\5\32\16\2UV\7\22\2\2VW\b\7\1\2"
            + "W\r\3\2\2\2XY\5\20\t\2Y`\b\b\1\2Z[\7\24\2\2[\\\5\20\t\2\\]\b\b\1\2]_\3"
            + "\2\2\2^Z\3\2\2\2_b\3\2\2\2`^\3\2\2\2`a\3\2\2\2ae\3\2\2\2b`\3\2\2\2ce\7"
            + "\n\2\2dX\3\2\2\2dc\3\2\2\2e\17\3\2\2\2fg\7\25\2\2gi\b\t\1\2hf\3\2\2\2"
            + "hi\3\2\2\2ij\3\2\2\2jk\5\32\16\2kl\b\t\1\2l\21\3\2\2\2mq\7\b\2\2np\5\24"
            + "\13\2on\3\2\2\2ps\3\2\2\2qo\3\2\2\2qr\3\2\2\2rt\3\2\2\2sq\3\2\2\2tu\7"
            + "\26\2\2uv\b\n\1\2v\23\3\2\2\2wx\7\21\2\2xy\7\20\2\2yz\7\23\2\2z{\5\32"
            + "\16\2{|\7\22\2\2|}\b\13\1\2}\u0085\3\2\2\2~\177\7\21\2\2\177\u0080\7\6"
            + "\2\2\u0080\u0081\7\23\2\2\u0081\u0082\7\20\2\2\u0082\u0083\7\22\2\2\u0083"
            + "\u0085\b\13\1\2\u0084w\3\2\2\2\u0084~\3\2\2\2\u0085\25\3\2\2\2\u0086\u0089"
            + "\7\3\2\2\u0087\u0088\7\20\2\2\u0088\u008a\b\f\1\2\u0089\u0087\3\2\2\2"
            + "\u008a\u008b\3\2\2\2\u008b\u0089\3\2\2\2\u008b\u008c\3\2\2\2\u008c\u008d"
            + "\3\2\2\2\u008d\u008e\7\26\2\2\u008e\27\3\2\2\2\u008f\u0090\7\t\2\2\u0090"
            + "\u0091\7\20\2\2\u0091\u0092\5\32\16\2\u0092\u0093\7\26\2\2\u0093\u0094"
            + "\b\r\1\2\u0094\31\3\2\2\2\u0095\u0096\7\20\2\2\u0096\u0097\b\16\1\2\u0097"
            + "\33\3\2\2\2\f)\63@I`dhq\u0084\u008b";
    public static final ATN _ATN = new ATNDeserializer().deserialize(_serializedATN.toCharArray());
    static {
        _decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
        for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
            _decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
        }
    }
}
