grammar Mohawk;

/* Convert Mohawk testcase into the RBAC internal format */

@header {
package mohawk.global.parser.mohawk.v1;

import java.util.Vector;
import java.util.Stack;
import java.util.HashMap;
import java.util.Map;
import mohawk.global.pieces.mohawk.PreCondProcessorInt;
import mohawk.global.pieces.mohawk.CAEntry;
import mohawk.global.pieces.mohawk.CREntry;
import mohawk.global.pieces.mohawk.PreCondition;
}

@members {
	public Vector<String> vRoles;
	public Vector<String> vUsers;
	public Vector<String> vAdmin;
	public Map<Integer, String> mRoleIndex;
	public Map<String, Integer> mRole2Index;
	public Map<Integer, String> mUserIndex;
	public Map<String,Vector<Integer>> mUA;
	public Map<String,Vector<CREntry>> mCR;
	public Map<String,Vector<CAEntry>> mCA;	
	public PreCondProcessorInt preCndP;
	public Stack<Integer> stackOperators;
	public Vector<String> vSpec; // This vector holds two strings - user and role that will be used in the LTL formulae
	
	// Indices for user and roles while parsing
	// Each user has an index corresponding to the order in which the name appears in the list.
	public int iRoleIndex = 0;
	public int iUserIndex = 0;
	
	// Counters
	public int numUA = 0;
	public int numCARules = 0;
	public int numCRRules = 0;
	

	public void initRbac() {
		vRoles = new Vector<String>();
		vUsers = new Vector<String>();
		vAdmin = new Vector<String>();
		mRoleIndex = new HashMap<Integer, String>();
		mRole2Index = new HashMap<String,Integer>();
		mUserIndex = new HashMap<Integer, String>();
		mUA = new HashMap<String,Vector<Integer>>();
		mCR = new HashMap<String,Vector<CREntry>>();
		mCA = new HashMap<String,Vector<CAEntry>>();
		vSpec = new Vector<String>();		
	}
	
	/*
	public RBACInstance getRBAC() {
		return new RBACInstance(vRoles, vUsers, vAdmin, mUA, mCR, mCA,vSpec);
	}
	*/
	
	public void setUA(String strUser, String strRole) {
	
		Vector<Integer> vUserUA = mUA.get(strUser);		
		if(vUserUA == null)
		{
			vUserUA = new Vector<Integer>();
			mUA.put(strUser,vUserUA);
		}
		
		int iRoleIndex = mRole2Index.get(strRole); //getMapKey(mRoleIndex, strRole);		
		vUserUA.add(iRoleIndex);
	}
	
	public void addCREntry(String inStrPreCond, String inStrRole) {
		
		CREntry crEntry = new CREntry(inStrPreCond, inStrRole);
		Vector<CREntry> vCR = mCR.get(inStrRole);
		
		if(vCR == null)
			vCR = new Vector<CREntry>();
		
		vCR.add(new CREntry(inStrPreCond, inStrRole));
		mCR.put(inStrRole,vCR);
	}
	
	public void addCAEntry(String inStrAdminRole, PreCondition pcPreCond, String inStrRole) {
		
		CAEntry caEntry = new CAEntry(inStrAdminRole, pcPreCond, inStrRole);
		Vector<CAEntry> vCA = mCA.get(inStrRole);
		
		if(vCA == null)
			vCA = new Vector<CAEntry>();
		vCA.add(caEntry);
		mCA.put(inStrRole,vCA);
	}
	/*	
	private int getMapKey(Map<Integer,String> inMap, String inString) {
		
		for(int i=0; i<inMap.size(); i++) {
			
			if(inMap.get(i).equals(inString)) {
				return i;
			}
		}
		
		System.out.println("Error - BTree::getMapIndex - Value not found in map");
		return 0;
	}
	*/
	
	public void addSpec(String inStrUser, String inStrRole) {
	
		vSpec.add(inStrUser);
		vSpec.add(inStrRole);
	}
}

/* Rules */
init
:
	roles users ua cr ca admin spec
;

roles
:
	'Roles'
	(
		r=ID
		{
			vRoles.add($r.getText()); 
            mRoleIndex.put(iRoleIndex,$r.getText()); 
            mRole2Index.put($r.getText(),iRoleIndex);
            iRoleIndex++;
        }
	)+ SEMI {
		System.out.println("[STATS] Mohawk Input Roles: " + iRoleIndex);
	}
;

users
:
	(
		'Users'
	)
	(
		u = ID
		{
			vUsers.add($u.getText()); 
            mUserIndex.put(iUserIndex,$u.getText());
            iUserIndex++;
		}
	)+ SEMI
;

ua
:
	'UA'
	(
		LANGLE x = ID COMMA y = ID RANGLE
		{
			setUA($x.getText(),$y.getText());
			numUA++;
		}
	)+ SEMI {
		System.out.println("[STATS] Mohawk Input UA: " + numUA);
	}
;

ca
:
	'CA'
	(
		caentry
	)* SEMI {
		System.out.println("[STATS] Mohawk Input CA Rules: " + numCARules);
	}
;

caentry
:
	LANGLE d = ID COMMA pre = precondition COMMA f = myrole RANGLE
	{
		try {
		  	PreCondition pcPreCond = $pre.p.result(); 
		  	addCAEntry($d.getText(), pcPreCond, $f.role);
		  	numCARules++;
  		}catch(Exception e) {
	  		e.printStackTrace();
	  	}	
	}

;

precondition returns [PreCondProcessorInt p] @init {
	$p = new PreCondProcessorInt(mRole2Index);
}
:
	a = rolecondition
	{
		try {
			if($a.not) {
				$p.addNeg($a.name);
			} else {
				$p.add($a.name);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	(
		COND b = rolecondition
		{
			try {
				if($b.not) {
					$p.addNeg($b.name);
				} else {
					$p.add($b.name);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	)*
	| 'TRUE'
;

rolecondition returns [Boolean not, String name] @init {
	$not = false;
}
:
	(
		NOT
		{$not = true;}

	)? r = myrole
	{
		$name = $r.role;
	}

;

cr
:
	'CR'
	(
		crentry
	)* SEMI {
		System.out.println("[STATS] Mohawk Input CR Rules: " + numCRRules);
	}
;

crentry
:
	LANGLE mm = ID COMMA nn = myrole RANGLE
	{
		addCREntry($mm.getText(),$nn.role);
	}

	| LANGLE 'FALSE' COMMA ID RANGLE
	{
		System.out.println("[STATS] Skipping CR entry as the admin is 'FALSE'");
	}

;

admin
:
	'ADMIN'
	(
		u=ID
		{
			vAdmin.add($u.getText());
		}
	)+ SEMI
;

spec
:
	'SPEC' su = ID sr = myrole SEMI
	{
		addSpec($su.getText(),$sr.role);
		System.out.println("[STATS] Mohawk Input SPEC: user: '" + $su.getText() + "' with role: '"+ $sr.role +"'");
	}

;

myrole returns [String role]
:
	a = ID
	{
		$role = $a.text;
	}

;

/* Whitespace and Comments (Must be above tokens) */
Whitespace
:
	[ \t]+ -> skip
;

Newline
:
	(
		'\r' '\n'?
		| '\n'
	) -> skip
;

BlockComment
:
	'/*' .*? '*/' -> skip
;

LineComment
:
	'//' ~[\r\n]* -> skip
;

/* TOKENS */
ID
:
	(
		'a' .. 'z'
		| 'A' .. 'Z'
		| '_'
	)
	(
		'a' .. 'z'
		| 'A' .. 'Z'
		| '0' .. '9'
		| '_'
	)*
;

LANGLE
:
	'<'
;

RANGLE
:
	'>'
;

COMMA
:
	','
;

COND
:
	'&'
;

NOT
:
	'-'
;

SEMI
:
	';'
;