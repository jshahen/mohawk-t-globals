// Generated from D:\Work\Masters\workspace (GradSchool)\Mohawk-T Globals\parsers\mohawk\global\parser\mohawk\v1\Mohawk.g4 by ANTLR 4.4

package mohawk.global.parser.mohawk.v1;

import java.util.Vector;
import java.util.Stack;
import java.util.HashMap;
import java.util.Map;
import mohawk.global.pieces.mohawk.PreCondProcessorInt;
import mohawk.global.pieces.mohawk.CAEntry;
import mohawk.global.pieces.mohawk.CREntry;
import mohawk.global.pieces.mohawk.PreCondition;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/** This interface defines a complete listener for a parse tree produced by
 * {@link MohawkParser}. */
public interface MohawkListener extends ParseTreeListener {
    /** Enter a parse tree produced by {@link MohawkParser#init}.
     * 
     * @param ctx the parse tree */
    void enterInit(@NotNull MohawkParser.InitContext ctx);
    /** Exit a parse tree produced by {@link MohawkParser#init}.
     * 
     * @param ctx the parse tree */
    void exitInit(@NotNull MohawkParser.InitContext ctx);
    /** Enter a parse tree produced by {@link MohawkParser#caentry}.
     * 
     * @param ctx the parse tree */
    void enterCaentry(@NotNull MohawkParser.CaentryContext ctx);
    /** Exit a parse tree produced by {@link MohawkParser#caentry}.
     * 
     * @param ctx the parse tree */
    void exitCaentry(@NotNull MohawkParser.CaentryContext ctx);
    /** Enter a parse tree produced by {@link MohawkParser#roles}.
     * 
     * @param ctx the parse tree */
    void enterRoles(@NotNull MohawkParser.RolesContext ctx);
    /** Exit a parse tree produced by {@link MohawkParser#roles}.
     * 
     * @param ctx the parse tree */
    void exitRoles(@NotNull MohawkParser.RolesContext ctx);
    /** Enter a parse tree produced by {@link MohawkParser#admin}.
     * 
     * @param ctx the parse tree */
    void enterAdmin(@NotNull MohawkParser.AdminContext ctx);
    /** Exit a parse tree produced by {@link MohawkParser#admin}.
     * 
     * @param ctx the parse tree */
    void exitAdmin(@NotNull MohawkParser.AdminContext ctx);
    /** Enter a parse tree produced by {@link MohawkParser#precondition}.
     * 
     * @param ctx the parse tree */
    void enterPrecondition(@NotNull MohawkParser.PreconditionContext ctx);
    /** Exit a parse tree produced by {@link MohawkParser#precondition}.
     * 
     * @param ctx the parse tree */
    void exitPrecondition(@NotNull MohawkParser.PreconditionContext ctx);
    /** Enter a parse tree produced by {@link MohawkParser#ua}.
     * 
     * @param ctx the parse tree */
    void enterUa(@NotNull MohawkParser.UaContext ctx);
    /** Exit a parse tree produced by {@link MohawkParser#ua}.
     * 
     * @param ctx the parse tree */
    void exitUa(@NotNull MohawkParser.UaContext ctx);
    /** Enter a parse tree produced by {@link MohawkParser#users}.
     * 
     * @param ctx the parse tree */
    void enterUsers(@NotNull MohawkParser.UsersContext ctx);
    /** Exit a parse tree produced by {@link MohawkParser#users}.
     * 
     * @param ctx the parse tree */
    void exitUsers(@NotNull MohawkParser.UsersContext ctx);
    /** Enter a parse tree produced by {@link MohawkParser#spec}.
     * 
     * @param ctx the parse tree */
    void enterSpec(@NotNull MohawkParser.SpecContext ctx);
    /** Exit a parse tree produced by {@link MohawkParser#spec}.
     * 
     * @param ctx the parse tree */
    void exitSpec(@NotNull MohawkParser.SpecContext ctx);
    /** Enter a parse tree produced by {@link MohawkParser#cr}.
     * 
     * @param ctx the parse tree */
    void enterCr(@NotNull MohawkParser.CrContext ctx);
    /** Exit a parse tree produced by {@link MohawkParser#cr}.
     * 
     * @param ctx the parse tree */
    void exitCr(@NotNull MohawkParser.CrContext ctx);
    /** Enter a parse tree produced by {@link MohawkParser#crentry}.
     * 
     * @param ctx the parse tree */
    void enterCrentry(@NotNull MohawkParser.CrentryContext ctx);
    /** Exit a parse tree produced by {@link MohawkParser#crentry}.
     * 
     * @param ctx the parse tree */
    void exitCrentry(@NotNull MohawkParser.CrentryContext ctx);
    /** Enter a parse tree produced by {@link MohawkParser#rolecondition}.
     * 
     * @param ctx the parse tree */
    void enterRolecondition(@NotNull MohawkParser.RoleconditionContext ctx);
    /** Exit a parse tree produced by {@link MohawkParser#rolecondition}.
     * 
     * @param ctx the parse tree */
    void exitRolecondition(@NotNull MohawkParser.RoleconditionContext ctx);
    /** Enter a parse tree produced by {@link MohawkParser#myrole}.
     * 
     * @param ctx the parse tree */
    void enterMyrole(@NotNull MohawkParser.MyroleContext ctx);
    /** Exit a parse tree produced by {@link MohawkParser#myrole}.
     * 
     * @param ctx the parse tree */
    void exitMyrole(@NotNull MohawkParser.MyroleContext ctx);
    /** Enter a parse tree produced by {@link MohawkParser#ca}.
     * 
     * @param ctx the parse tree */
    void enterCa(@NotNull MohawkParser.CaContext ctx);
    /** Exit a parse tree produced by {@link MohawkParser#ca}.
     * 
     * @param ctx the parse tree */
    void exitCa(@NotNull MohawkParser.CaContext ctx);
}
