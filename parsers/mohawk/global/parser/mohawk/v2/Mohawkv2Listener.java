// Generated from D:\Work\Masters\workspace (GradSchool)\Mohawk-T Globals\parsers\mohawk\global\parser\mohawk\v2\Mohawkv2.g4 by ANTLR 4.4

package mohawk.global.parser.mohawk.v2;

import java.util.*;
import java.util.logging.*;

import org.apache.commons.lang3.StringUtils;
import mohawk.global.pieces.*;
import mohawk.global.helper.*;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/** This interface defines a complete listener for a parse tree produced by
 * {@link Mohawkv2Parser}. */
public interface Mohawkv2Listener extends ParseTreeListener {
    /** Enter a parse tree produced by {@link Mohawkv2Parser#init}.
     * 
     * @param ctx the parse tree */
    void enterInit(@NotNull Mohawkv2Parser.InitContext ctx);
    /** Exit a parse tree produced by {@link Mohawkv2Parser#init}.
     * 
     * @param ctx the parse tree */
    void exitInit(@NotNull Mohawkv2Parser.InitContext ctx);
    /** Enter a parse tree produced by {@link Mohawkv2Parser#caentry}.
     * 
     * @param ctx the parse tree */
    void enterCaentry(@NotNull Mohawkv2Parser.CaentryContext ctx);
    /** Exit a parse tree produced by {@link Mohawkv2Parser#caentry}.
     * 
     * @param ctx the parse tree */
    void exitCaentry(@NotNull Mohawkv2Parser.CaentryContext ctx);
    /** Enter a parse tree produced by {@link Mohawkv2Parser#roles}.
     * 
     * @param ctx the parse tree */
    void enterRoles(@NotNull Mohawkv2Parser.RolesContext ctx);
    /** Exit a parse tree produced by {@link Mohawkv2Parser#roles}.
     * 
     * @param ctx the parse tree */
    void exitRoles(@NotNull Mohawkv2Parser.RolesContext ctx);
    /** Enter a parse tree produced by {@link Mohawkv2Parser#admin}.
     * 
     * @param ctx the parse tree */
    void enterAdmin(@NotNull Mohawkv2Parser.AdminContext ctx);
    /** Exit a parse tree produced by {@link Mohawkv2Parser#admin}.
     * 
     * @param ctx the parse tree */
    void exitAdmin(@NotNull Mohawkv2Parser.AdminContext ctx);
    /** Enter a parse tree produced by {@link Mohawkv2Parser#precondition}.
     * 
     * @param ctx the parse tree */
    void enterPrecondition(@NotNull Mohawkv2Parser.PreconditionContext ctx);
    /** Exit a parse tree produced by {@link Mohawkv2Parser#precondition}.
     * 
     * @param ctx the parse tree */
    void exitPrecondition(@NotNull Mohawkv2Parser.PreconditionContext ctx);
    /** Enter a parse tree produced by {@link Mohawkv2Parser#ua}.
     * 
     * @param ctx the parse tree */
    void enterUa(@NotNull Mohawkv2Parser.UaContext ctx);
    /** Exit a parse tree produced by {@link Mohawkv2Parser#ua}.
     * 
     * @param ctx the parse tree */
    void exitUa(@NotNull Mohawkv2Parser.UaContext ctx);
    /** Enter a parse tree produced by {@link Mohawkv2Parser#users}.
     * 
     * @param ctx the parse tree */
    void enterUsers(@NotNull Mohawkv2Parser.UsersContext ctx);
    /** Exit a parse tree produced by {@link Mohawkv2Parser#users}.
     * 
     * @param ctx the parse tree */
    void exitUsers(@NotNull Mohawkv2Parser.UsersContext ctx);
    /** Enter a parse tree produced by {@link Mohawkv2Parser#spec}.
     * 
     * @param ctx the parse tree */
    void enterSpec(@NotNull Mohawkv2Parser.SpecContext ctx);
    /** Exit a parse tree produced by {@link Mohawkv2Parser#spec}.
     * 
     * @param ctx the parse tree */
    void exitSpec(@NotNull Mohawkv2Parser.SpecContext ctx);
    /** Enter a parse tree produced by {@link Mohawkv2Parser#cr}.
     * 
     * @param ctx the parse tree */
    void enterCr(@NotNull Mohawkv2Parser.CrContext ctx);
    /** Exit a parse tree produced by {@link Mohawkv2Parser#cr}.
     * 
     * @param ctx the parse tree */
    void exitCr(@NotNull Mohawkv2Parser.CrContext ctx);
    /** Enter a parse tree produced by {@link Mohawkv2Parser#crentry}.
     * 
     * @param ctx the parse tree */
    void enterCrentry(@NotNull Mohawkv2Parser.CrentryContext ctx);
    /** Exit a parse tree produced by {@link Mohawkv2Parser#crentry}.
     * 
     * @param ctx the parse tree */
    void exitCrentry(@NotNull Mohawkv2Parser.CrentryContext ctx);
    /** Enter a parse tree produced by {@link Mohawkv2Parser#rolecondition}.
     * 
     * @param ctx the parse tree */
    void enterRolecondition(@NotNull Mohawkv2Parser.RoleconditionContext ctx);
    /** Exit a parse tree produced by {@link Mohawkv2Parser#rolecondition}.
     * 
     * @param ctx the parse tree */
    void exitRolecondition(@NotNull Mohawkv2Parser.RoleconditionContext ctx);
    /** Enter a parse tree produced by {@link Mohawkv2Parser#myrole}.
     * 
     * @param ctx the parse tree */
    void enterMyrole(@NotNull Mohawkv2Parser.MyroleContext ctx);
    /** Exit a parse tree produced by {@link Mohawkv2Parser#myrole}.
     * 
     * @param ctx the parse tree */
    void exitMyrole(@NotNull Mohawkv2Parser.MyroleContext ctx);
    /** Enter a parse tree produced by {@link Mohawkv2Parser#ca}.
     * 
     * @param ctx the parse tree */
    void enterCa(@NotNull Mohawkv2Parser.CaContext ctx);
    /** Exit a parse tree produced by {@link Mohawkv2Parser#ca}.
     * 
     * @param ctx the parse tree */
    void exitCa(@NotNull Mohawkv2Parser.CaContext ctx);
}
