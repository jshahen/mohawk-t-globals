package mohawk.global.testing;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.BeforeClass;
import org.junit.Test;

import mohawk.global.helper.RoleHelper;
import mohawk.global.pieces.*;

public class RoleHelperTests {
    public static Boolean printOut = true;

    public static RoleHelper helper = new RoleHelper();
    public static ArrayList<Rule> rules = new ArrayList<Rule>();

    public static Rule rule1;
    public static Rule rule2;
    public static Rule rule3;
    public static Rule rule4;
    public static Rule rule5;
    public static Rule rule6;

    public static Role admin = new Role("admin");
    public static Role cond1 = new Role("cond1");
    public static Role cond2 = new Role("cond2");
    public static Role cond3 = new Role("cond3", "", true);
    public static Role goal = new Role("goal");

    @BeforeClass
    public static void setup() {
        /*helper.add(cond1);
        helper.add(cond2);
        helper.add(cond3);
        helper.add(goal);
        helper.add(admin);
        */

        rule6 = new Rule(RuleType.ASSIGN, Role.allRoles(), new TimeInterval(0), new ArrayList<Role>(),
                new ArrayList<TimeSlot>(Arrays.asList(new TimeSlot(0))), cond2);
        rule1 = new Rule(RuleType.ASSIGN, Role.allRoles(), new TimeInterval(0), new ArrayList<Role>(),
                new ArrayList<TimeSlot>(Arrays.asList(new TimeSlot(0))), admin);
        rule2 = new Rule(RuleType.ENABLE, Role.allRoles(), new TimeInterval(0), new ArrayList<Role>(),
                new ArrayList<TimeSlot>(Arrays.asList(new TimeSlot(0))), admin);
        rule3 = new Rule(RuleType.DISABLE, admin, new TimeInterval(0), new ArrayList<Role>(),
                new ArrayList<TimeSlot>(Arrays.asList(new TimeSlot(0))), cond1);
        rule4 = new Rule(RuleType.ASSIGN, admin, new TimeInterval(0), new ArrayList<Role>(Arrays.asList(cond1)),
                new ArrayList<TimeSlot>(Arrays.asList(new TimeSlot(0))), goal);

        rule5 = new Rule(RuleType.ENABLE, admin, new TimeInterval(0),
                new ArrayList<Role>(Arrays.asList(cond1, cond2, cond3)),
                new ArrayList<TimeSlot>(Arrays.asList(new TimeSlot(0))), goal);

        rules.add(rule1);
        rules.add(rule2);
        rules.add(rule3);
        rules.add(rule4);
        rules.add(rule5);
        rules.add(rule6);

        helper.add(rule6);
        helper.add(rule1);
        helper.add(rule2);
        helper.add(rule3);
        helper.add(rule4);
        helper.add(rule5);

        helper.allowZeroRole = true;
        helper.setupSortedRoles();
    }

    @Test
    public void removeEnableTests() {
        DebugMsg.header(printOut);
        DebugMsg.msg(printOut, "removeEnableTests()");
        DebugMsg.header(printOut);
        DebugMsg.msg(printOut, "Rules: " + rules);
        DebugMsg.msg(printOut, "Reduced Roles: " + helper._hashedRoles);

        ArrayList<Rule> enabledRemovedRules = helper.removeEnableDisableRules(rules);
        helper.setupSortedRoles();
        DebugMsg.msg(printOut, "Reduced Roles 2: " + helper._hashedRoles);

        DebugMsg.msg(printOut, "Enabled Removed Rules: " + enabledRemovedRules);
        DebugMsg.msg(printOut, "Rules: " + rules);

        for (Rule r : enabledRemovedRules) {
            assertThat(r._type, not(equalTo(RuleType.ENABLE)));
            assertThat(r._type, not(equalTo(RuleType.DISABLE)));
        }
    }

    @Test
    public void removeEnableTestsv2() {
        DebugMsg.header(printOut);
        DebugMsg.msg(printOut, "removeEnableTestsv2()");
        DebugMsg.header(printOut);
        DebugMsg.msg(printOut, "Rules: " + rules);
        DebugMsg.msg(printOut, "Reduced Roles: " + helper.getSet());

        ArrayList<Rule> enabledRemovedRules = helper.removeEnableDisableRulesv2(rules);
        DebugMsg.msg(printOut, "Reduced Roles 2: " + helper.getSet());

        DebugMsg.msg(printOut, "Enabled Removed Rules: " + enabledRemovedRules);
        DebugMsg.msg(printOut, "Rules: " + rules);

        for (Rule r : enabledRemovedRules) {
            assertThat(r._type, not(equalTo(RuleType.ENABLE)));
            assertThat(r._type, not(equalTo(RuleType.DISABLE)));
        }
    }

    @Test
    public void changeToEnabled() {
        DebugMsg.header(printOut);
        DebugMsg.msg(printOut, "changeToEnabled()");
        DebugMsg.header(printOut);
        DebugMsg.msg(printOut, "Rule: " + rule5);
        // Convert target role to their 'enabled' form
        DebugMsg.msg(printOut, "changeType(RoleType.ENABLED): " + rule5._role.changeType(RoleType.ENABLED));

        assertEquals(rule5._role.getType(), RoleType.ENABLED);

        // Convert all precondition roles to their 'enabled' form
        DebugMsg.msg(printOut, "rolesEnabled(): " + rule5.rolesEnabled());

        for (Role r : rule5._preconditions) {
            assertEquals(r.getType(), RoleType.ENABLED);
        }

        // Adds any additional precondition roles if there appears a NOT role
        DebugMsg.msg(printOut, "rolesNotEnabledCombined(): " + rule5.rolesNotEnabledCombined());

        DebugMsg.msg(printOut, "Rule: " + rule5);
    }

    @Test
    public void roleHelperConstructor() {
        DebugMsg.header(printOut);
        DebugMsg.msg(printOut, "roleHelperConstructor()");
        DebugMsg.header(printOut);

        DebugMsg.msg(printOut, "Original Roles: " + helper);
        RoleHelper copy = new RoleHelper(helper);
        DebugMsg.msg(printOut, "Copy Roles: " + copy);

        assertEquals(helper.numberOfRoles(), copy.numberOfRoles());

        for (Role r : helper.getSet()) {
            assertNotNull(copy.indexOf(r));
        }
    }

    @Test
    public void testSetupSortedRoles() {
        DebugMsg.header(printOut);
        DebugMsg.msg(printOut, "testSetupSortedRoles()");
        DebugMsg.header(printOut);

        helper.allowZeroRole = true;
        helper.setupSortedRoles();

        assertEquals((int) 0, (int) helper.indexOf(admin));

        DebugMsg.msg(printOut, "helper._hashedRoles = " + helper._hashedRoles);

    }
}
