package mohawk.global.testing;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;

import mohawk.global.pieces.*;

public class MohawkTParserTests {

    @Test
    public void testAdminRoleSet() {
        TimeSlot t1 = new TimeSlot(1);
        Role role1 = new Role("r1");
        Role role2 = new Role("r2");

        Rule r1 = new Rule(RuleType.ASSIGN, new Role("r1"), t1, new ArrayList<Role>(Arrays.asList(role1)),
                new ArrayList<TimeSlot>(Arrays.asList(t1)), role2);
        Rule r2 = new Rule(RuleType.ENABLE, new Role("r1", "", true), t1, new ArrayList<Role>(Arrays.asList(role1)),
                new ArrayList<TimeSlot>(Arrays.asList(t1)), role1);
        Rule r3 = new Rule(RuleType.REVOKE, new Role("r2", "", true), t1, new ArrayList<Role>(Arrays.asList(role1)),
                new ArrayList<TimeSlot>(Arrays.asList(t1)), role1);
        Rule r4 = new Rule(RuleType.REVOKE, role1, t1, new ArrayList<Role>(Arrays.asList(role1)),
                new ArrayList<TimeSlot>(Arrays.asList(t1)), role1);

        MohawkT m = new MohawkT();

        m.addRule(r1);
        assertEquals(new Integer(1), m.roleHelper.numberOfAdminRoles());
        assertTrue(m.roleHelper.getAdminRoles().contains(role1));
        assertTrue(m.roleHelper.getAdminRoles().contains(new Role("r1")));
        assertFalse(m.roleHelper.getAdminRoles().contains(new Role("r2")));

        m.addRule(r2);
        assertEquals(new Integer(1), m.roleHelper.numberOfAdminRoles());
        assertTrue(m.roleHelper.getAdminRoles().contains(role1));
        assertTrue(m.roleHelper.getAdminRoles().contains(new Role("r1")));
        assertFalse(m.roleHelper.getAdminRoles().contains(new Role("r2")));

        m.addRule(r3);
        assertEquals(new Integer(2), m.roleHelper.numberOfAdminRoles());
        assertTrue(m.roleHelper.getAdminRoles().contains(role1));
        assertTrue(m.roleHelper.getAdminRoles().contains(new Role("r1")));
        assertTrue(m.roleHelper.getAdminRoles().contains(new Role("r2")));

        m.addRule(r4);
        assertEquals(new Integer(2), m.roleHelper.numberOfAdminRoles());
        assertTrue(m.roleHelper.getAdminRoles().contains(role1));
        assertTrue(m.roleHelper.getAdminRoles().contains(new Role("r1")));
        assertTrue(m.roleHelper.getAdminRoles().contains(new Role("r2")));
    }
}
