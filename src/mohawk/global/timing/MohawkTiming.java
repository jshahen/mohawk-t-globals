package mohawk.global.timing;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

public class MohawkTiming {
    public final static Logger logger = Logger.getLogger("mohawk");
    private Map<String, TimingEvent> timings = new Hashtable<String, TimingEvent>();
    private String lastFinishedTimer = null;
    public Boolean printOnStop = false;
    public DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

    public String[] heading = new String[]{"Category", "Start Time", "Start Milliseconds", "Finish Time",
            "Finish Milliseconds", "Timer Key", "Duration (ms)", "Comment"};

    public Map<String, TimingEvent> getTimings() {
        return timings;
    }

    public void blankTimer(String key) {
        timings.put(key, new TimingEvent());
    }

    public void startTimer(String key) {
        TimingEvent t = timings.get(key);
        if (t == null) {
            t = new TimingEvent();
        }
        t.setStartTimeNow();
        timings.put(key, t);
    }

    public Boolean stopTimer(String key) {
        TimingEvent t = timings.get(key);
        if (t != null) {
            t.setFinishTimeNow();
            timings.put(key, t);
            lastFinishedTimer = key;

            if (printOnStop) {
                System.out.println(
                        "[TIMER STOPPED @ " + dateFormat.format(new Date()) + "] " + key + ": " + t.duration() + " ms");
            }

            return true;
        }
        return false;
    }

    public String getLastFinishedTimer() {
        return lastFinishedTimer;
    }

    /** Used to indicate that the operation failed after a certain amount of time. The timing event's
     * {@link TimingEvent#failed} flag will be set to TRUE and the finish time will be set to NOW using \
     * {@link TimingEvent#setFinishTimeNow()}.<br>
     * <b>NOTE:</b> This only occurs if the timing event has not finished yet, if it was finished previously then this
     * function performs no actions.
     * 
     * @param key
     *            the exact timer key to cancel */
    public Boolean cancelTimer(String key) {
        TimingEvent t = timings.get(key);
        if (t != null) {
            if (!t.hasFinished()) {
                t.setFinishTimeNow();
                t.failed();
                timings.put(key, t);
                lastFinishedTimer = key;

                if (printOnStop) {
                    System.out.println(key + ": " + t.duration() + " ms");
                }
            } else {
                if (printOnStop) {
                    System.out.println("[Key was already stopped] " + key + ": " + t.duration() + " ms");
                }
            }

            return true;
        }
        return false;
    }

    /** This function will find all timers with the prefix {@code timerPrefix} and cancel them. This function is to
     * be used in the case of a class crashes of being forced to exit without cleaning up its timers.
     * 
     * @param timerPrefix */
    public void cancelAll(String timerPrefix) {
        for (String key : timings.keySet()) {
            if (key.startsWith(timerPrefix)) {
                cancelTimer(key);
            }
        }
    }

    public void removeTimer(String key) {
        timings.remove(key);
    }

    public Boolean writeOut(File results) throws IOException {
        return writeOut(results, false);
    }

    public Boolean writeOut(File results, Boolean forceOverwrite) throws IOException {
        Boolean writeHeader = forceOverwrite;
        if (!results.exists()) {
            if (!results.createNewFile()) {
                logger.warning("[TIMING] Unable to create the file: " + results.getAbsolutePath());
                return false;
            }
            writeHeader = true;
        }

        if (!results.isFile()) {
            logger.warning("[TIMING] Parameter pointing to something that is not a file: " + results.getAbsolutePath());
            return false;
        }

        FileWriter fw = new FileWriter(results, !forceOverwrite);

        if (writeHeader) {
            writeHeader(fw);
        }

        for (Map.Entry<String, TimingEvent> entry : timings.entrySet()) {
            if (writeOutSingle(fw, entry.getKey()) == false) {
                logger.warning("[TIMING] Unable to write the row: " + entry.toString());
                return false;
            }
        }
        fw.close();

        return true;
    }

    public Boolean writeOutLast(FileWriter fw) throws IOException {
        return writeOutSingle(fw, lastFinishedTimer);
    }

    public Boolean writeOutSingle(FileWriter fw, String key) throws IOException {
        TimingEvent value = timings.get(key);

        if (value == null) { return false; }

        try {
            fw.write(value.category + "," + value.startTime + "," + value.startTimeMilliSec + "," + value.finishTime
                    + "," + value.finishTimeMilliSec + "," + StringEscapeUtils.escapeCsv(key) + "," + value.duration()
                    + "," + value.comment + "\n");
        } catch (NullPointerException e) {
            System.out.println("[ERROR] NullPointer when trying to access: " + key);
            logger.severe("[ERROR] NullPointer when trying to access: " + key);
        }
        fw.flush(); // flushes just in-case this function is run once in a while!
        return true;
    }

    public void writeHeader(FileWriter fw) throws IOException {
        fw.write(StringUtils.join(heading, ",") + "\n");
    }

    @Override
    public String toString() {
        StringBuilder t = new StringBuilder();
        int i = 0;
        String s;
        for (Map.Entry<String, TimingEvent> entry : timings.entrySet()) {
            t.append(entry.getKey() + ": ");
            try {
                s = entry.getValue().toString();
            } catch (NullPointerException e) {
                s = "Not Finished";
            }
            t.append(s);

            i++;
            if (i != timings.size()) {
                t.append(" - ");
            }
        }
        return t.toString();
    }

    /** Returns the Elapsed Time in Seconds (with decimal places)
     * 
     * @return */
    public Double getLastElapsedTimeSec() {
        TimingEvent te = timings.get(getLastFinishedTimer());

        return te.durationSec();
    }

    /** Return the Elapsed Time in Milliseconds
     * 
     * @return */
    public Long getLastElapsedTime() {
        TimingEvent te = timings.get(getLastFinishedTimer());

        return te.duration();
    }
}
