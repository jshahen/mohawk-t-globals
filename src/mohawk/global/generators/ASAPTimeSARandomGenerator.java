package mohawk.global.generators;

import java.util.Random;
import java.util.logging.Logger;

import mohawk.global.pieces.RuleType;
import mohawk.global.pieces.reduced.ASAPTimeSA;
import mohawk.global.pieces.reduced.rules.ASAPTimeSA_Rule;

public class ASAPTimeSARandomGenerator {
    public static final Logger logger = Logger.getLogger("mohawk");
    public static final String VERSION = "v1.0";

    public ASAPTimeSA lastTestcase;
    public String rolePrefix = "role";
    /** Changes the generation of Preconditions
     * 
     * {equallyRandom, 1/2Empty, 3/5Empty} */
    public String preconditionGen = "equallyRandom";

    /** Returns a pseudo-random number between min and max, inclusive. 
     * The difference between min and max can be at most <code>Integer.MAX_VALUE - 1</code>.
     *
     * @param min Minimum value
     * @param max Maximum value. Must be greater than min.
     * @return Integer between min and max, inclusive.
     * @see java.util.Random#nextInt(int) 
     */
    public static int randInt(int min, int max) {

        // NOTE: Usually this should be a field rather than a method
        // variable so that it is not re-seeded every call.
        Random rand = new Random();

        // nextInt is normally exclusive of the top value, so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }

    public ASAPTimeSA generate(Integer NUMBER_OF_ROLES, Integer NUMBER_OF_TIME_SLOTS, Integer NUMBER_OF_RULES) {
        lastTestcase = new ASAPTimeSA("ASAPTime SA RandomGenerator " + VERSION);
        lastTestcase.numberOfRoles = NUMBER_OF_ROLES;
        lastTestcase.numberOfTimeslots = NUMBER_OF_TIME_SLOTS;

        switch (preconditionGen) {
            case "1/2Empty" :
                logger.info("[GENERATOR] Using a probability of 1/2 of getting 0, 1/4 for 1, and 1/4 for -1");
                break;
            case "3/5Empty" :
                logger.info("[GENERATOR] Using a probability of 3/5 of getting 0, 1/5 for 1, and 1/5 for -1");
                break;
            case "equallyRandom" :
            default :
                logger.info("[GENERATOR] Using a probability of 1/3 of getting 0, 1/3 for 1, and 1/3 for -1");
                break;
        }

        ASAPTimeSA_Rule newRule;
        Integer tmp;
        // Create random rules
        for (Integer i = 0; i < NUMBER_OF_RULES; i++) {
            newRule = new ASAPTimeSA_Rule();

            // Random Administrator Time
            newRule.adminTime = randInt(1, NUMBER_OF_TIME_SLOTS);

            // Random Positive and Negative Preconditions
            for (Integer j = 1; j <= NUMBER_OF_ROLES; j++) {
                switch (preconditionGen) {
                    case "1/2Empty" :
                        /*
                         * tmp has a probability of 1/2 of getting 0, 1/4 for 1, and 1/4 for -1
                         */
                        tmp = randInt(0, 3) - 1;
                        if (tmp >= 2) {
                            tmp = 0;
                        }
                        break;
                    case "3/5Empty" :
                        /*
                         * tmp has a probability of 3/5 of getting 0, 1/5 for 1, and 1/5 for -1
                         */
                        tmp = randInt(0, 4) - 1;
                        if (tmp >= 2) {
                            tmp = 0;
                        }
                        break;
                    case "equallyRandom" :
                    default :
                        tmp = randInt(0, 2) - 1;
                        break;
                }

                switch (tmp) {
                    case 0 :
                        // Do Nothing
                        break;
                    case 1 :
                        // Positive Condition
                        newRule.precondition.add(j);
                        break;
                    case -1 :
                        // Positive Condition
                        newRule.precondition.add(-1 * j);
                        break;
                }
            }

            // Random Role Schedule
            newRule.roleTime = randInt(1, NUMBER_OF_TIME_SLOTS);

            // Random Role Assignment
            newRule.role = randInt(1, NUMBER_OF_ROLES);

            // Random Rule Type (only CanAssign and CanRevoke)
            if (randInt(0, 1) == 0) {
                newRule.ruleType = RuleType.ASSIGN.toRanise();
            } else {
                newRule.ruleType = RuleType.REVOKE.toRanise();
            }

            if (lastTestcase.addRule(newRule) == true) {
                logger.fine("[GENERATOR] Successfully added a new rule");
            }
        }

        // All Roles for the Query
        lastTestcase.query.goalRole = randInt(1, NUMBER_OF_ROLES);

        // query._timeslot = new TimeSlot(0);
        lastTestcase.query.goalTimeslot = randInt(1, NUMBER_OF_TIME_SLOTS);

        return lastTestcase;
    }

    @Override
    public String toString() {
        StringBuilder t = new StringBuilder();
        t.append("numberOfRoles = " + lastTestcase.numberOfRoles + "\n");
        t.append("numberOfTimeslots = " + lastTestcase.numberOfTimeslots + "\n");
        t.append("numberOfRules = " + lastTestcase.rules.size() + "\n");
        t.append(lastTestcase.query + "\n");

        return t.toString();
    }
}
