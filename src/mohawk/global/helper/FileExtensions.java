package mohawk.global.helper;

import java.io.File;
import java.io.FilenameFilter;

public class FileExtensions {
    public String Mohawk = ".mohawk";
    public String Mohawk_T = ".mohawkT";
    public String ASASPTime_SA = ".asasptime_sa";
    public String ASASPTime_NSA = ".asasptime_nsa";
    public String TRole = ".trole";
    public String TRule = ".trule";

    public String[] getFileExtensions() {
        return new String[]{Mohawk, Mohawk_T, ASASPTime_SA, ASASPTime_NSA, TRole, TRule};
    }

    public static FilenameFilter getFilter(final String ext) {
        return new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.endsWith(ext);
            }
        };
    }

    public static File[] getFiles(String directory, FilenameFilter filter) {
        File dir = new File(directory);
        if (!dir.isDirectory()) { return null; }

        File[] files = dir.listFiles(filter);

        return files;
    }
}
