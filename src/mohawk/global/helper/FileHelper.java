package mohawk.global.helper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;

public class FileHelper {
    public final static Logger logger = Logger.getLogger("mohawk");
    public String smvFilepath = "latestRBAC2SMV.smv";
    public Boolean smvDeleteFile = false;
    public String specFile = "";
    public String fileExt = ".mohawkT";
    public ArrayList<File> specFiles = new ArrayList<File>();
    public boolean bulk = false;

    public String lastError = null;
    public StringBuilder errorLog = new StringBuilder();

    public void loadSpecFiles() throws IOException {
        if (this.bulk == true) {
            this.loadSpecFilesFromFolder(this.specFile);
        } else {
            this.addSpecFile(this.specFile);
        }
    }

    public void loadSpecFilesFromFolder(String path) {
        if (path == null || path == "") {
            logError("[ERROR] No SPEC Folder provided");
        }

        File folder = new File(path);

        if (!folder.exists()) {
            logError("[ERROR] Spec Folder: '" + path + "' does not exists!");
        }
        if (!folder.isDirectory()) {
            logError("[ERROR] Spec Folder: '" + path + "' is not a folder and the 'bulk' option is present. "
                    + "Try removing the '-bulk' option if you wish to use "
                    + "a specific file, or change the option to point to a folder");
        }

        for (File f : folder.listFiles()) {
            if (f.getName().endsWith(fileExt)) {
                logger.fine("Adding file to specFiles: " + f.getAbsolutePath());
                specFiles.add(f);
            }
        }
    }

    private void logError(String msg) {
        lastError = msg;
        errorLog.append(msg);
        logger.severe(msg);
    }

    public void addSpecFile(String path) throws IOException {
        if (path == null || path.equals("")) {
            logError("[ERROR] No SPEC File provided");
        }

        File file2 = new File(path);
        if (!file2.exists()) {
            logError("[ERROR] Spec File: '" + path + "' does not exists!");
        }
        if (!file2.isFile()) {
            logError("[ERROR] Spec File: '" + path + "' is not a file and the 'bulk' option was not set. "
                    + "Try setting the '-bulk' option if you wish to search "
                    + "through the folder, or change the option to point to a specific file");
        }

        logger.fine("[FILE IO] Using SPEC File: " + file2.getCanonicalPath());
        specFiles.add(file2);
    }

    /** Prints the file list with an optional character limit
     * 
     * @param characterLimit
     *            if less than 0 then it will have no limit,
     *            else the resulting string will be less than or equal to the characterLimit
     * @param absolutePath
     *            if TRUE then it will print the absolute path of the file,
     *            else it will print the filename
     * @return */
    public String printFileNames(int characterLimit, boolean absolutePath) {
        StringBuilder s = new StringBuilder();
        String tmp;

        s.append("[");
        for (int i = 0; i < specFiles.size(); i++) {
            if (absolutePath) {
                tmp = specFiles.get(i).getAbsolutePath();
            } else {
                tmp = specFiles.get(i).getName();
            }
            if (s.length() + tmp.length() > characterLimit && characterLimit > 0) {
                break;
            }
            s.append(tmp).append(",");
        }
        s.append("]");
        return s.toString();
    }
}
