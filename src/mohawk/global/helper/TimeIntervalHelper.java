package mohawk.global.helper;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import mohawk.global.pieces.*;
import mohawk.global.testing.DebugMsg;

public class TimeIntervalHelper {
    public static final Logger logger = Logger.getLogger("mohawk");
    public static Boolean DEBUGGING = false;

    public SortedSet<TimeInterval> _timeIntervals = new TreeSet<TimeInterval>();
    public Integer maxTimeSlot = -1;
    public Integer minTimeSlot = Integer.MAX_VALUE;
    public static Integer MaxTimesToPrint = 25;

    /** if true then the first timeslot is 't0', if false than the first timeslot is 't1' */
    public Boolean allowZeroTimeslot = true;
    public HashMap<TimeInterval, Integer> _reducedTimeIntervals = new HashMap<TimeInterval, Integer>();
    public SortedSet<TimeInterval> _reducedTimeIntervalsSet = new TreeSet<TimeInterval>();

    public enum TimeType {
        IntervalStart, IntervalEnd, StartAndEnd
    }

    public TimeIntervalHelper() {}

    /** Deep Copy Constructor
     * 
     * @param tih */
    public TimeIntervalHelper(TimeIntervalHelper tih) {
        allowZeroTimeslot = new Boolean(tih.allowZeroTimeslot);
        maxTimeSlot = new Integer(tih.maxTimeSlot);
        minTimeSlot = new Integer(tih.minTimeSlot);

        for (TimeInterval ti : tih._timeIntervals) {
            _timeIntervals.add(new TimeInterval(ti));
        }

        for (TimeInterval ti : tih._reducedTimeIntervalsSet) {
            _reducedTimeIntervalsSet.add(new TimeInterval(ti));
        }

        for (Map.Entry<TimeInterval, Integer> entry : tih._reducedTimeIntervals.entrySet()) {
            _reducedTimeIntervals.put(new TimeInterval(entry.getKey()), new Integer(entry.getValue()));
        }
    }

    /** Returns the target timeslots that appear in the parameter originalRules,
     * and if deepCopy is TRUE then it performs a deep copy instead of a shallow copy
     * 
     * @param originalRules
     * @param deepCopy
     * @return */
    public static SortedSet<TimeSlot> getTargetTimeslots(ArrayList<Rule> originalRules, Boolean deepCopy) {
        SortedSet<TimeSlot> targetTimeslots = new TreeSet<TimeSlot>();

        for (Rule r : originalRules) {
            if (deepCopy) {
                for (TimeSlot ts : r._roleSchedule) {
                    targetTimeslots.add(new TimeSlot(ts));
                }
            } else {
                targetTimeslots.addAll(r._roleSchedule);
            }
        }

        if (logger.isLoggable(Level.FINE)) {
            logger.fine("[Reduction Step] Target Timeslots: " + targetTimeslots);
        }

        return targetTimeslots;
    }

    /** Returns the maximum timeslot that has been seen returns -1 to indicate that no timeslots have been seen
     * 
     * @return the greatest timeslot that is valid */
    public Integer getNumberOfTimeSlots() {
        if (!hasReduced()) {
            reduceToTimeslots();
        }
        return sizeReduced();
    }

    /** Returns the maximum timeslot that has been seen returns -1 to indicate that no timeslots have been seen
     * 
     * @return the greatest timeslot that is valid */
    public Integer getMaximumTimeSlot() {
        if (maxTimeSlot.equals(-1)) { return -1; }
        return maxTimeSlot - minTimeSlot + 1;// if maxTimeSlot == minTimeSlot => returns 1
    }

    /** Returns the next available timeslot that has been seen returns 0 to indicate that no timeslots have been seen
     * 
     * @return the greatest timeslot that is valid and hasn't been used */
    public Integer getNextTimeSlot() {
        return maxTimeSlot + 1;
    }

    /** Returns the number of unique intervals that appear in the MohawkT file
     * 
     * @return */
    public Integer size() {
        return _timeIntervals.size();
    }

    public void add(TimeInterval ti) {
        _timeIntervals.add(ti);

        maxTimeSlot = Math.max(maxTimeSlot, ti._finish);
        minTimeSlot = Math.min(minTimeSlot, ti._start);
    }

    /** Quick function for adding all of the time-intervals and timeslots from a rule into this helper
     * 
     * @param rule */
    public void add(Rule rule) {
        add(rule._adminTimeInterval);
        addAll(rule._roleSchedule);
    }

    /** Quick function for adding an array of timeslots into this helper
     * 
     * @param timeIntervals
     *            an arraylist of timeslots */
    public void addAll(ArrayList<TimeSlot> timeIntervals) {
        for (TimeInterval ti : timeIntervals) {
            add(ti);
        }
    }

    @Override
    public String toString() {
        // OLD CODE
        // return "|Size=" + _timeIntervals.size() + "| " + _timeIntervals.toString();
        StringBuilder sb = new StringBuilder();
        sb.append("|Size=" + _timeIntervals.size() + "| [");

        Integer i = 0;
        for (TimeInterval ti : _timeIntervals) {
            if (i.equals(MaxTimesToPrint)) {
                sb.append("...");
                break;
            }
            sb.append(ti.toString() + ", ");
            i = i + 1;
        }
        sb.append("]");

        return sb.toString();
    }

    /** Returns TRUE if the function {@link TimeIntervalHelper#reduceToTimeslots()} has been called already.
     * 
     * @return */
    public boolean hasReduced() {
        return _reducedTimeIntervals.size() > 0;
    }

    /** Returns the number of reduced unique intervals
     * 
     * @return */
    public Integer sizeReduced() {
        return _reducedTimeIntervals.size();
    }

    /** Sets up the internal data-structure for converting between a TimeInterval to a Timeslot only space */
    public void reduceToTimeslots() {
        HashMap<Integer, TimeType> timeline = new HashMap<Integer, TimeType>();

        TimeType tmp;
        for (TimeInterval ti : _timeIntervals) {
            if (ti.isTimeslot()) {
                if (DEBUGGING) {
                    DebugMsg.msg(DEBUGGING, "Skipping Timeslot: " + ti);/* Debugging */
                }
                continue;
            }
            tmp = timeline.get(ti.getStartTime());

            if (tmp == null) {
                tmp = TimeType.IntervalStart;
            } else if (tmp == TimeType.IntervalEnd) {
                tmp = TimeType.StartAndEnd;
            }
            timeline.put(ti.getStartTime(), tmp);

            tmp = timeline.get(ti.getFinishTime());

            if (tmp == null) {
                tmp = TimeType.IntervalEnd;
            } else if (tmp == TimeType.IntervalStart) {
                tmp = TimeType.StartAndEnd;
            }
            timeline.put(ti.getFinishTime(), tmp);
        }

        if (DEBUGGING) {
            DebugMsg.msg(DEBUGGING, "Timeline: " + timeline); /* Debugging */
        }

        ArrayList<Integer> times = new ArrayList<Integer>(timeline.keySet());
        Collections.sort(times);

        Integer previous = null;
        TimeType prev, current;
        Integer start, finish;
        TimeInterval ti_tmp;
        for (Integer i : times) {
            if (previous == null) {
                previous = i;
                continue;
            }

            prev = timeline.get(previous);
            current = timeline.get(i);

            // discontinuity
            if (prev == TimeType.IntervalEnd && current == TimeType.IntervalStart) {
                previous = i;
                continue;
            }

            if (prev == TimeType.IntervalStart) {
                start = previous;
            } else if (prev == TimeType.IntervalEnd) {
                start = previous + 1;
            } else {// TimeType.StartAndEnd
                ti_tmp = new TimeInterval(previous);
                _reducedTimeIntervalsSet.add(ti_tmp);
                start = previous + 1;
            }

            if (current == TimeType.IntervalStart) {
                finish = i - 1;
            } else if (current == TimeType.IntervalEnd) {
                finish = i;
            } else {// TimeType.StartAndEnd
                if (prev == TimeType.StartAndEnd) {
                    previous = i;
                    continue;
                }
                finish = i - 1;
            }

            ti_tmp = new TimeInterval(start, finish);
            _reducedTimeIntervalsSet.add(ti_tmp);

            previous = i;
        }

        SortedSet<TimeInterval> goBetween = new TreeSet<TimeInterval>();
        SortedSet<TimeInterval> goBetweenNext = new TreeSet<TimeInterval>();
        goBetweenNext.addAll(_reducedTimeIntervalsSet);
        TimeIntervalSplit tis;
        Boolean added = false;
        for (TimeInterval ti : _timeIntervals) {
            if (!ti.isTimeslot()) {
                DebugMsg.msg(DEBUGGING, "Skipping Non-Timeslot: " + ti);/* Debugging */
                continue;
            }

            added = false;
            goBetween.clear();
            goBetween.addAll(goBetweenNext);
            for (TimeInterval ti2 : goBetween) {
                if (ti2.isOverlapping(ti) && !ti2.equals(ti)) {
                    tis = new TimeIntervalSplit(ti, ti2);

                    if (DEBUGGING) {
                        DebugMsg.msg(DEBUGGING, "Timeslot isOverlapping TimeInterval: " + ti + " ; " + ti2 + " ; "
                                + tis);/* Debugging */
                    }

                    goBetweenNext.remove(ti2);
                    goBetweenNext.addAll(tis.getTimeIntervals());
                    added = true;
                }
            }

            if (!added) {
                goBetweenNext.add(ti);
            }
        }

        Integer index;
        if (allowZeroTimeslot) {
            index = 0;
        } else {
            index = 1;
        }
        for (TimeInterval ti : goBetweenNext) {
            _reducedTimeIntervals.put(ti, index);
            index++;
        }
    }

    /** Finds all of the time intervals that the parameter overlaps with and returns the set of indexes for those
     * reduced
     * Timeslots. If no timeslots have been seen, meaning that all rules are all time or there are no rules then it will
     * return 0 if allowZeroTimeslot is true, and 1 if not true.
     * 
     * @param timeInterval
     *            the time interval you wish to split into reduced timeslots
     * @return */
    public SortedSet<Integer> indexOfReduced(TimeInterval timeInterval) {
        SortedSet<Integer> overlapped = new TreeSet<Integer>();

        // If no timeslots and intervals were seen just return the lowest timeslot value (0 or 1)
        if (_reducedTimeIntervals.size() == 0) {
            if (allowZeroTimeslot) {
                overlapped.add(0);
            } else {
                overlapped.add(1);
            }
            return overlapped;
        }

        for (TimeInterval ti : _reducedTimeIntervals.keySet()) {
            if (ti.isOverlapping(timeInterval)) {
                overlapped.add(_reducedTimeIntervals.get(ti));
            }
        }

        return overlapped;
    }

    /** Returns a new TimeInterval from the minimum timeslot seen to the maximum timeslot seen.
     * 
     * @return */
    public TimeInterval getAllTimeInterval() {
        if (maxTimeSlot.equals(-1)) { throw new IllegalArgumentException(
                "Cannot return the All Time Interval when there has been not timeslots entered!"); }
        return new TimeInterval(minTimeSlot, maxTimeSlot);
    }
}
