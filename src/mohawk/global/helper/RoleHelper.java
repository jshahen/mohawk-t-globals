package mohawk.global.helper;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.util.*;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import mohawk.global.pieces.*;
import mohawk.global.pieces.reduced.rules.MohawkCA_Rule;
import mohawk.global.pieces.reduced.rules.MohawkCR_Rule;
import mohawk.global.timing.TimingEvent;

public class RoleHelper {
    public static final Logger logger = Logger.getLogger("mohawk");

    public static String RoleEnabled = "_ENABLE";
    public static String AssignedAndEnabled = "_AND_";
    public static String UniquePrefix = "__u";
    public static String UsesAdminTimeslot = "__UAT_";
    public static Integer MaxRolesToPrint = 25;

    private Set<Role> _roles = new HashSet<Role>();
    private Set<Role> _adminRoles = new HashSet<Role>();
    // public SortedSet<Role> _roles = new TreeSet<Role>(new
    // AlphanumericComparator(Locale.ENGLISH));
    // public ArrayList<Role> _rolesQuick = new ArrayList<Role>();
    /** Role Name --&gt; Integer unique to role (counting up) */
    public HashMap<String, Integer> _hashedRoles = null;
    public Boolean allowZeroRole = true;
    public boolean ruleComments = false;
    private Integer uniqueCount = 0;

    public RoleHelper() {}

    /** Deep Copy Constructor
     * 
     * @param rh */
    public RoleHelper(RoleHelper rh) {
        allowZeroRole = new Boolean(rh.allowZeroRole);
        uniqueCount = new Integer(rh.uniqueCount);

        for (Role r : rh._roles) {
            add(new Role(r));
        }

        if (rh._hashedRoles != null) {
            _hashedRoles = new HashMap<String, Integer>();
            for (Map.Entry<String, Integer> entry : rh._hashedRoles.entrySet()) {
                _hashedRoles.put(new String(entry.getKey()), new Integer(entry.getValue()));
            }
        }
    }

    /** Returns the administrator roles that appear in the parameter originalRules,
     * and if deepCopy is TRUE then it performs a deep copy instead of a shallow copy
     * 
     * @param originalRules
     * @param deepCopy
     * @return */
    public static SortedSet<Role> getAdministratorRoles(ArrayList<Rule> originalRules, Boolean deepCopy) {
        SortedSet<Role> adminRoles = new TreeSet<Role>();

        for (Rule r : originalRules) {
            if (!r._adminRole.isAllRoles()) {
                if (deepCopy) {
                    adminRoles.add(new Role(r._adminRole));
                } else {
                    adminRoles.add(r._adminRole);
                }
            }
        }

        if (logger.isLoggable(Level.FINE)) {
            logger.fine("[Reduction Step] Admistrative Roles: " + adminRoles);
        }

        return adminRoles;
    }

    public Set<Role> getAdminRoles() {
        return _adminRoles;
    }

    /** Returns the target roles that appear in the parameter originalRules,
     * and if deepCopy is TRUE then it performs a deep copy of the roles instead of a shallow copy
     * 
     * @param originalRules
     * @param deepCopy
     * @return */
    public static Set<Role> getTargetRoles(ArrayList<Rule> originalRules, Boolean deepCopy) {
        Set<Role> targetRoles = new HashSet<Role>();

        for (Rule r : originalRules) {
            if (deepCopy) {
                targetRoles.add(new Role(r._role));
            } else {
                targetRoles.add(r._role);
            }
        }

        if (logger.isLoggable(Level.FINE)) {
            logger.fine("[Reduction Step] Target Roles: " + targetRoles);
        }

        return targetRoles;
    }

    /** Returns the positive precondition roles that appear in the parameter originalRules, and if deepCopy is TRUE then
     * it performs a deep copy of the roles instead of a shallow copy
     * 
     * @param originalRules
     * @param deepCopy
     * @return */
    public static Set<Role> getPositivePreconditionRoles(ArrayList<Rule> originalRules, boolean deepCopy) {
        Set<Role> positivePreconditionRoles = new HashSet<Role>();

        for (Rule rule : originalRules) {
            for (Role r : rule._preconditions) {
                // Only copy Positive Preconditions and ignore the AllRoles role
                if (r.isPositivePrecondition() && !r.isAllRoles()) {
                    if (deepCopy) {
                        positivePreconditionRoles.add(new Role(r));
                    } else {
                        positivePreconditionRoles.add(r);
                    }
                }
            }
        }

        if (logger.isLoggable(Level.FINE)) {
            logger.fine("[Reduction Step] Positive Precondition Roles: " + positivePreconditionRoles);
        }

        return positivePreconditionRoles;
    }

    public Integer numberOfRoles() {
        return _roles.size();
    }

    public Integer numberOfAdminRoles() {
        return _adminRoles.size();
    }

    public boolean internalUpdateRequired() {
        if (_hashedRoles == null) { return true; }
        return _roles.size() != _hashedRoles.size();
    }

    /** @param role
     * @return returns the index from the sorted array of roles, negative if role has NOT flag set, null if not found */
    public Integer indexOf(Role role) {
        return indexOf(role, false);
    }

    public Integer indexOf(Role role, boolean positive) {
        if (role == null) { throw new NullPointerException("Role is NULL"); }

        String name = role.getName();
        Integer result = indexOf(name);

        if (!positive & role._not) { return -1 * result; }
        return result;
    }

    public Integer indexOf(String rolename) {
        if (rolename == null) { throw new NullPointerException("Role is NULL"); }
        if (rolename.isEmpty()) { throw new IllegalArgumentException("Role Name is Empty"); }

        if (internalUpdateRequired()) {
            setupSortedRoles();
        }

        Integer result = _hashedRoles.get(rolename);

        if (result == null) {
            logger.log(Level.SEVERE, "Unable to find role: " + rolename + " in current table: hashed="
                    + _hashedRoles.size() + "; roles=" + _roles.size());
            throw new NoSuchElementException("Unable to find role: " + rolename);
        }

        return result;
    }

    public Role get(int index) {
        if (internalUpdateRequired()) {
            setupSortedRoles();
        }

        for (Entry<String, Integer> r : _hashedRoles.entrySet()) {
            if (r.getValue().equals(index)) { return new Role(r.getKey()); }
        }

        return null;
    }

    public Role[] getArray() {
        return _roles.toArray(new Role[0]);
    }

    public Set<Role> getSet() {
        return _roles;
    }

    public Role[] getArrayShortNames() {
        ArrayList<Role> rr = new ArrayList<Role>(_hashedRoles.size());
        for (Integer i : _hashedRoles.values()) {
            rr.add(new Role("r" + i));
        }

        return rr.toArray(new Role[0]);
    }

    public Boolean dumpRoleSet(String filePath) {
        Boolean result = false;
        Path fileP = Paths.get(filePath);
        Charset charset = Charset.forName("utf-8");
        try (BufferedWriter writer = Files.newBufferedWriter(fileP, charset)) {
            writer.write("Role Set |" + _roles.size() + "|");
            writer.newLine();
            writer.newLine();

            for (Role r : _roles) {
                writer.write(r.getName());
                writer.newLine();
            }
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    public Boolean dumpHashedRoles(String filePath) {
        Boolean result = false;
        Path fileP = Paths.get(filePath);
        Charset charset = Charset.forName("utf-8");
        try (BufferedWriter writer = Files.newBufferedWriter(fileP, charset)) {
            writer.write("Role Set |" + _roles.size() + "|");
            writer.newLine();
            writer.newLine();

            for (String r : _hashedRoles.keySet()) {
                writer.write(r + "=" + _hashedRoles.get(r));
                writer.newLine();
            }
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    public boolean addAll(ArrayList<Role> roles) {
        boolean result = true;
        for (Role role : roles) {
            result = result & add(role);
        }
        return result;
    }

    public boolean add(Role role) {
        if (role == null) {
            logger.fine("[RoleHelper] Adding Role: null");
            return false;
        }

        if (role.isAllRoles()) {
            logger.fine("[RoleHelper] Adding Role: " + role.getString());
            return false;
        }
        boolean result = _roles.add(role.toLimitedRole()); // WORKS
        // boolean result = _roles.add(role); // DOES NOT WORK
        // logger.fine("[RoleHelper] Adding Role: " + role.getString() + "; Result: " + result);
        return result;
    }

    /**
     * Only adds the role into the Admin Role Set, you  must call {@link RoleHelper#add(Role)} separately to add it 
     * as a normal role!
     * @param role NULL and AllRoles are ignored
     * @return TRUE when the role was added and not already in the set
     */
    public boolean addAdmin(Role role) {
        if (role == null) {
            logger.fine("[RoleHelper] Adding Admin Role: null");
            return false;
        }

        if (role.isAllRoles()) {
            logger.fine("[RoleHelper] Adding Admin Role: " + role.getString());
            return false;
        }
        boolean result = _adminRoles.add(role.toLimitedRole());
        // logger.fine("[RoleHelper] Adding Admin Role: " + role.getString() + "; Result: " + result);
        return result;
    }

    public boolean add(Rule rule) {
        if (rule == null) {
            logger.fine("[RoleHelper] Adding Rule: null");
            return false;
        }

        boolean result = true;

        result = result & add(rule._role);
        result = result & add(rule._adminRole);
        result = result & addAdmin(rule._adminRole);
        result = result & addAll(rule._preconditions);

        return result;
    }

    /** 
     * Forces the assignment of roles to integer numbers for the use of indexable rules. <br>
     * Forces the order to be admin roles + normal roles.
     * 
     * If roles are after this, then this method must be called again (it overwrites the internal structures)
     */
    public void setupSortedRoles() {
        ArrayList<Role> _sortedRoles = new ArrayList<Role>(_adminRoles);
        Set<Role> normalRoles = new HashSet<Role>(_roles);
        normalRoles.removeAll(_adminRoles);
        _sortedRoles.addAll(normalRoles);

        _hashedRoles = new HashMap<String, Integer>();
        for (int i = 0; i < _sortedRoles.size(); i++) {
            if (allowZeroRole) {
                _hashedRoles.put(_sortedRoles.get(i).getName(), i);
            } else {
                _hashedRoles.put(_sortedRoles.get(i).getName(), i + 1);
            }
        }
    }

    /** 
     * A shortcut to get a unique role added without having to call setupSortedRoles, this will call setupSortedRoles
     * if _hashedRoles needs updating
     * 
     * The new role is always added at the end of the sorted _roles
     * 
     * @param suffix
     * @return the Unique Role 
     */
    public Role addUniqueRole(String suffix) {
        Role uniqueRole = getUniqueRole(suffix);

        if (internalUpdateRequired()) {
            setupSortedRoles();
        } else {
            if (allowZeroRole) {
                _hashedRoles.put(uniqueRole.getName(), _roles.size());
            } else {
                _hashedRoles.put(uniqueRole.getName(), _roles.size() + 1);
            }
        }

        add(uniqueRole);

        return uniqueRole;
    }

    /** Returns the last role name and appends the suffix to it
     * 
     * @param suffix
     * @return new Role(_roles.last()._name + suffix); */
    public Role getUniqueRole(String suffix) {
        if (suffix == null) { throw new NullPointerException("Suffix is NULL"); }
        if (suffix.isEmpty()) { throw new IllegalArgumentException("Suffix is Empty"); }

        Role uniqueRole = new Role(RoleHelper.UniquePrefix + uniqueCount + suffix);
        uniqueCount++;
        return uniqueRole;
    }

    /** Removes the CanEnable and CanDisable rules from the ArrayList. This function introduces new Roles that are
     * required in the reduction to remove these types of rules. It will also change the preconditions to include these
     * new roles. This does a Deep Copy on originalRules to protect the integrity of the original rules.
     * 
     * NOTE: ArrayList.addAll() does NOT do a deep copy and thus changes the Rules in originalRules!
     * 
     * @param originalRules */
    public ArrayList<Rule> removeEnableDisableRules(ArrayList<Rule> originalRules) {
        System.out.println("Using Version 1 of removeEnableDisableRules");
        logger.info("Using Version 1 of removeEnableDisableRules");

        ArrayList<Rule> rules = new ArrayList<Rule>();
        HashMap<Role, Role> enabledRoles = new HashMap<Role, Role>();

        /** PROBLEM: CanEnabled Rules are created first and the CanAssign rules are only for that admin slot, but since
         * we check for the role with the target timeslot instead of the the admin timeslot the result is not equivalent
         * 
         * ANSWER: Create CanAssign rules for every timeslot for each CanEnable rule */

        Role tmp = null;
        Rule rule = null;
        for (Rule orig_rule : originalRules) {
            /*
             * WARNING: DO NOT TOUCH orig_rule as it points to the original rule and thus changes the originalRules
             * ArrayList!
             */
            rule = new Rule(orig_rule);// Performs a Deep Copy of the Rule
            if (rule._type == RuleType.ENABLE || rule._type == RuleType.DISABLE) {
                if (!rule._adminRole.isAllRoles()) {
                    tmp = enabledRoles.get(rule._adminRole);
                    if (tmp == null) {
                        tmp = addUniqueRole("_" + rule._adminRole.getName() + RoleHelper.RoleEnabled
                                + RoleHelper.UsesAdminTimeslot);
                        enabledRoles.put(rule._adminRole, tmp);
                    }
                    rule._preconditions.add(tmp);
                    logger.fine("[removeEnableDisableRules] Adding role to precondition for Enable/Disable: " + tmp);
                }

                tmp = enabledRoles.get(rule._role);
                if (tmp == null) {
                    tmp = addUniqueRole(
                            "_" + rule._role.getName() + RoleHelper.RoleEnabled + RoleHelper.UsesAdminTimeslot);
                    enabledRoles.put(rule._role, tmp);
                }
                rule._role = tmp;
                rule._type = (rule._type == RuleType.ENABLE) ? RuleType.ASSIGN : RuleType.REVOKE;

                // NEW
                rule.roleSceduleToAllTime();

                logger.fine("[removeEnableDisableRules] Converting Rule " + orig_rule + " to : " + rule);
            } else if (rule._type == RuleType.ASSIGN || rule._type == RuleType.REVOKE) {
                if (!rule._adminRole.isAllRoles()) {
                    tmp = enabledRoles.get(rule._adminRole);
                    if (tmp == null) {
                        tmp = addUniqueRole("_" + rule._adminRole.getName() + RoleHelper.RoleEnabled
                                + RoleHelper.UsesAdminTimeslot);
                        enabledRoles.put(rule._adminRole, tmp);
                    }
                    rule._preconditions.add(tmp);
                    logger.fine("[removeEnableDisableRules] Adding role to precondition for Assign/Revoke: " + tmp);
                }
            } else {
                throw new IllegalArgumentException("Rule has a unknown rule type! " + rule);
            }

            rules.add(rule);// Copies the reference of the Rule
        }

        if (logger.isLoggable(Level.FINE)) {
            logger.fine("[END removeEnableDisableRules] EnabledRoles: " + enabledRoles);
            logger.fine("[END removeEnableDisableRules] New Rules: " + rules);
        }

        return rules;
    }

    /** From the Mohawk+T Manual: Section "Logical Reduction 2"
     * 
     * @param originalRules
     * @return */
    public ArrayList<Rule> removeEnableDisableRulesv2(ArrayList<Rule> originalRules) {
        System.out.println("Using Version 2 of removeEnableDisableRules");
        logger.info("Using Version 2 of removeEnableDisableRules");

        ArrayList<Rule> rules = new ArrayList<Rule>();
        HashSet<RoleTimeInterval> adminAssignRoles = new HashSet<RoleTimeInterval>();
        HashSet<RoleTimeInterval> adminRevokeRoles = new HashSet<RoleTimeInterval>();

        Rule rule = null;
        for (Rule orig_rule : originalRules) {
            /*
             * WARNING: DO NOT TOUCH orig_rule as it points to the original rule and thus changes the originalRules
             * ArrayList!
             */
            rule = new Rule(orig_rule);// Performs a Deep Copy of the Rule

            switch (rule._type) {
                case ASSIGN :
                case REVOKE :
                    // Do nothing special here
                    break;
                case ENABLE :
                    // Convert type to CanAssign
                    rule._type = RuleType.ASSIGN;
                case DISABLE :
                    if (rule._type == RuleType.DISABLE) {
                        // Convert type to CanRevoke
                        rule._type = RuleType.REVOKE;
                    }
                    // Convert target role to their 'enabled' form
                    add(rule._role.changeType(RoleType.ENABLED));

                    // Convert all precondition roles to their 'enabled' form
                    addAll(rule.rolesEnabled());

                    // Adds any additional precondition roles if there appears a
                    // NOT role
                    addAll(rule.rolesNotEnabledCombined());
                    break;
                default :
                    throw new IllegalArgumentException("Rule has a unknown rule type! " + rule);
            }

            if (!rule.isAdminRoleTrue()) {
                if (rule._type == RuleType.ASSIGN || rule._type == RuleType.ENABLE) {
                    adminAssignRoles.add(new RoleTimeInterval(new Role(rule._adminRole), rule._adminTimeInterval));
                } else if (rule._type == RuleType.REVOKE || rule._type == RuleType.DISABLE) {
                    adminRevokeRoles.add(new RoleTimeInterval(new Role(rule._adminRole), rule._adminTimeInterval));
                }

                // Convert admin role to Combined form (assigned and enabled)
                add(rule._adminRole.changeType(RoleType.COMBINED));
            }

            logger.fine("[removeEnableDisableRules] Converting Rule " + orig_rule + " to : " + rule);
            rules.add(rule);// Copies the reference of the Rule
        }

        /** For \canassignt and \canenablet rules, $\tuple{a, ti_a, C, ti_t, t}$, we need to add a new \canassignt rule:
         * $\tuple{true, L_{all}, a \wedge a_e, ti_a, a_{ti_a}}$. This rule properly reflects \canenablet for the role
         * $a$. */
        Role rTmp, rGoal;
        for (RoleTimeInterval rti : adminAssignRoles) {
            // Create new CanAssign rule that assigns the combined role for the
            // admin time-interval
            rTmp = new Role(rti._role);
            rTmp.changeType(RoleType.ENABLED);
            rGoal = new Role(rti._role);
            rGoal.changeType(RoleType.COMBINED);

            rules.add(new Rule(RuleType.ASSIGN, Role.allRoles(), new TimeSlot(1),
                    new ArrayList<Role>(Arrays.asList(rti._role, rTmp)), rti._timeInterval.getTimeSlots(), rGoal));
        }

        /** For \canrevoket and \candisablet rules, $\tuple{a, ti_a, C, ti_t, t}$, we need to add 2 new \canrevoket
         * rules: $\tuple{true, L_{all}, a, ti_a, a_{ti_a}}$, and $\tuple{true, L_{all}, a_e, ti_a, a_{ti_a}}$. These
         * two rules allow for removing the role $a_{ti_a}$ if either $a_e$ or $a$ is removed. This properly reflects
         * \canrevoket and \candisablet of the role $a$. */
        Rule ruleTmp;
        for (RoleTimeInterval rti : adminRevokeRoles) {
            // Create 2 new CanRevoke rule that removes the combined role for
            // the admin time-interval
            // Add it to a set of rules (this limits duplicate rules)
            rTmp = new Role(rti._role);
            rTmp._not = true;
            rGoal = new Role(rti._role);
            rGoal.changeType(RoleType.COMBINED);

            // new CanRevoke rule: NOT r1
            ruleTmp = new Rule(RuleType.REVOKE, Role.allRoles(), new TimeSlot(1),
                    new ArrayList<Role>(Arrays.asList(rTmp)), rti._timeInterval.getTimeSlots(), rGoal);

            if (ruleComments) {
                ruleTmp._comment = "CanEnablev2 - New CanRevoke rule - [pre] NOT ASSIGNED";
            }
            rules.add(ruleTmp);

            rTmp = new Role(rti._role);
            rTmp._not = true;
            rTmp.changeType(RoleType.ENABLED);
            // new CanRevoke rule: NOT r1e
            ruleTmp = new Rule(RuleType.REVOKE, Role.allRoles(), new TimeSlot(1),
                    new ArrayList<Role>(Arrays.asList(rTmp)), rti._timeInterval.getTimeSlots(), rGoal);

            if (ruleComments) {
                ruleTmp._comment = "CanEnablev2 - New CanRevoke rule - [pre] NOT ENABLED";
            }
            rules.add(ruleTmp);
        }

        if (logger.isLoggable(Level.FINE)) {
            logger.fine("[END removeEnableDisableRules] New Rules: " + rules);
        }

        return rules;
    }

    /** THIS IS THE LONGEST WAITED FUNCTION
     * 
     * @param originalRules
     * @param query
     * @param timeIntervalHelper
     * @return */
    public ArrayList<Rule> alwaysOnRules(ArrayList<Rule> originalRules, Query query,
            TimeIntervalHelper timeIntervalHelper) {
        ArrayList<Rule> rules = new ArrayList<Rule>();

        // Suffixes for the different types of roles which will be added
        String suffixCopy = "_AL";
        String suffixAdminExtra = RoleHelper.UsesAdminTimeslot + suffixCopy + "x";

        // Administrator Roles
        SortedSet<Role> adminRoles = getAdministratorRoles(originalRules, true);
        SortedSet<Role> originalRoles = new TreeSet<Role>();

        if (logger.isLoggable(Level.FINE)) {
            // does not convert adminRoles to a string unless it actually will
            // be printed
            logger.fine("Administrative Roles: " + adminRoles);
        }

        // Deep copy to protect the original roles
        for (Role r : _roles) {
            originalRoles.add(new Role(r));
        }
        _roles.clear();// get rid of the roles that do not exists anymore in the
                       // rules

        // the size of all the administrators * the size of all the time
        // intervals
        Integer AL = adminRoles.size() * timeIntervalHelper.size();
        logger.log(Level.INFO, "[REDUCTION STEP] Always On Administrators ({0}) * Timeslots ({1}) = ({2})",
                new Object[]{adminRoles.size(), timeIntervalHelper.size(), AL});

        // Create AL + 1 copies of all roles in the system
        for (Role r : originalRoles) {
            for (Integer i = 0; i < AL + 1; i++) {
                add(new Role(r, suffixCopy, i));
            }
        }

        // Create an extra AL copies of Admin Roles
        for (Role r : adminRoles) {
            // This one starts from 1 instead of 0
            for (int i = 1; i < AL + 1; i++) {
                add(new Role(r, suffixAdminExtra, i));
            }
        }

        // Copy Rules
        Rule tmp;
        // For j=0 only copy rules that have TRUE as their admin role
        for (Rule r : originalRules) {
            tmp = new Rule(r);
            if (tmp._type != RuleType.ASSIGN && tmp._type != RuleType.REVOKE) { throw new IllegalArgumentException(
                    "Rule is not of Can Assign or Can Revoke! (" + tmp + ")"); }

            if (tmp._adminRole.isAllRoles()) {
                logger.fine("[REDUCTION STEP] Looking for an Admin Role: " + tmp._role);
                if (adminRoles.contains(tmp._role)) {
                    logger.fine("[REDUCTION STEP] Found an Admin Role: " + tmp._role);
                    // PROBLEM: adding suffixAdminExtra but not adding it to the
                    // RoleHelper internal list of roles
                    // tmp.addSuffix(suffixAdminExtra, 0);
                    // ANSWER: Return all of the new roles from addSuffix and
                    // add them all to the internal SET
                    addAll(tmp.addSuffix(suffixAdminExtra, 0));
                } else {
                    tmp.addSuffix(suffixCopy, 0);
                }

                if (logger.isLoggable(Level.FINE)) {
                    logger.fine("[REDUCTION STEP] Adding Rules with TRUE Admin: " + tmp);
                }
                rules.add(tmp);
            }
        }

        // For j=1..AL copy all rules and append the special admin role
        // (suffixAdminExtra)
        for (Rule r : originalRules) {
            for (int j = 1; j < AL + 1; j++) {
                tmp = new Rule(r);
                if (tmp._type != RuleType.ASSIGN && tmp._type != RuleType.REVOKE) { throw new IllegalArgumentException(
                        "Rule is not of Can Assign or Can Revoke! (" + tmp + ")"); }

                if (tmp._adminRole.isAllRoles()) {
                    tmp.addSuffix(suffixCopy, j);
                    tmp._adminRole = Role.allRoles();
                } else {
                    tmp.addSuffix(suffixCopy, j);
                    tmp._preconditions.add(new Role(tmp._adminRole, suffixAdminExtra, j));
                    tmp._adminRole = Role.allRoles();
                }
                rules.add(tmp);
            }
        }

        // Creating the rules: $<a_{i,j-1}, a'_{i,j}> and <a'_{i,j},
        // a'_{i,j+1}>$
        TimeInterval allTime = timeIntervalHelper.getAllTimeInterval();
        for (Role adminRole : adminRoles) {
            for (int j = 1; j < AL + 1; j++) {
                // PROBLEM: these new rules have a time-interval for the admin
                // instead of a time slot
                // ANSWER: Fixed problem by only using the Start Time of the
                // Time-Interval;
                // this fix is reasonable because the administrator role is
                // TRUE,
                // thus anyone can activate this rule at the first timeslot
                tmp = new Rule(RuleType.ASSIGN, /* Can Assign */
                        Role.allRoles(), /* TRUE */
                        /* OLD LINE: */// allTime, // L_ALL
                        /* NEW LINE: */allTime.getStartTimeSlot(), /*
                                                                    * Do not need all times for a TRUE Admin
                                                                    */
                        new ArrayList<Role>(Arrays.asList(new Role(adminRole, suffixCopy, j - 1))), /* a_i,j-1 */
                        new ArrayList<TimeSlot>(), /* L_ALL */
                        new Role(adminRole, suffixAdminExtra, j) // a'_i,j
                );
                rules.add(tmp);

                if (j != AL) {
                    tmp = new Rule(RuleType.ASSIGN, /* Can Assign */
                            Role.allRoles(), /* TRUE */
                            /* OLD LINE: */// allTime, // L_ALL */
                            /* NEW LINE: */allTime.getStartTimeSlot(), /*
                                                                        * Do not need all times for a TRUE Admin
                                                                        */
                            new ArrayList<Role>(Arrays.asList(new Role(adminRole, suffixAdminExtra, j))), /* a'_i,j */
                            new ArrayList<TimeSlot>(), /* L_ALL */
                            new Role(adminRole, suffixAdminExtra, j + 1) /* a'_i,j+1 */
                    );
                    rules.add(tmp);
                }
            }
        }

        // update the query to point to the |AL| version of the roles
        logger.log(Level.FINE, "[REDUCTION STEP] OLD Query: " + query);
        ArrayList<Role> qRoles = new ArrayList<Role>(query._roles);
        query._roles = new ArrayList<Role>();
        Role tmpRole;
        for (Role r : qRoles) {
            tmpRole = new Role(r, suffixCopy, AL);
            query._roles.add(tmpRole);
        }

        if (logger.isLoggable(Level.FINE)) {
            // does not convert query or rules to a string unless it actually
            // will be printed
            logger.log(Level.FINE, "[REDUCTION STEP] NEW Query: " + query);
            logger.log(Level.FINE, "[END alwaysOnRules] New Rules: " + rules);
        }

        return rules;
    }

    /** Goes through all the rules and does a deep copy (if deepCopy == TRUE) of the rules where the precondition is
     * "invokable" (which means that there does not exists a role and it's negation in the precondition)
     * 
     * @param originalRules
     * @param deepCopy
     * @return */
    public ArrayList<Rule> removeUninvokablePrecondtions(ArrayList<Rule> originalRules, Boolean deepCopy) {
        ArrayList<Rule> rules = new ArrayList<Rule>();

        for (Rule origRule : originalRules) {
            if (origRule.isInvokablePrecondition()) {
                if (deepCopy) {
                    rules.add(new Rule(origRule));
                } else {
                    rules.add(origRule);
                }
            } else {
                logger.fine("Skipping rule because it has an invokable precondition: " + origRule);
            }
        }

        return rules;
    }

    /** Removes all preconditions from the Can Revoke Rules by creating new Can Assign rules
     * 
     * @param originalRules
     * @param deepCopy
     * @return Returns all previous Can Assign and Can Revoke rules, except the Can Revoke rules have empty
     *         preconditions and there are new Can Assign rules and the old Can Assign rules have been modified if they
     *         have a negated role that also has a Can Revoke rule */
    public ArrayList<Rule> removePrecondtionsCanRevoke(ArrayList<Rule> originalRules, Boolean deepCopy) {
        ArrayList<Rule> rules = new ArrayList<Rule>();
        ArrayList<Rule> newCanAssign = new ArrayList<Rule>();

        HashMap<Role, Role> role2role = new HashMap<Role, Role>();

        TimingEvent removeCR = new TimingEvent();
        removeCR.setStartTimeNow();

        Rule tmp = null;
        Role tr = null;
        for (Rule origRule : originalRules) {
            if (origRule._type == RuleType.REVOKE && origRule._preconditions.size() != 0) {
                // Create new unique role for the new Can Assign rule
                // ASUMPTION: that the '_CR1' will produce a new unique role
                tr = new Role(origRule._role, "_CR", 1);
                add(tr);
                role2role.put(origRule._role, tr);

                // Create new Can Assign Rule
                tmp = new Rule(origRule);
                tmp._type = RuleType.ASSIGN;
                tmp._role = tr;
                tmp._preconditions.add(tmp._role.getNot());
                newCanAssign.add(tmp);

                // Create/change Can Revoke rule to remove precondition
                if (deepCopy) {
                    tmp = new Rule(origRule);
                } else {
                    tmp = origRule;
                }
                tmp._preconditions.clear();
            } else {
                if (deepCopy) {
                    tmp = new Rule(origRule);
                } else {
                    tmp = origRule;
                }
            }

            rules.add(tmp);
        }

        removeCR.setFinishTimeNow();
        System.out.println("Finished Removing CR " + removeCR);

        TimingEvent changePrecondition = new TimingEvent();
        changePrecondition.setStartTimeNow();
        // Find all rules that contain a negated role in the precondition and
        // add the new role to the precondition

        for (Rule r : rules) {
            r.addConditionalPrecondition(role2role);
        }

        changePrecondition.setFinishTimeNow();
        System.out.println("Finished addConditionalPrecondition " + changePrecondition);

        // Add this after the addition of the new precondition
        rules.addAll(newCanAssign);

        return rules;
    }

    /** Removes temporality from the Mohawk-T Rules format by replacing the roles held in this instance of the
     * RoleHelper
     * with roles that symbolize the role and the timeslot. Because there exists a time interval for the administrator
     * and a role-schedule for precondition we make multiple rules for each of the values in the Cartesian product of
     * these sets
     * 
     * ASSERTIONS: * There must only exists rules of the form Can Assign and Can Revoke * Rules must have TRUE as their
     * administrator
     * 
     * @param workableRules
     * @param timeIntervalHelper
     * @return
     * @throws Exception */
    public ArrayList<Object> removeTemporality(ArrayList<Rule> workableRules, TimeIntervalHelper timeIntervalHelper)
            throws Exception {
        ArrayList<Object> rules = new ArrayList<Object>();

        TimeInterval[] allTimeIntervals = timeIntervalHelper._timeIntervals.toArray(new TimeInterval[0]);
        TimeInterval[] tis;

        // Get rid of the old roles
        _roles.clear();

        for (Rule r : workableRules) {
            if (r._type != RuleType.ASSIGN && r._type != RuleType.REVOKE) { throw new Exception(
                    "Rule Type must be Can Assign or Can Revoke"); }

            if (r._roleSchedule.size() == 0) {
                tis = allTimeIntervals;
            } else {
                tis = r._roleSchedule.toArray(new TimeInterval[0]);
            }

            // Create all the rules in their reduced format
            for (TimeInterval ti : tis) {
                // Add new role, even if it is a can enable role
                add(new Role(r._role, ti));
                for (Role role : r._preconditions) {
                    // WARNING: Precondition Enable Roles are special
                    if (role.usesAdminTimeslot()) {
                        if (logger.isLoggable(Level.FINE)) {
                            logger.fine("[REDUCTION STEP] Adding Enabled Precondition Role with AdminTimeInterval: "
                                    + role + r._adminTimeInterval.standardRoleName() + " ; From Rule: " + r);
                        }
                        add(new Role(role, r._adminTimeInterval));
                    } else {
                        add(new Role(role, ti));
                    }
                }

                if (logger.isLoggable(Level.FINE)) {
                    logger.fine("Converting to CA/CR Rule (ti = " + ti + "): " + r);
                }

                if (r._type == RuleType.ASSIGN) {
                    rules.add(new MohawkCA_Rule(r, ti));
                } else {
                    rules.add(new MohawkCR_Rule(r, ti));
                }
            }
        }

        return rules;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("|Size=" + _roles.size() + "| [");

        Integer i = 0;
        for (Role r : _roles) {
            if (i.equals(MaxRolesToPrint)) {
                sb.append("...");
                break;
            }
            sb.append(r.toString() + ", ");
            i = i + 1;
        }
        sb.append("]");

        return sb.toString();
    }
}
