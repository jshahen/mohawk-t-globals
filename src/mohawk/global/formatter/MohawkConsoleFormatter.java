package mohawk.global.formatter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.*;

import org.apache.commons.lang3.text.WordUtils;

public class MohawkConsoleFormatter extends Formatter {
    public final static Logger logger = Logger.getLogger("mohawk");
    public static final DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
    public Integer padLeft = Level.WARNING.toString().length() + 1;
    public Integer maxWidth = 180;
    public String newLineStr = "\n    ";

    public String format(LogRecord record) {
        StringBuilder builder = new StringBuilder(1000);
        StringBuilder builder2 = new StringBuilder(100);

        // Date Time
        builder.append(getDateTime(record.getMillis())).append(" - ");
        // Class Name
        builder.append("[").append(record.getSourceClassName()).append(".");
        // Method Name
        builder.append(record.getSourceMethodName()).append(" #");
        // Thread ID
        builder.append(record.getThreadID()).append("]:\n");
        // Logging Level
        String signifcantLevel = "";
        char padChar = ' ';
        if (record.getLevel() == Level.WARNING || record.getLevel() == Level.SEVERE) {
            signifcantLevel = "";
            padChar = '*';
        }
        builder2.append("[");
        builder2.append(
                String.format("%1$" + signifcantLevel + padLeft + "s", record.getLevel()).replace(' ', padChar));
        builder2.append("] ");
        // Message
        builder2.append(formatMessage(record));
        builder2.append("\n"); // End of Message
        if (maxWidth == 0) {
            builder.append(builder2);
        } else {
            builder.append(WordUtils.wrap(builder2.toString(), maxWidth, newLineStr, true));
        }

        return builder.toString();
    }

    public static String getDateTime(long millis) {
        return df.format(new Date(millis));
    }

    /** Get the current line number.
     * 
     * @return int - Current line number. */
    public static int getLineNumber() {
        return Thread.currentThread().getStackTrace()[2].getLineNumber();
    }

    public String getHead(Handler h) {
        return super.getHead(h);
    }

    public String getTail(Handler h) {
        return super.getTail(h);
    }
}
