package mohawk.global.comparator;

import java.util.Comparator;

import mohawk.global.pieces.Rule;

public class RuleComparator implements Comparator<Rule> {
    @Override
    public int compare(Rule arg0, Rule arg1) {
        return arg0.getInfoCode().compareTo(arg1.getInfoCode());
    }
}
