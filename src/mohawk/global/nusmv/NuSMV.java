/**
 * The MIT License
 * 
 * Copyright (c) 2010 Karthick Jayaraman
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */

package mohawk.global.nusmv;

import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.logging.Logger;

import mohawk.global.results.CounterExample;
import mohawk.global.results.CounterExampleStep;

/** @author Jonathan Shahen; kjayaram */
public class NuSMV {
    public final static Logger logger = Logger.getLogger("mohawk");

    public Process execProcess = null;
    /** Exit value returned from the last execution of{@link NuSMV#RunNuSMV(List, String)}; NULL if not run yet. */
    public Integer exitValue = null;
    public String NuSMVProcessName = "NuSMV";
    /** Holds the path to the file where the program's console output is stored */
    public String programOutput = "logs/latestNuSMVOutput.txt";
    /** Internal reference to the current bound */
    public Integer bound = null;

    /** When TRUE it will output the NuSMV output as it comes in */
    public boolean writeNuSMVOutput = false;

    public CounterExample counterExample = new CounterExample();

    /** Empty constructor, uses the default values */
    public NuSMV() {}

    public NuSMV(String nusmvProcessName) {
        NuSMVProcessName = nusmvProcessName;
    }

    /** Function to call the symbolic model checker.
     * 
     * @param filename path to the SMV file
     * @return TRUE is the QUERY is reachable, FALSE if it is unreachable
     * @throws IOException
     * @throws InterruptedException */
    public boolean SMCFile(String filename) throws IOException, InterruptedException {
        // Commandline to invoke the NuSMV symbolic model checker.
        List<String> commands = Arrays.asList(NuSMVProcessName, filename);
        return RunNuSMV(commands);
    }

    /** Call the NuSMV symbolic model checker. The bound and SMV model filename are passed as inputs
     * 
     * @param bound
     * @param filename
     * @return TRUE is the QUERY is reachable, FALSE if it is unreachable
     * @throws IOException
     * @throws InterruptedException */
    public boolean BMCFile(Integer bound, String filename) throws IOException, InterruptedException {
        this.bound = bound;
        List<String> commands = Arrays.asList(NuSMVProcessName, "-bmc", "-bmc_length", bound.toString(), filename);
        return RunNuSMV(commands);
    }

    /** Run the NuSMV engine using the specified command and input.<br>
     * Returns TRUE is the QUERY is reachable, FALSE if it is unreachable
     * 
     * @param commands List&lt;String&gt; = [program,command param 1, command param 2, ...]
     * @return TRUE is the QUERY is reachable, FALSE if it is unreachable
     * @throws IOException
     * @throws InterruptedException
     */
    private boolean RunNuSMV(List<String> commands) throws IOException, InterruptedException {
        logger.entering("NuSMV", "RunNuSMV(commands)");
        logger.info("[NuSMV] Running NuSMV with the commands: " + commands);

        // Run the NuSMV engine
        ProcessBuilder builder = new ProcessBuilder(commands);
        builder.redirectErrorStream(true); // combine the Error into the normal stream
        execProcess = builder.start();

        BufferedReader bufread = new BufferedReader(new InputStreamReader(execProcess.getInputStream()));
        String strLine = null;
        String adminUser = "", targetUser = "", rule = "";
        StringBuilder _lastOutput = new StringBuilder();
        StringBuilder _ruleProgression = new StringBuilder();
        int index;

        if (writeNuSMVOutput) {
            System.out.println("################### START: NuSMV Output ###################");
        }
        _lastOutput.append("################### START: NuSMV Output ###################\n");
        while ((strLine = bufread.readLine()) != null) {

            index = strLine.indexOf("user = ");
            if (index != -1) {
                targetUser = strLine.substring(index + 7, strLine.length()).trim();
            }
            index = strLine.indexOf("admin = ");
            if (index != -1) {
                adminUser = strLine.substring(index + 8, strLine.length()).trim();
            }
            index = strLine.indexOf("rule = ");
            if (index != -1) {
                rule = strLine.substring(index + 7, strLine.length()).trim();
                _ruleProgression.append(rule).append(" -> ");

                counterExample.addStep(new CounterExampleStep(rule, targetUser, adminUser));
            }
            if (bound != null) {
                index = strLine.indexOf("-- no counterexample found with bound ");
                if (index != -1) {
                    strLine += "/" + bound;
                }
            }

            _lastOutput.append(strLine + "\n");
            if (writeNuSMVOutput) {
                System.out.println(strLine);
            }
        }
        bound = null; // clear the bound if an SMC call is called next
        _lastOutput.append("################### END: NuSMV Output ###################\n");
        if (writeNuSMVOutput) {
            System.out.println("################### END: NuSMV Output ###################");
        }

        execProcess.waitFor();
        exitValue = execProcess.exitValue();
        _lastOutput.append("################### NuSMV Exit Value: " + execProcess.exitValue() + "\n");
        if (writeNuSMVOutput) {
            System.out.println("################### NuSMV Exit Value: " + execProcess.exitValue());
        }

        if (_ruleProgression.length() > 0) {
            _ruleProgression.append("GOAL_STATE");
            counterExample.goalReached = true;
        } else {
            counterExample.goalReached = false;
            _ruleProgression.append("GOAL_STATE_UNREACHABLE");
        }
        logger.info("[NuSMV] Rule Progression: " + _ruleProgression.toString());
        logger.info("[NuSMV] Counter Example: " + counterExample);

        try {
            FileWriter fw = new FileWriter(programOutput, false);
            fw.write("Generated on: " + Calendar.getInstance().getTime() + "\n");
            fw.write("Rule Progression:\n");
            fw.write(_ruleProgression.toString());
            fw.write("\n\nCounter Example:\n");
            fw.write(counterExample.toString());
            fw.write("\n\n");
            fw.write(_lastOutput.toString());
            fw.close();
        } catch (Exception e) {
            // do not send this error up
            System.out.println("[ERROR] Failed to write out the NuSMV log");
        }

        logger.exiting("NuSMV", "RunNuSMV(commands)");

        // Use this for the Mohawk's conversion to NuSMV
        if (_lastOutput.toString().indexOf("= FALSE  is false") != -1) { return true; }
        // Use this for Mohawk+T's conversion to NuSMV
        if (_lastOutput.toString().indexOf("false") != -1) { return true; }

        return false;
    }

    /** Function to call the symbolic model checker. Takes the NuSMV code as input
     * 
     * @param smvmodel
     * @return TRUE is the QUERY is reachable, FALSE if it is unreachable
     * @throws IOException
     * @throws InterruptedException */
    public boolean SMC(String smvmodel) throws IOException, InterruptedException {
        List<String> commands = Arrays.asList(NuSMVProcessName); // Commandline to invoke the NuSMV symbolic model
                                                                 // checker.
        return RunNuSMV(commands, smvmodel);
    }

    /** Call the NuSMV symbolic model checker. The bound and SMV model are passed as inputs
     * 
     * @param bound
     * @param smvmodel
     * @return TRUE is the QUERY is reachable, FALSE if it is unreachable
     * @throws IOException
     * @throws InterruptedException */
    public boolean BMC(Integer bound, String smvmodel) throws IOException, InterruptedException {
        List<String> commands = Arrays.asList(NuSMVProcessName, "-bmc", "-bmc_length", bound.toString());
        return RunNuSMV(commands, smvmodel);
    }

    /** Run the NuSMV engine using the specified command and input.<br>
     * Returns TRUE is the QUERY is reachable, FALSE if it is unreachable
     * 
     * @param commands
     * @param smvmodel
     * @return TRUE is the QUERY is reachable, FALSE if it is unreachable
     * @throws IOException
     * @throws InterruptedException */
    private boolean RunNuSMV(List<String> commands, String smvmodel) throws IOException, InterruptedException {
        logger.entering("NuSMV", "RunNuSMV(commands, smvmodel)");
        logger.info("[NuSMV] Running NuSMV with the commands: " + commands);

        // Run the NuSMV engine
        ProcessBuilder builder = new ProcessBuilder(commands);
        builder.redirectErrorStream(true); // combine the Error into the normal stream
        execProcess = builder.start();

        execProcess.getOutputStream().write(smvmodel.getBytes());
        execProcess.getOutputStream().flush();
        execProcess.getOutputStream().close();

        BufferedReader bufread = new BufferedReader(new InputStreamReader(execProcess.getInputStream()));

        String strLine = null;
        StringBuilder _lastOutput = new StringBuilder();
        System.out.print("Reading Lines");
        while ((strLine = bufread.readLine()) != null) {
            _lastOutput.append(strLine + "\n");
            System.out.print('.');
        }
        System.out.println("Done Reading\n");

        System.out.println("################### NuSMV Output ###################");
        System.out.println(_lastOutput.toString());
        System.out.println("################### NuSMV Output ###################");
        execProcess.waitFor();
        _lastOutput.append("\n################### NuSMV Return Value ################### \n")
                .append(execProcess.exitValue());
        System.out.println("NuSMV Exit Value: " + execProcess.exitValue());

        try {
            Path nusmvLog = Paths.get(programOutput);
            Files.write(nusmvLog, _lastOutput.toString().getBytes());
        } catch (Exception e) {
            // do not send this error up
            System.out.println("[ERROR] Failed to write out the NuSMV log");
        }

        logger.exiting("NuSMV", "RunNuSMV(commands, smvmodel)");

        if (_lastOutput.toString().indexOf("= FALSE  is false") != -1) { return true; }
        if (_lastOutput.toString().indexOf("false") != -1) { return true; }

        return false;
    }

    public boolean BMC_old(int bound, String smvmodel) throws IOException, InterruptedException {
        String commands = String.format(NuSMVProcessName + " -bmc -bmc_length %d", bound);
        return RunNuSMV_old(commands, smvmodel);
    }

    public boolean SMC_old(String smvmodel) throws IOException, InterruptedException {
        String commands = NuSMVProcessName; // Commandline to invoke the NuSMV symbolic model checker.
        return RunNuSMV_old(commands, smvmodel);
    }

    // Run the NuSMV engine using the specified command and input
    private boolean RunNuSMV_old(String commands, String smvmodel) throws IOException, InterruptedException {
        Runtime rt = Runtime.getRuntime();
        String strOutput = "";

        // Run the NuSMV engine
        Process execProcess = rt.exec(commands);
        execProcess.getOutputStream().write(smvmodel.getBytes());
        execProcess.getOutputStream().flush();
        execProcess.getOutputStream().close();

        BufferedReader bufread = new BufferedReader(new InputStreamReader(execProcess.getInputStream()));
        BufferedReader bufread_err = new BufferedReader(new InputStreamReader(execProcess.getErrorStream()));

        String strLine = null;
        while ((strLine = bufread.readLine()) != null) {
            strOutput = strOutput + strLine + "\n";
        }

        System.out.println(strOutput);

        String strLine_err = "";
        while ((strLine = bufread_err.readLine()) != null) {
            strLine_err = strLine_err + strLine + "\n";
        }

        System.out.println("------------------ERROR Buffer------------------");
        System.out.println(strLine_err);
        System.out.println("------------------ERROR Buffer------------------");
        execProcess.waitFor();
        System.out.println("NuSMV Return: " + execProcess.exitValue());

        if (strOutput.indexOf("false") != -1) return true;

        return false;
    }
}
