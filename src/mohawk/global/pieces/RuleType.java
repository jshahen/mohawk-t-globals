package mohawk.global.pieces;

public enum RuleType {
    ASSIGN("CanAssign", "A"), DISABLE("CanDisable", "D"), ENABLE("CanEnable", "E"), REVOKE("CanRevoke", "R");

    private final String _name;
    public final String _shortform;

    RuleType(String name, String shortform) {
        this._name = name;
        this._shortform = shortform;
    }

    @Override
    public String toString() {
        return this._name;
    }

    public String toRanise() {
        String tmp = _name.toLowerCase();
        tmp = tmp.replaceFirst("can", "can_");

        return tmp;
    }

    public String toUzun() {
        return _shortform;
    }
}
