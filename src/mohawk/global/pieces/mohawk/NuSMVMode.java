/**
 * 
 */
package mohawk.global.pieces.mohawk;

/** @author Jonathan Shahen; kjayaram */
public enum NuSMVMode {

    SMC,   // Symbolic model checking
    BMC,   // Bounded modelchecking
    BOTH
}
