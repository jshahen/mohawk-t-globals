/**
 * The MIT License
 * 
 * Copyright (c) 2010 Karthick Jayaraman
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */

package mohawk.global.pieces.mohawk;

import java.util.*;

/** This class represents a CAEntry. Each CAEntry has three parts: - Roles that the administrator should have -
 * Preconditions, which are essentially set of roles the user should have Preconditions on roles are specified as a
 * BitVector - Role - The role that can be assigned
 * 
 * @author Karthick Jayaraman */
public class CAEntry {
    private String strAdminRole;
    private PreCondition pcPreconditions;
    private String strRole;

    public CAEntry(String inStrAdminRole, PreCondition inPcPreCondition, String inStrRole) {
        strAdminRole = inStrAdminRole;
        pcPreconditions = inPcPreCondition;
        strRole = inStrRole;
    }

    public String getAdminRole() {
        return strAdminRole;
    }

    public PreCondition getPreConditions() {
        return pcPreconditions;
    }

    public String getRole() {
        return strRole;
    }

    public Boolean isStartable() {
        Boolean startable = true;

        // TRUE precondition means it is startable (truly startable)
        if (pcPreconditions.size() == 0) { return true; }

        for (Integer value : pcPreconditions.valueSet()) {
            // 1 - refers to positive condition
            // 2 - refers to negative condition.
            startable = startable & value == 2;
        }

        return startable;
    }

    @Override
    public String toString() {
        return "<" + strAdminRole + "," + pcPreconditions + "," + strRole + ">";
    }

    /** Will update the CAEntry by removing all negative roles,
     * if it reaches a positive role condition then it will return false (the rule will need to be removed)
     * 
     * @param removeRolesIndex
     * @return TRUE: updated and ready to be used; FALSE: cannot update and must be deleted */
    public boolean updateRule(HashSet<Integer> removeRolesIndex, Vector<String> vRoles, Map<Integer, String> oldMap) {
        // TRUE precondition means it is startable (truly startable)
        if (pcPreconditions.size() == 0) { return true; }

        PreCondition newPreCond = new PreCondition();
        Vector<Integer> roles = new Vector<Integer>(pcPreconditions.keySet());
        for (int i = roles.size() - 1; i >= 0; i--) {
            Integer role = roles.get(i);
            Integer value = pcPreconditions.getConditional(role);

            if (removeRolesIndex.contains(role)) {
                // 1 - refers to positive condition
                // 2 - refers to negative condition.
                if (value == 1) {
                    return false;
                } else {
                    // DO NOT ADD to the new PreCondition
                }
            } else {
                newPreCond.addConditional(vRoles.indexOf(oldMap.get(role)), value);
            }
        }
        pcPreconditions = newPreCond;

        return true;
    }
}
