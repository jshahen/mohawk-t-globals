package mohawk.global.pieces;

import java.util.ArrayList;

public class TimeInterval implements Comparable<TimeInterval> {
    protected static final Boolean sortByStartTime = true;
    public Integer _start = null;
    public Integer _finish = null;

    public TimeInterval(Integer startFinish) {
        this(startFinish, startFinish);
    }

    public TimeInterval(Integer start, Integer finish) {
        this._start = start;
        this._finish = finish;
    }

    /** Deep Copy Constructor
     * 
     * @param timeInterval */
    public TimeInterval(TimeInterval timeInterval) {
        _start = new Integer(timeInterval.getStartTime());
        _finish = new Integer(timeInterval.getFinishTime());
    }

    public Integer getFinishTime() {
        return _finish;
    }

    public Integer getStartTime() {
        return _start;
    }

    public void setFinishTime(Integer finish) {
        _finish = finish;
    }

    public void setStartTime(Integer start) {
        _start = start;
    }

    public Boolean isValidInterval() {
        return _start != null && _finish != null && _start <= _finish;
    }

    public Boolean isTimeslot() {
        return _start.equals(_finish);
    }

    /** Returns if this object and obj are overlapping, where t1-t3 and t3-t5 are overlapping on t3
     * 
     * @param obj
     * @return */
    public Boolean isOverlapping(TimeInterval obj) {
        return ((_start <= obj.getStartTime() && obj.getStartTime() <= _finish)
                || (obj.getStartTime() <= _start && _start <= obj.getFinishTime()));
    }

    /** There are three types of overlap: Full/Equality Overlap, Shared Overlap, and Normal OVerlap
     * 
     * Full/Equality Overlap: Returns 0 if the two TimeIntervals are equal
     * 
     * Shared Overlap: Returns 1 if strictly the starting points are equal or if the finishing points are equal
     * 
     * Normal Overlap: Returns 2 if there exists overlap and no point is equal to the other
     * 
     * @param obj
     * @return -1 if no overlap exists, 0 for Full/Equality Overlap, 1 for Shared Overlap, 2 for Normal Overlap */
    public Integer typeOfOverlap(TimeInterval obj) {
        if (!isOverlapping(obj)) { return -1; }

        Boolean start = getStartTime().equals(obj.getStartTime());
        Boolean finish = getFinishTime().equals(obj.getFinishTime());

        if (start && finish) {
            // Equality
            return 0;
        } else if (start || finish) {
            // Shared
            return 1;
        } else {
            // Normal
            return 2;
        }
    }

    /** Returns if this object takes place before the param second
     * 
     * @param second
     * @return */
    public boolean isBefore(TimeInterval second) {
        return getStartTime() < second.getStartTime();
    }

    /** Returns the Mohawk-T representation of this class with the Timeslot shortform (instead of "[t1-t1]" this will
     * return "t1")
     * 
     * @return */
    public String getString() {
        if (isTimeslot()) { return "t" + _start.toString(); }
        return getFullString();
    }

    /** Returns the Mohawk-T full representation "t1-t2"
     * 
     * @return */
    public String getFullString() {
        return "t" + _start + "-t" + _finish;
    }

    /** Provides a unified role name addition for TimeIntervals
     * 
     * @return */
    public String standardRoleName() {
        return "_" + toString();
    }

    public TimeSlot getStartTimeSlot() {
        return new TimeSlot(_start);
    }

    public ArrayList<TimeSlot> getTimeSlots() {
        ArrayList<TimeSlot> ts = new ArrayList<TimeSlot>();

        for (int i = _start; i <= _finish; i++) {
            ts.add(new TimeSlot(i));
        }

        return ts;
    }

    @Override
    public String toString() {
        return getString();
    }

    @Override
    public boolean equals(Object obj) {
        if ((obj instanceof TimeInterval)) {
            TimeInterval _obj = (TimeInterval) obj;

            return (this._start.equals(_obj.getStartTime()) && this._finish.equals(_obj.getFinishTime()));
        }

        if ((obj instanceof TimeSlot)) {
            TimeSlot _obj = (TimeSlot) obj;

            return (this._start.equals(_obj.getStartTime()) && this._finish.equals(_obj.getFinishTime()));
        }

        return false;
    }

    @Override
    public int compareTo(TimeInterval o) {
        if (equals(o)) { return 0; }

        Integer i;
        if (sortByStartTime) {
            i = _start.compareTo(o._start);

            if (i.equals(0)) { return _finish.compareTo(o._finish); }
            return i;
        } else {
            i = _finish.compareTo(o._finish);

            if (i.equals(0)) { return _start.compareTo(o._start); }
            return i;
        }
    }

    @Override
    public int hashCode() {
        return getFullString().hashCode();
    }
}
