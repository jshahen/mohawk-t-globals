package mohawk.global.pieces;

import java.util.ArrayList;

import org.apache.commons.lang3.StringEscapeUtils;

import mohawk.global.formatter.MohawkConsoleFormatter;
import mohawk.global.helper.RoleHelper;
import mohawk.global.helper.TimeIntervalHelper;

public class MohawkT {
    public RoleHelper roleHelper = new RoleHelper();
    public TimeIntervalHelper timeIntervalHelper = new TimeIntervalHelper();

    public CanAssign canAssign = new CanAssign();
    public CanRevoke canRevoke = new CanRevoke();
    public CanEnable canEnable = new CanEnable();
    public CanDisable canDisable = new CanDisable();

    public Query query = new Query();
    public ExpectedResult expectedResult = ExpectedResult.UNKNOWN;

    /** Available space to fill in any comments about this Mohawk+T policy */
    public String comment = "";
    /** Will be filled in if a generator was used to create this instance */
    public String generatorName = "";
    /** 
     * Stores the result from the method {@link MohawkT#generateComment(Boolean)}; 
     * DEFAULT: null, must call {@link MohawkT#generateComment(Boolean)} to populate this field.
     */
    public String generatedComment = null;
    /** If this instance was read in from a file, the original filename will be here */
    public String policyFilename = "<Filename Not Provided>";

    public MohawkT(String generatorName) {
        this.generatorName = generatorName;
    }

    public MohawkT() {
        this("No Generator Name Provided");
    }

    public Integer numberOfRules() {
        return canAssign.size() + canDisable.size() + canEnable.size() + canRevoke.size();
    }

    public Integer numberOfRoles() {
        return roleHelper.numberOfRoles();
    }

    public Integer numberOfAdminRoles() {
        return roleHelper.numberOfAdminRoles();
    }

    public Integer numberOfTimeslots() {
        return timeIntervalHelper.getNumberOfTimeSlots();
    }

    /** Returns the MohawkT String that can be put into a file and parsed by Mohawk-Converter
     * 
     * @return String that represents a MohawkT testcase */
    public String getString(String sectionDivider, Boolean queryAtTop, Boolean verboseComments) {
        StringBuilder s = new StringBuilder();

        if (sectionDivider == null) {
            sectionDivider = "\n\n";
        }

        if (queryAtTop == null) {
            queryAtTop = true;
        }

        if (verboseComments == null) {
            verboseComments = true;
        }

        s.append(getCommentHeader(verboseComments)).append(sectionDivider);

        if (queryAtTop) {
            s.append(query.getString()).append(sectionDivider);
            s.append(expectedResult.getString()).append(sectionDivider);
        }

        s.append(canAssign.getString(verboseComments)).append(sectionDivider);
        s.append(canRevoke.getString(verboseComments)).append(sectionDivider);
        s.append(canEnable.getString(verboseComments)).append(sectionDivider);
        s.append(canDisable.getString(verboseComments));

        if (queryAtTop == false) {
            s.append(sectionDivider);
            s.append(query.getString()).append(sectionDivider);
            s.append(expectedResult.getString());
        }

        return s.toString();
    }

    public void generateComment(Boolean verboseComments) {
        StringBuilder s = new StringBuilder();

        s.append("Generated On        : " + MohawkConsoleFormatter.getDateTime(System.currentTimeMillis()) + "\n");
        s.append("Generated With      : " + generatorName + "\n");
        s.append("Number of Roles     : " + numberOfRoles() + "\n");
        s.append("Number of Timeslots : " + numberOfTimeslots() + "\n");
        s.append("Number of Rules     : " + numberOfRules() + "\n");

        if (verboseComments) {
            roleHelper.setupSortedRoles();
            s.append("\n");
            s.append("Roles     : " + roleHelper.toString()).append("\n");
            s.append("Timeslots : " + timeIntervalHelper.toString()).append("\n");
        }

        generatedComment = s.toString();
    }

    public String getCommentHeader(Boolean verboseComments) {
        if (generatedComment == null) {
            generateComment(verboseComments);
        }

        String tmp = generatedComment + "\n\nTESTCASE COMMENTS:\n" + comment;

        tmp = tmp.replaceAll("\n", "\n * ");

        return "/* " + tmp + "\n */";
    }

    public static String getCSVHeader() {
        StringBuilder sb = new StringBuilder();
        sb.append("Filename").append(",");
        sb.append("Timeslots").append(",");
        sb.append("Roles").append(",");
        sb.append("Claimed Roles").append(",");
        sb.append("Total Rules").append(",");
        sb.append("CanAssign Rules").append(",");
        sb.append("CanRevoke Rules").append(",");
        sb.append("CanEnable Rules").append(",");
        sb.append("CanDisable Rules").append(",");
        sb.append("Truely Startable CA Rules").append(",");
        sb.append("Truely Startable CR Rules").append(",");
        sb.append("Truely Startable CE Rules").append(",");
        sb.append("Truely Startable CD Rules").append(",");
        sb.append("Startable CA Rules").append(",");
        sb.append("Startable CR Rules").append(",");
        sb.append("Startable CE Rules").append(",");
        sb.append("Startable CD Rules").append(",");
        sb.append("Largest Precondition CA Rules").append(",");
        sb.append("Largest Precondition CR Rules").append(",");
        sb.append("Largest Precondition CE Rules").append(",");
        sb.append("Largest Precondition CD Rules").append(",");
        sb.append("Largest Role Schedule CA Rules").append(",");
        sb.append("Largest Role Schedule CR Rules").append(",");
        sb.append("Largest Role Schedule CE Rules").append(",");
        sb.append("Largest Role Schedule CD Rules");

        sb.append("\n");

        return sb.toString();
    }

    public String getCSVString(String filename, Integer claimedRoles) {
        StringBuilder sb = new StringBuilder();
        // Filename
        sb.append(StringEscapeUtils.escapeCsv(filename)).append(",");
        // Timeslots
        sb.append(numberOfTimeslots()).append(",");
        // Roles
        sb.append(numberOfRoles()).append(",");
        // Claimed Roles
        sb.append(claimedRoles).append(",");
        // Total Rules
        sb.append(numberOfRules()).append(",");
        // CanAssign Rules
        sb.append(canAssign.numberOfRules()).append(",");
        // CanRevoke Rules
        sb.append(canRevoke.numberOfRules()).append(",");
        // CanEnable Rules
        sb.append(canEnable.numberOfRules()).append(",");
        // CanDisable Rules
        sb.append(canDisable.numberOfRules()).append(",");
        // Truely Startable CA Rules
        sb.append(canAssign.trulyStartableRules).append(",");
        // Truely Startable CR Rules
        sb.append(canRevoke.trulyStartableRules).append(",");
        // Truely Startable CE Rules
        sb.append(canEnable.trulyStartableRules).append(",");
        // Truely Startable CD Rules
        sb.append(canDisable.trulyStartableRules).append(",");
        // Startable CA Rules
        sb.append(canAssign.startableRules).append(",");
        // Startable CR Rules
        sb.append(canRevoke.startableRules).append(",");
        // Startable CE Rules
        sb.append(canEnable.startableRules).append(",");
        // Startable CD Rules
        sb.append(canDisable.startableRules).append(",");
        // Largest Precondition CA Rules
        sb.append(canAssign.largestPrecondtion).append(",");
        // Largest Precondition CR Rules
        sb.append(canRevoke.largestPrecondtion).append(",");
        // Largest Precondition CE Rules
        sb.append(canEnable.largestPrecondtion).append(",");
        // Largest Precondition CD Rules
        sb.append(canDisable.largestPrecondtion).append(",");
        // Largest Role Schedule CA Rules
        sb.append(canAssign.largestRoleSchedule).append(",");
        // Largest Role Schedule CR Rules
        sb.append(canRevoke.largestRoleSchedule).append(",");
        // Largest Role Schedule CE Rules
        sb.append(canEnable.largestRoleSchedule).append(",");
        // Largest Role Schedule CD Rules
        sb.append(canDisable.largestRoleSchedule);

        sb.append("\n");
        return sb.toString();
    }

    public ArrayList<Rule> getAllRules() {
        ArrayList<Rule> allRules = new ArrayList<Rule>();
        allRules.addAll(canAssign._rules);
        allRules.addAll(canRevoke._rules);
        allRules.addAll(canEnable._rules);
        allRules.addAll(canDisable._rules);
        return allRules;
    }

    public Boolean addRule(Rule newRule) {
        return addRule(newRule, true);
    }

    public Boolean addRule(Rule newRule, boolean addToHelpers) {
        if (newRule == null) { return false; }
        if (newRule._type == null) { return false; }

        switch (newRule._type) {
            case ASSIGN :
                canAssign.addRule(newRule);
                break;
            case DISABLE :
                canDisable.addRule(newRule);
                break;
            case ENABLE :
                canEnable.addRule(newRule);
                break;
            case REVOKE :
                canRevoke.addRule(newRule);
                break;
            default :
                return false;
        }

        if (addToHelpers) {
            roleHelper.add(newRule);
            timeIntervalHelper.add(newRule);
        }

        return true;
    }

    public Boolean addQuery(Query q, boolean addToHelpers) {
        if (q == null) { return false; }
        if (q._roles == null) { return false; }
        if (q._timeslot == null) { return false; }

        query = new Query(q);

        if (addToHelpers) {
            roleHelper.addAll(q._roles);
            timeIntervalHelper.add(q._timeslot);
        }

        return true;
    }

    /** Adds all of the rules given to it.
     * 
     * @param rules a list of rules */
    public void addRules(ArrayList<Rule> rules) {
        for (Rule r : rules) {
            addRule(r);
        }
    }

    @Override
    public String toString() {
        StringBuilder t = new StringBuilder();
        t.append(query + "\n");
        t.append(expectedResult + "\n");
        t.append(canAssign + "\n");
        t.append(canRevoke + "\n");
        t.append(canEnable + "\n");
        t.append(canDisable + "\n");

        return t.toString();
    }

    public boolean ruleOverlapTimeslot(Rule r, Integer time) {
        boolean overlap = false;

        // All Time
        if (r._roleSchedule.isEmpty()) { return true; }

        for (TimeSlot ts : r._roleSchedule) {
            Integer t = timeIntervalHelper.indexOfReduced(ts).first();

            if (t.equals(time)) { return true; }
        }

        return overlap;
    }

    /** Assigns each rule in a section with its own index number */
    public void indexAllRules() {
        canAssign.indexRules();
        canRevoke.indexRules();
        canEnable.indexRules();
        canDisable.indexRules();
    }

    /** Removes all of the rules from each block */
    public void clearAllRules() {
        canAssign.clearRules();
        canRevoke.clearRules();
        canEnable.clearRules();
        canDisable.clearRules();
    }

    /** 
     * Copies metadat from a policy into this one. Metadata includes:
     * <ul>
     * <li>{@link MohawkT#comment}
     * <li>{@link MohawkT#expectedResult}
     * <li>{@link MohawkT#generatorName}
     * <li>{@link MohawkT#policyFilename}
     * </ul>
     * @param origPolicy
     */
    public void copyMetaData(MohawkT origPolicy) {
        this.comment = origPolicy.comment;
        this.expectedResult = origPolicy.expectedResult;
        this.generatorName = origPolicy.generatorName;
        this.policyFilename = origPolicy.policyFilename;
    }

}
