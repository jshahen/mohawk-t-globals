package mohawk.global.pieces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;

public class Rule {
    public static final Logger logger = Logger.getLogger("mohawk");

    /** Only used in NuSMV reduction */
    public Integer ruleIndex = null;

    public RuleType _type;
    public Role _adminRole;
    public TimeInterval _adminTimeInterval;
    /** A list of roles, where inside of the role the NOT condition is listed
     * If EMPTY == No Precondition (TRUE) */
    public ArrayList<Role> _preconditions = new ArrayList<Role>();
    /** The role schedule that the precondition must meet (ALL OF THEM).
     * If the role schedule is empty == all timeslots */
    public ArrayList<TimeSlot> _roleSchedule = new ArrayList<TimeSlot>();//
    /** The target role of this rule */
    public Role _role;
    public String _comment = "";

    protected Boolean _isRuleStartable = true;// is the rule

    public Rule() {}

    public Rule(RuleType type, Role adminRole, TimeInterval adminTimeInterval, ArrayList<Role> preconditions,
            ArrayList<TimeSlot> roleSchedule, Role role, String comment) {
        _type = type;
        _adminRole = adminRole;
        _adminTimeInterval = adminTimeInterval;
        _preconditions.addAll(preconditions);
        _roleSchedule.addAll(roleSchedule);
        _role = role;
        _comment = comment;
    }

    public Rule(RuleType type, Role adminRole, TimeInterval adminTimeInterval, ArrayList<Role> preconditions,
            ArrayList<TimeSlot> roleSchedule, Role role) {
        this(type, adminRole, adminTimeInterval, preconditions, roleSchedule, role, "");
    }

    /** Convert a Query into a Can Assign rule
     * 
     * @param query
     * @param goalRole */
    public Rule(Query query, Role goalRole) {
        _type = RuleType.ASSIGN;
        _adminRole = Role.allRoles();
        _adminTimeInterval = query._timeslot;
        _preconditions.addAll(query._roles);
        _roleSchedule.add(query._timeslot);
        _role = goalRole;
    }

    /** Deep Copy Constructor
     * 
     * @param rule */
    public Rule(Rule rule) {
        _type = rule._type;
        _adminRole = new Role(rule._adminRole);
        _adminTimeInterval = new TimeInterval(rule._adminTimeInterval);

        for (Role r : rule._preconditions) {
            _preconditions.add(new Role(r));
        }

        for (TimeSlot ts : rule._roleSchedule) {
            _roleSchedule.add(new TimeSlot(ts));
        }

        _role = new Role(rule._role);
    }

    /** Returns the MohawkT representation of a RoleSchedule
     * 
     * @return */
    public static String getRoleScheduleString(ArrayList<TimeSlot> roleSchedule) {
        if (roleSchedule.size() == 0) { return "TRUE"; }

        if (roleSchedule.size() == 1) { return roleSchedule.get(0).getString(); }

        StringBuffer s = new StringBuffer();
        s.append("[");
        for (int i = 0; i < roleSchedule.size(); i++) {
            s.append(roleSchedule.get(i));

            if (i != roleSchedule.size() - 1) {
                s.append(", ");
            }
        }
        s.append("]");

        return s.toString();
    }

    /** Returns the MohawkT representation of the Preconditions
     * 
     * @return */
    public static String getPreconditionString(ArrayList<Role> preconditions) {
        if (preconditions.size() == 0) { return "TRUE"; }

        StringBuffer s = new StringBuffer();

        for (int i = 0; i < preconditions.size(); i++) {
            if (i != 0) {
                s.append(" & ");
            }
            s.append(preconditions.get(i).getString());
        }

        return s.toString();
    }

    public static String getInfoCodeHeader() {
        StringBuilder sb = new StringBuilder();

        sb.append("/*\nThe returned code is of the form: TA{1Tru, 2Neg, 3Pos, 4Mix}\n").append("Where:\n")
                .append("  * T - means Truly Startable (Admin is TRUE and is Startable)\n")
                .append("  * A - means Admin is TRUE\n").append("  * 1Tru - means the Precondition is TRUE\n")
                .append("  * 2Neg - means the Precondition is only Negative roles\n")
                .append("  * 3Pos - means the Precondition is only Positive roles\n")
                .append("  * 4Mix - means the Precondition is mixed positive and negative roles\n*/");

        return sb.toString();
    }

    /** Checks if the admin role is TRUE
     * 
     * @return */
    public Boolean isAdminRoleTrue() {
        return _adminRole.isAllRoles();
    }

    /** A Startable Rule is a rule that requires no pre-existing role assignment, which means that if the admin user is
     * available this rule can give an initial rule
     * 
     * @return whether the rule is startable or not */
    public Boolean isRuleStartable() {
        return _isRuleStartable;
    }

    /** A Truly Startable rule is one where the rule is startable and the administrator is TRUE
     * 
     * @return whether the rule is truly startable or not */
    public Boolean isRuleTrulyStartable() {
        return _isRuleStartable && _adminRole.isAllRoles();
    }

    /** Checks to see if there exists a pair of roles where their name is the same but one is POSITIVE and the other is
     * the NEGATION (_not == TRUE)
     * 
     * @return */
    public boolean isInvokablePrecondition() {
        HashMap<String, Boolean> roleMap = new HashMap<String, Boolean>();

        Boolean tmp = null;
        for (Role r : _preconditions) {
            tmp = roleMap.get(r._name);
            if (tmp != null) {
                if (!r._not.equals(tmp)) { return false; }

                continue;
            }

            roleMap.put(r._name, r._not);
        }

        return true;
    }

    /** checks to see if the Rule is a Startable Rule
     * 
     * @return whether the rule is startable or not */
    public Boolean checkIsRuleStartable() {
        _isRuleStartable = true;

        for (Role r : _preconditions) {
            _isRuleStartable = _isRuleStartable & (r._not || r.isAllRoles());
        }

        return _isRuleStartable;
    }

    public Boolean checkIsRuleTrulyStartable() {
        return checkIsRuleStartable() && _adminRole.isAllRoles();
    }

    public Boolean checkIsRuleNoAdminNeeded() {
        return _adminRole.isAllRoles();
    }

    /** If the rule has TRUE for its precondition
     * 
     * @return (precondition == TRUE) */
    public Boolean noConditions() {
        return _preconditions.size() == 0;
    }

    /** Provides an informative code about the Rule's precondition; used for sorting
     * Tru - means the Precondition is TRUE
     * Neg - means the Precondition is only Negative roles
     * Pos - means the Precondition is only Positive roles
     * Mix - means the Precondition is mixed positive and negative roles
     * 
     * @return */
    public String getPreconditionCode() {
        boolean p = false, n = false;
        for (Role r : _preconditions) {
            if (r._not) {
                n = true;
            } else {
                p = true;
            }

            // don't need to look after we find 1 negative and 1 positive role
            if (n && p) {
                break;
            }
        }

        if (n && p) {
            return "Mix";
        } else if (p) {
            return "Pos";
        } else if (n) {
            return "Neg";
        } else {
            return "Tru";
        }
    }

    /** Returns the MohawkT representation of this class
     * 
     * @return */
    public String getString(Integer currentRuleNumber, Integer padding) {
        StringBuilder s = new StringBuilder();

        if (currentRuleNumber != null) {
            s.append("/* C").append(_type._shortform + "-")
                    .append(StringUtils.leftPad(currentRuleNumber.toString(), padding, '0')).append(" ")
                    .append(getInfoCode()).append(" */  ");
        }

        s.append("<").append(_adminRole.getString()).append(", ").append(_adminTimeInterval.getString()).append(", ")
                .append(getPreconditionString(_preconditions)).append(", ").append(getRoleScheduleString(_roleSchedule))
                .append(", ").append(_role.getString()).append(">");

        return s.toString();
    }

    /** Returns a short string that can be used to generate a hashcode
     * 
     * @return */
    public String getShortString() {
        StringBuilder s = new StringBuilder();
        s.append("<").append(_adminRole.getString()).append(", ").append(_adminTimeInterval.getString()).append(", ")
                .append(_preconditions.size()).append(", ").append(_roleSchedule.size()).append(", ")
                .append(_role.getString()).append(">");

        return s.toString();
    }

    /** The returned code is of the form: TA{1Tru, 2Neg, 3Pos, 4Mix}
     * Where:
     * * T - means Truly Startable (Admin is TRUE and is Startable)
     * * A - means Admin is TRUE
     * * 1Tru - means the Precondition is TRUE
     * * 2Neg - means the Precondition is only Negative roles
     * * 3Pos - means the Precondition is only Positive roles
     * * 4Mix - means the Precondition is mixed positive and negative roles
     * 
     * @return A code that can be sorted to display the most relevant rules first */
    public String getInfoCode() {
        String notChar = "X";
        StringBuilder code = new StringBuilder();

        if (checkIsRuleTrulyStartable()) {
            code.append("T");
        } else {
            code.append(notChar);
        }

        if (checkIsRuleNoAdminNeeded()) {
            code.append("A");
        } else {
            code.append(notChar);
        }

        String condCode = getPreconditionCode();
        switch (condCode) {
            case "Tru" :
                code.append("1" + condCode);
                break;
            case "Neg" :
                code.append("2" + condCode);
                break;
            case "Pos" :
                code.append("3" + condCode);
                break;
            case "Mix" :
                code.append("4" + condCode);
                break;
            default :
                code.append("???");
        }

        return code.toString();
    }

    /** Converts the current rule to have the AllTime for the role schedule */
    public void roleSceduleToAllTime() {
        _roleSchedule.clear();
    }

    /** Changes the rules in the preconditions and the target role with their suffix and the count
     * 
     * @param suffix
     * @param count */
    public ArrayList<Role> addSuffix(String suffix, int count, boolean deepCopy) {
        ArrayList<Role> result = new ArrayList<Role>();

        this._role = new Role(this._role, suffix, count);
        if (deepCopy) {
            result.add(new Role(this._role));
        } else {
            result.add(this._role);
        }

        ArrayList<Role> tmp = _preconditions;
        _preconditions = new ArrayList<Role>();
        for (Role r : tmp) {
            _preconditions.add(new Role(r, suffix, count));
            if (deepCopy) {
                result.add(new Role(r, suffix, count));
            }
        }

        if (!deepCopy) {
            result.addAll(_preconditions);
        }

        return result;
    }

    public ArrayList<Role> addSuffix(String suffix, int count) {
        return addSuffix(suffix, count, false);
    }

    /** If the condition role is found in the precondition the addition role is added to the preconditions
     * 
     * @param condition
     * @param addition
     * @param negateCondtion
     *            if TRUE then the condition's _not variable is negated */
    public void addConditionalPrecondition(Role condition, Role addition, Boolean negateCondtion) {
        Role c = new Role(condition);

        if (negateCondtion) {
            c._not = !c._not;
        }

        if (_preconditions.contains(condition)) {
            _preconditions.add(addition);
        }
    }

    public void addConditionalPrecondition(HashMap<Role, Role> role2role) {
        ArrayList<Role> newPrecondition = new ArrayList<Role>();

        Role tmp;
        for (Role r : _preconditions) {
            tmp = role2role.get(r);
            if (tmp == null) {
                // nothing to change
                tmp = r;
            } else {
                tmp._not = r._not;
            }
            newPrecondition.add(tmp);
        }
    }

    /** Converts all precondition roles to ENABLED
     * 
     * @return new list of preconditions */
    public ArrayList<Role> rolesEnabled() {
        for (Role r : _preconditions) {
            r.changeType(RoleType.ENABLED);
        }
        return _preconditions;
    }

    public ArrayList<Role> rolesNotEnabledCombined() {
        ArrayList<Role> newRoles = new ArrayList<Role>();

        Role tmp;
        for (Role r : _preconditions) {
            if (r.getType() == RoleType.ENABLED) {
                if (r._not) {
                    tmp = new Role(r);

                    tmp.changeType(RoleType.COMBINED);

                    newRoles.add(tmp);
                }
            } else {
                logger.warning("How is there a role that is not ENABLED? " + r);
            }
        }

        for (Role r : newRoles) {
            _preconditions.add(r);
        }

        return newRoles;
    }

    /** Updates all the internal statistics of a rule when it is done being constructed */
    public void update() {
        checkIsRuleStartable();
    }

    @Override
    public String toString() {
        if (_type == null) { return "<Unknown Type Rule>"; }

        StringBuffer s = new StringBuffer();
        s.append(_type).append(" [").append(getInfoCode()).append("] <").append(_adminRole.toString()).append(", ")
                .append("[t").append(_adminTimeInterval.getStartTime()).append("-t")
                .append(_adminTimeInterval.getFinishTime()).append("], ");

        if (_preconditions.size() == 0) {
            s.append("TRUE, ");
        } else {
            s.append(_preconditions.toString()).append(", ");
        }
        s.append(getRoleScheduleString(_roleSchedule)).append(", ").append(_role.toString()).append(">");

        return s.toString();
    }

    // **********************************************************
    // SET Functions
    // **********************************************************
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Rule) {
            Rule o = (Rule) obj;
            boolean result = _type == o._type && _adminRole.equals(o._adminRole)
                    && _adminTimeInterval.equals(o._adminTimeInterval) && _role.equals(o._role);

            if (result == true) {
                result = _preconditions.containsAll(o._preconditions);
                result = (result == false) ? false : _roleSchedule.containsAll(o._roleSchedule);

                return result;
            }
        }

        return false;
    }

    @Override
    public int hashCode() {
        return getShortString().hashCode();
    }

    /** Returns the Rule Index with a prefix for the rule type
     * 
     * @return */
    public String getRuleIndex() {
        if (ruleIndex == null) { return "C" + _type._shortform + "??"; }
        return "C" + _type._shortform + ruleIndex;
    }

    /** Simple function to get the negative precondition roles
     * 
     * @return */
    public ArrayList<Role> getNegativePreconditionRoles() {
        ArrayList<Role> negRoles = new ArrayList<>();

        for (Role r : _preconditions) {
            if (!r.isPositivePrecondition()) {
                negRoles.add(r);
            }
        }

        return negRoles;
    }

    /** Simple function to get the positive precondition roles
     * 
     * @return */
    public ArrayList<Role> getPositivePreconditionRoles() {
        ArrayList<Role> posRoles = new ArrayList<>();

        for (Role r : _preconditions) {
            if (r.isPositivePrecondition()) {
                posRoles.add(r);
            }
        }

        return posRoles;
    }
}
