package mohawk.global.pieces;

import mohawk.global.helper.RoleHelper;

public class Role implements Comparable<Role> {
    protected String _name;
    protected String _suffix = "";
    /** Flag used to indicate if the role is a negative or positive condition.
     * When TRUE this role is being used as in the negative in a rule's
     * precondition */
    public Boolean _not;
    private Boolean _allRoles = false;
    private RoleType _type = RoleType.ASSIGNED;

    public Role(String name, String suffix, Boolean not, Boolean isAllRoles, RoleType type) {
        this._name = name;
        this._suffix = suffix;
        this._not = not;
        this._allRoles = isAllRoles;
        this._type = type;
    }

    public Role(String name, String suffix, Boolean not, Boolean isAllRoles) {
        this(name, suffix, not, isAllRoles, RoleType.ASSIGNED);
    }

    public Role(String name, String suffix, Boolean not) {
        this(name, suffix, not, false);
    }

    public Role(String name, String suffix) {
        this(name, suffix, false);
    }

    public Role(String name) {
        this(name, "");
    }

    /** Performs a deep copy of the role and add the string "_suffix" to the end
     * of the string
     * 
     * @param role
     * @param suffix */
    public Role(Role role, Integer suffix) {
        this(role, null, suffix);
    }

    public Role(Role role, String suffixStr, Integer suffix) {
        this(role);

        if (suffixStr == null) {
            suffixStr = "_";
        }
        _suffix += new String(suffixStr + suffix);
    }

    /** Deep Copy Constructor
     * 
     * @param role */
    public Role(Role role) {
        this(role._name, role._suffix, role._not, role.isAllRoles(), role.getType());
    }

    /** Deep Copy Constructor with a suffix
     * 
     * @param role
     * @param suffix */
    public Role(Role role, String suffix) {
        this(role);
        _suffix += suffix;
    }

    /** Deep Copy Constructor with a suffix
     * 
     * @param role
     * @param ti
     *            Allows for standard role names with timeintervals */
    public Role(Role role, TimeInterval ti) {
        this(role, ti.standardRoleName());
    }

    public void copy(Role role) {
        _name = role._name;
        _not = role._not;
        _allRoles = role.isAllRoles();
    }

    public void copyDeep(Role role) {
        _name = new String(role._name);
        _not = new Boolean(role._not);
        _allRoles = new Boolean(role.isAllRoles());
    }

    /** Returns a new role that represents the AllRoles object, represented as
     * 'TRUE' in Mohawk-T
     * 
     * @return */
    public static Role allRoles() {
        Role r = new Role("");
        r._allRoles = true;

        return r;
    }

    public Boolean isAllRoles() {
        return _allRoles;
    }

    public boolean usesAdminTimeslot() {
        return (getName().indexOf(RoleHelper.UsesAdminTimeslot) != -1);
    }

    public RoleType getType() {
        return _type;
    }

    /** Returns the role name, it changes based on the RoleType
     * 
     * @return */
    public String getName() {
        String str = "";
        switch (_type) {
            case ASSIGNED :
                str = "";
                break;
            case ENABLED :
                str = "_ENABLED";
                break;
            case COMBINED :
                str = "_COMBINED";
                break;
        }

        return _name + str + _suffix;
    }

    /** Returns the MohawkT representation of this class
     * 
     * @return */
    public String getString() {
        if (_allRoles == true) { return "TRUE"; }
        if (_not == false) { return getName(); }
        return "NOT ~ " + getName();
    }

    public Role getNot() {
        Role r = new Role(this);
        r._not = true;
        return r;
    }

    public LimitedRole toLimitedRole() {
        return new LimitedRole(this);
    }

    public Role changeType(RoleType type) {
        _type = type;

        return this;
    }

    public Role suffix(String suffix) {
        this._suffix += suffix;
        return this;
    }

    @Override
    public String toString() {
        if (_allRoles == true) { return "TRUE"; }
        if (_not == false) { return getName(); }
        return "NOT " + getName();
    }

    /** Very Important for RoleHelper with respect to the SET exclusion */
    @Override
    public int hashCode() {
        return getName().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Role) {
            Role r = (Role) obj;
            // if (_name.equals(r._name) && _not.equals(r._not) &&
            // _allRoles.equals(r._allRoles)) { return true; }
            if (getName().equals(r.getName())) { return true; }
        } else if (obj instanceof String) {
            String r = (String) obj;
            return getName().equals(r);
        }
        return false;
    }

    @Override
    public int compareTo(Role o) {
        // if (this._allRoles && o._allRoles) { return 0; }
        //
        // if (this._allRoles && !o._allRoles) { return 1; }
        //
        // if (!this._allRoles && o._allRoles) { return -1; }

        int nameCompare = getName().compareTo(o.getName());

        // if (nameCompare == 0) {
        // if (this._not && !o._not) { return -1; }
        //
        // if (!this._not && o._not) { return 1; }
        //
        // return 0;
        // }

        return nameCompare;
    }

    /** Returns TRUE when the role is a positive precondition, and FALSE if it is
     * a negative precondition
     * 
     * @return */
    public boolean isPositivePrecondition() {
        if (_not == null) { return true; }
        return (_not == false);
    }
}
