package mohawk.global.pieces.reduced.nusmv;

import java.util.SortedSet;

import mohawk.global.pieces.*;

public class NuSMVRule_Case {
    public String condition;
    public Boolean trueResult = null;

    public NuSMVRule_Case(MohawkT m, Rule r) {
        condition = getNuSMVCondition(m, r);
        if (r._type == RuleType.ASSIGN || r._type == RuleType.ENABLE) {
            trueResult = true;
        } else {
            trueResult = null;
        }
    }

    public static String getNuSMVCondition(MohawkT m, Rule r) {
        StringBuilder sb = new StringBuilder();
        sb.append("rule=").append(r.getRuleIndex());

        // Only add admin conditions if required
        if (!r.isAdminRoleTrue()) {
            SortedSet<Integer> timeslots = m.timeIntervalHelper.indexOfReduced(r._adminTimeInterval);
            String uStr = "(u[admin][" + m.roleHelper.indexOf(r._adminRole, true) + "][";
            String reStr = "e[" + m.roleHelper.indexOf(r._adminRole, true) + "][";

            sb.append(" & ( ");
            boolean first = true;
            for (Integer t : timeslots) {
                if (!first) {
                    sb.append("|");
                } else {
                    first = false;
                }
                sb.append(uStr).append(t).append("]&").append(reStr).append(t).append("])");
            }
            sb.append(" )");
        }

        // only add precondition conditions if required
        if (!r.noConditions()) {
            for (Role role : r._preconditions) {
                if (role._not) {
                    sb.append(" & !(");
                } else {
                    sb.append(" & (");
                }

                String roleStr;
                if (r._type == RuleType.ASSIGN || r._type == RuleType.REVOKE) {
                    roleStr = "u[user][" + m.roleHelper.indexOf(role, true) + "][";
                } else {
                    roleStr = "e[" + m.roleHelper.indexOf(role, true) + "][";
                }
                boolean first = true;
                for (TimeSlot t : r._roleSchedule) {
                    if (!first) {
                        if (role._not) {
                            sb.append("|");
                        } else {
                            sb.append("&");
                        }
                    } else {
                        first = false;
                    }
                    sb.append(roleStr).append(m.timeIntervalHelper.indexOfReduced(t).first()).append("]");
                }

                sb.append(")");
            }
        }

        return sb.toString();
    }
}
