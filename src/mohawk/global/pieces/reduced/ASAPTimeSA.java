package mohawk.global.pieces.reduced;

import java.util.ArrayList;

import mohawk.global.pieces.reduced.query.ASAPTimeSA_Query;
import mohawk.global.pieces.reduced.rules.ASAPTimeSA_Rule;

public class ASAPTimeSA {
    public String generatorName = ""; // to be filled in with the generator
    public String comment = ""; // to be filled in with the generator

    public Integer numberOfRoles;
    public Integer numberOfTimeslots;
    public ASAPTimeSA_Query query = new ASAPTimeSA_Query();
    public ArrayList<ASAPTimeSA_Rule> rules = new ArrayList<ASAPTimeSA_Rule>();

    public ASAPTimeSA(String generatorName) {
        this.generatorName = generatorName;
    }

    public boolean addRule(ASAPTimeSA_Rule newRule) {
        rules.add(newRule);
        return true;
    }

    public String getString() {
        return getString(false);
    }

    public String getString(boolean getSimpleNSA) {
        StringBuilder s = new StringBuilder();

        s.append("CONFIG " + numberOfRoles + " " + numberOfTimeslots + "\n");
        s.append("GOAL " + query.goalRole + " " + query.goalTimeslot + "\n");

        for (ASAPTimeSA_Rule r : rules) {
            s.append(r.getString(getSimpleNSA)).append("\n");
        }
        return s.toString();
    }
}
