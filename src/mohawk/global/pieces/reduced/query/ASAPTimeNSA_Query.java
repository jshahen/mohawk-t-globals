package mohawk.global.pieces.reduced.query;

public class ASAPTimeNSA_Query {
    public Integer goalRole;
    public Integer goalTimeslot;

    @Override
    public String toString() {
        return "ASAPTimeNSA_Query{goalRole: " + goalRole + "; goalTimeslot: " + goalTimeslot + "}";
    }
}
