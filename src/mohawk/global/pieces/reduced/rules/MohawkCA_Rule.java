package mohawk.global.pieces.reduced.rules;

import java.util.ArrayList;
import java.util.logging.Logger;

import mohawk.global.helper.RoleHelper;
import mohawk.global.pieces.*;

public class MohawkCA_Rule {
    public static final Logger logger = Logger.getLogger("mohawk");

    public String role;
    public ArrayList<String> precondition;

    public MohawkCA_Rule(Rule rule) {
        role = rule._role.getName();

        precondition = new ArrayList<String>();
        for (Role role : rule._preconditions) {
            if (role._not) {
                precondition.add("-" + role.getName());
            } else {
                precondition.add(role.getName());
            }
        }
    }

    public MohawkCA_Rule(Rule rule, String suffix) {
        role = rule._role.getName() + suffix;

        precondition = new ArrayList<String>();
        String pre = "";
        String suf = "";
        for (Role role : rule._preconditions) {
            if (role._not) {
                pre = "-";
            } else {
                pre = "";
            }

            if (role.usesAdminTimeslot()) {
                suf = rule._adminTimeInterval.standardRoleName();
            } else {
                suf = suffix;
            }

            precondition.add(pre + role.getName() + suf);
        }
    }

    public MohawkCA_Rule(Rule r, TimeInterval ti) {
        this(r, ti.standardRoleName());
    }

    public void shortRolenames(RoleHelper rh) {
        try {
            role = "r" + rh.indexOf(role);
        } catch (Exception e) {
            logger.warning("Caught an exception with rule: " + this);
            throw e;
        }

        String s, ss, pre;
        for (int i = 0; i < precondition.size(); i++) {
            s = precondition.get(i);

            if (s.startsWith("-")) {
                pre = "-";
                s = s.substring(1);// remove the -
            } else {
                pre = "";
            }

            try {
                ss = pre + "r" + rh.indexOf(s);
            } catch (Exception e) {
                logger.warning("Caught an exception with rule: " + this);
                throw e;
            }
            precondition.set(i, ss);
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("MohawkCA_Rule <").append(role).append(", ").append(precondition).append(">");

        return sb.toString();
    }
}
