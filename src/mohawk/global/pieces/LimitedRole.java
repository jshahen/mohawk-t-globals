package mohawk.global.pieces;

/** @author Jonathan Shahen */
public class LimitedRole extends Role {

    public LimitedRole(Role role) {
        super(role._name, role._suffix, false, false, role.getType());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Role) {
            Role r = (Role) obj;
            return getName().equals(r.getName());
        }

        if (obj instanceof LimitedRole) {
            LimitedRole lr = (LimitedRole) obj;
            return getName().equals(lr.getName());
        }

        if (obj instanceof String) {
            String lr = (String) obj;
            return getName().equals(lr);
        }

        return false;
    }
}
