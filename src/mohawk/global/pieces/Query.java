package mohawk.global.pieces;

import java.util.ArrayList;

public class Query {
    public TimeSlot _timeslot;
    public ArrayList<Role> _roles = new ArrayList<Role>();

    public Query() {}

    /** Deep copy constructor
     * 
     * @param query */
    public Query(Query query) {
        _timeslot = new TimeSlot(query._timeslot);

        for (Role r : query._roles) {
            _roles.add(new Role(r));
        }
    }

    @Override
    public String toString() {
        return "Query: " + _timeslot + ", " + _roles;
    }

    /** Returns the MohawkT representation of this class
     * 
     * @return */
    public String getString() {
        StringBuilder s = new StringBuilder();

        s.append("Query: " + _timeslot + ", [");

        Role r;
        for (int i = 0; i < _roles.size(); i++) {
            if (i != 0) {
                s.append(", ");
            }
            r = _roles.get(i);
            s.append(r.getString());
        }

        s.append("]");

        return s.toString();
    }
}
