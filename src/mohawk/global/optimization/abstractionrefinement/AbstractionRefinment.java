package mohawk.global.optimization.abstractionrefinement;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import mohawk.global.helper.RoleHelper;
import mohawk.global.helper.TimeIntervalHelper;
import mohawk.global.pieces.*;

public class AbstractionRefinment {
    public static final Logger logger = Logger.getLogger("mohawk");

    public MohawkT origPolicy = null;
    public MohawkT nextPolicy = null;

    public RoleHelper rh = null;
    public TimeIntervalHelper tih = null;

    private int refinementStep = 0;
    public Set<Role> curR;
    public Set<Integer> curT;
    public boolean curRChanged = false;
    public boolean curTChanged = false;

    public AbstractionRefinment(MohawkT orig) {
        origPolicy = orig;
        rh = orig.roleHelper;
        tih = orig.timeIntervalHelper;
    }

    /** Returns what number of refinement steps the class has performed.
     * 
     * @return number of refinement steps so far. Only increments if a change was detected. */
    public int getRefinementStep() {
        return refinementStep;
    }

    /** Quick function to return the policy({@link AbstractionRefinment#nextPolicy}) that was created
     * using {@link AbstractionRefinment#calculateNextPolicy()}.
     * 
     * @return */
    public MohawkT getRefinedPolicy() {
        return nextPolicy;
    }

    /** Creates the next refinement step and stores the new policy.
     * 
     * @return An error string to indicate the status of this function.
     *         The following error values exists:
     *         <ul>
     *         <li><b>""</b> (Empty String) - Indicates the function ran correctly</li>
     *         <li><b>No Refinements Found\n</b> - The last refinement step changed nothing from it's previous step</li>
     *         </ul>
    */
    public String calculateNextPolicy() {
        String errStr = "";

        curRChanged = false;
        curTChanged = false;

        if (nextPolicy == null) {
            errStr += initialStep();
        } else {
            errStr += refinementStep();
        }

        // Check if we have a new refinement step
        if (curRChanged || curTChanged) {
            refinementStep++;

            // Increase the policy name
            nextPolicy.generatorName = "Abstraction-Refinement Step " + refinementStep;
        } else {
            errStr += "No Refinements Found\n";
        }

        return errStr;
    }

    private String initialStep() {
        String errStr = "";

        // Create empty sets and policy to start with
        curR = new HashSet<Role>();
        curT = new HashSet<Integer>();
        nextPolicy = new MohawkT();

        // Copy over the metadata from the original policy
        nextPolicy.copyMetaData(origPolicy);

        // Copy the original query and update the Helper Functions
        nextPolicy.query = origPolicy.query;
        nextPolicy.roleHelper.addAll(nextPolicy.query._roles);
        nextPolicy.timeIntervalHelper.add(nextPolicy.query._timeslot);

        // Reduce to Timeslots
        tih.reduceToTimeslots();

        // Add roles from the Query to curR
        curRChanged = curRChanged | curR.addAll(origPolicy.query._roles);

        // Add the Query timeslot
        Set<Integer> ss = tih.indexOfReduced(origPolicy.query._timeslot);
        curTChanged = curTChanged | curT.addAll(ss);

        errStr += addAllRules();

        return errStr;
    }

    /** Loop's through all rules in the origPolicy and adds them to nextPolicy if they satisfy the following:
     * <ul>
     * <li>The target role is in {@link AbstractionRefinment#curR}, and</li>
     * <li>The target time slot array elements have at least 1 timeslot within {@link AbstractionRefinment#curT}</li>
     * </ul>
     * 
     * @return error string (empty string means no error) */
    private String addAllRules() {
        String errStr = "";

        for (Rule r : origPolicy.getAllRules()) {
            if (curR.contains(r._role)) {
                boolean containsAny = false;
                for (TimeSlot t : r._roleSchedule) {
                    containsAny = containsAny | curT.containsAll(tih.indexOfReduced(t));
                }

                if (containsAny) {
                    logger.fine("Adding rule: " + r);
                    // Add the rule to the next policy and update the helpers
                    nextPolicy.addRule(r);
                    nextPolicy.roleHelper.add(r);
                    nextPolicy.timeIntervalHelper.add(r);
                }
            }
        }

        return errStr;
    }

    private String refinementStep() {
        String errStr = "";

        // Loop through rules and add all the roles to curR and all timeslots to curT
        for (Rule r : nextPolicy.getAllRules()) {
            curRChanged = curRChanged | curR.add(r._adminRole);
            curRChanged = curRChanged | curR.add(r._role);
            curRChanged = curRChanged | curR.addAll(r._preconditions);

            curTChanged = curTChanged | curT.addAll(tih.indexOfReduced(r._adminTimeInterval));
            for (TimeSlot ts : r._roleSchedule) {
                curTChanged = curTChanged | curT.addAll(tih.indexOfReduced(ts));
            }
        }

        // Clear out all of the rules
        nextPolicy.clearAllRules();

        errStr += addAllRules();

        return errStr;
    }
}
