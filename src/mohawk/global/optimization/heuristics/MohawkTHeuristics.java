package mohawk.global.optimization.heuristics;

import java.util.*;
import java.util.logging.Logger;

import mohawk.global.pieces.*;
import mohawk.global.results.ExecutionResult;

public class MohawkTHeuristics {
    public final static Logger logger = Logger.getLogger("mohawk");

    public MohawkT m;
    /**  */
    public boolean debug = false;

    public MohawkTHeuristics(MohawkT policy) {
        m = policy;
    }

    /** Check the Mohawk+T policy with certain heuristics to determine the result without running a solver
     * 
     * @return null if no result could be determined, REACHABLE/UNREACHABLE if guaranteed result */
    public ExecutionResult getResult() {
        ExecutionResult result = null;

        result = emptyQuery();
        if (result != null) { return result; }

        result = lessCARulesThanQueryRoles();
        if (result != null) { return result; }

        result = noCARulesForQueryRoles();
        if (result != null) { return result; }

        return result;
    }

    /** If the Query role array is empty then every user satisfies the query and thus the policy is reachable
     * 
     * @return */
    public ExecutionResult emptyQuery() {
        ExecutionResult result = null;

        if (m.query._roles.isEmpty()) {
            result = ExecutionResult.GOAL_REACHABLE;
            logger.info("[HEURISTICS] Setting result = GOAL_REACHABLE because the query has no goal roles.");
        }

        return result;
    }

    /** If the number of CanAssign rules is less than the number of roles in the Query,
     * then it is impossible to have a reachable state (assumed every user is assigned to 0 roles in the beginning)
     * 
     * @return */
    public ExecutionResult lessCARulesThanQueryRoles() {
        ExecutionResult result = null;

        if (m.canAssign.size() < m.query._roles.size()) {
            result = ExecutionResult.GOAL_UNREACHABLE;
            logger.info("[HEURISTICS] Setting result = GOAL_UNREACHABLE because there are less CanAssign rules than "
                    + "goal roles in the query.");
        }

        return result;
    }

    public ExecutionResult noCARulesForQueryRoles() {
        ExecutionResult result = null;

        TimeSlot goalTimeslot = m.query._timeslot;
        Set<Role> goalRoles = new HashSet<Role>(m.query._roles);
        HashMap<Role, Boolean> found = new HashMap<Role, Boolean>(goalRoles.size());

        for (Role r : goalRoles) {
            found.put(r, false);
        }

        for (Rule rule : m.canAssign.getRules()) {
            if (goalRoles.contains(rule._role)) {
                if (found.get(rule._role) == false) {
                    for (TimeSlot ts : rule._roleSchedule) {
                        if (ts.equals(goalTimeslot)) {
                            found.put(rule._role, true);
                        }
                    }
                }
            }
        }

        boolean allFound = true;

        for (Role r : goalRoles) {
            allFound = allFound & found.get(r);
        }

        if (!allFound) {
            result = ExecutionResult.GOAL_UNREACHABLE;
            logger.info("[HEURISTICS] Setting result = GOAL_UNREACHABLE because there does not exist a "
                    + "CanAssign rule for each goal role within the timeslot " + goalTimeslot + ". Found CA Rules: "
                    + found);
        }

        return result;
    }
}
