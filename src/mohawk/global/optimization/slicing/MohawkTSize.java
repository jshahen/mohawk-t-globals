package mohawk.global.optimization.slicing;

import mohawk.global.pieces.MohawkT;

public class MohawkTSize {
    public int numRoles;
    public int numRules;
    public int numTimeslots;
    public int numAdminRoles;

    public MohawkTSize(int roles, int timeslots, int rules) {
        numRoles = roles;
        numRules = rules;
        numTimeslots = timeslots;
    }

    /** Creates a MohawkTSize instance for a Mohawk+T policy
     * 
     * @param policy */
    public MohawkTSize(MohawkT policy) {
        numRoles = policy.numberOfRoles();
        numTimeslots = policy.numberOfTimeslots();
        numRules = policy.numberOfRules();
        numAdminRoles = policy.roleHelper.getAdminRoles().size();
    }

    @Override
    public String toString() {
        return "Mohawk+TSize {Roles:" + numRoles + ", Timeslots: " + numTimeslots + ", Rules: " + numRules
                + ", Admin-Roles: " + numAdminRoles + "}";
    }
}
