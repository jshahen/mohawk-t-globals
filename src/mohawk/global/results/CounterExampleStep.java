package mohawk.global.results;

public class CounterExampleStep {
    public static boolean useShortString = true;
    public static int shortStringVersion = 2;

    public String adminUser = "TRUE";
    public String targetUser = "{ERROR! Target User not Provided}";
    public String rule = "{ERROR! Rule not Provided}";

    public CounterExampleStep(String rule, String targetUser, String adminUser) {
        this.rule = rule;
        this.targetUser = targetUser;
        this.adminUser = adminUser;
    }

    public String getLongString() {
        return "{Rule: " + rule + "; Admin User[" + adminUser + "] -> Target User[" + targetUser + "]}";
    }

    public String getShortString() {
        switch (shortStringVersion) {
            default :
            case 1 :
                return "a" + adminUser + " -" + rule + "> T" + targetUser;
            case 2 :
                return rule + "[a" + adminUser + ", T" + targetUser + "]";
        }
    }

    @Override
    public String toString() {
        if (useShortString) { return getShortString(); }
        return getLongString();
    }
}
