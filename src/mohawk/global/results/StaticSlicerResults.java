package mohawk.global.results;

import java.io.*;
import java.util.ArrayList;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

/** @author Jonathan Shahen */
public class StaticSlicerResults {
    public final static Logger logger = Logger.getLogger("mohawk");
    public ArrayList<StaticSlicerResult> history = new ArrayList<StaticSlicerResult>();
    private StaticSlicerResult lastTestResult = null;

    public String[] heading = new String[]{
            // Default Data
            "File", "Datetime", "Comments",
            // Original Policy Specific Data
            "|Orig. Rules|", "|Orig. Roles|", "|Orig. Timeslots|", "|Orig. Admin Roles|",
            // Sliced Policy Specific Data
            "|Sliced Rules|", "|Sliced Roles|", "|Sliced Timeslots|", "|Sliced Admin Roles|",
            // Statistics on the decrease or lack of decrease (should never be an increase)
            "Diff. Rules", "Diff. Roles", "Diff. Timeslots", "Diff. Admin Roles",
            // Statistics on the decrease or lack of decrease (should never be an increase)
            "% Improv. Rules", "% Improv. Roles", "% Improv. Timeslots", "% Improv. Admin Roles",
            // Timing and incremental improvements
            "Slice 01", "Timing Slice 01 (ms)", "Slice 02", "Timing Slice 02 (ms)", "Final Timing (ms)"};

    public Boolean writeOutSingle(FileWriter fw, StaticSlicerResult result) throws IOException {
        if (result == null) { return false; }

        fw.write(StringEscapeUtils.escapeCsv(result._filename) + "," + //
                StringEscapeUtils.escapeCsv(result._datetime) + "," + //
                StringEscapeUtils.escapeCsv(result._comment) + "," + //
                result._numOrigRules + "," + //
                result._numOrigRoles + "," + //
                result._numOrigTimeslots + "," + //
                result._numOrigAdminRoles + "," + //
                result._numSlicedRules + "," + //
                result._numSlicedRoles + "," + //
                result._numSlicedTimeslots + "," + //
                result._numSlicedAdminRoles + "," + //
                result._diffRules + "," + //
                result._diffRoles + "," + //
                result._diffTimeslots + "," + //
                result._diffAdminRoles + "," + //
                result._percentRules + "," + //
                result._percentRoles + "," + //
                result._percentTimeslots + "," + //
                result._percentAdminRoles + "," + //
                StringEscapeUtils.escapeCsv(result._slice01Str) + "," + //
                result._timingSlice01 + "," + //
                StringEscapeUtils.escapeCsv(result._slice02Str) + "," + //
                result._timingSlice02 + "," + //
                result._totalTiming + "," + //
                "\n");

        fw.flush();// flushes just in-case this function is run once in a while!
        return true;
    }

    public StaticSlicerResult getLastResult() {
        return lastTestResult;
    }

    public void add(StaticSlicerResult result) {
        history.add(result);
        lastTestResult = result;
    }

    public Boolean writeOut(File results) throws IOException {
        return writeOut(results, false);
    }

    public Boolean writeOut(File results, Boolean forceOverwrite) throws IOException {
        FileWriter fw = getFileWriter(results, forceOverwrite);

        if (fw == null) {
            logger.warning("[StaticSlicer_RESULTS] Unable to create the filewriter");
            return false;
        }

        for (StaticSlicerResult result : history) {
            if (writeOutSingle(fw, result) == false) {
                logger.warning("[StaticSlicer_RESULTS] Unable to write the row: " + result.toString());
                return false;
            }
        }
        fw.close();

        return true;
    }

    public FileWriter getFileWriter(File results, Boolean forceOverwrite) throws IOException {
        Boolean writeHeader = forceOverwrite;
        if (!results.exists()) {
            if (!results.createNewFile()) {
                logger.warning("[StaticSlicer_RESULTS] Unable to create the file: " + results.getAbsolutePath());
                return null;
            }
            writeHeader = true;
        }

        if (!results.isFile()) {
            logger.warning("[StaticSlicer_RESULTS] Parameter pointing to something that is not a file: "
                    + results.getAbsolutePath());
            return null;
        }

        FileWriter fw = new FileWriter(results, !forceOverwrite);

        if (writeHeader) {
            writeHeader(fw);
        }
        return fw;
    }

    public Boolean writeOutLast(FileWriter fw) throws IOException {
        return writeOutSingle(fw, lastTestResult);
    }

    public void writeHeader(FileWriter fw) throws IOException {
        fw.write(StringUtils.join(heading, ",") + "\n");
    }

    @Override
    public String toString() {
        StringBuilder t = new StringBuilder();
        int i = 0;
        for (StaticSlicerResult result : history) {
            if (i != 0) {
                t.append("\n");
            }
            i++;
            t.append(result.toString());
        }
        return t.toString();
    }
}
