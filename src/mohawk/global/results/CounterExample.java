package mohawk.global.results;

import java.util.ArrayList;

public class CounterExample {
    public ArrayList<CounterExampleStep> steps = new ArrayList<>();
    public boolean goalReached = true;

    public void addStep(CounterExampleStep step) {
        steps.add(step);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("Initial State -> ");

        for (CounterExampleStep ces : steps) {
            sb.append(ces.toString()).append(" -> ");
        }

        if (goalReached) {
            sb.append("Goal State");
        } else {
            sb.append("Goal State is Unreachable");
        }
        return sb.toString();
    }
}
