package mohawk.global.results;

import java.io.*;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

/** @author Jonathan Shahen */
public class DiameterResults {
    public final static Logger logger = Logger.getLogger("mohawk");
    public ArrayList<DiameterResult> history = new ArrayList<DiameterResult>();
    private DiameterResult lastTestResult = null;
    /** Sets the precision on the results file for the diameters in scientific notation */
    public int diameterPrecision = 4;

    /** Formats 2^x into scientific notation with {@code scale} number of digits
     * 
     * @param x
     * @param scale
     * @return formatted scientific notation string of 2^{x} */
    public static String format(Integer x, int scale) {
        if (x == null) { return "null"; }

        // REQUIRED!! BigInteger takes forever if given a large power!
        if (DiameterResult.isTooLarge(x)) { return "Too Large"; }

        BigInteger two = new BigInteger("2");
        two = two.pow(x);

        NumberFormat formatter = new DecimalFormat("0.0E0");
        formatter.setRoundingMode(RoundingMode.HALF_UP);
        formatter.setMinimumFractionDigits(scale);
        return formatter.format(two);
    }

    public String[] heading = new String[]{
            // Default Data
            "File", "Datetime", "Comments",
            // Policy Specific Data
            "|Rules|", "|Roles|", "|Timeslots|", "|Admin Roles|",
            // Diameter and timings
            "Initial Diameter", "Initial Diameter STR", "Diameter T01", "Diameter T01 STR", "Timing 01", "Diameter T02",
            "Diameter T02 STR", "Timing 02", "Diameter T03", "Diameter T03 STR", "Timing 03", "Final Timing"};

    public Boolean writeOutSingle(FileWriter fw, DiameterResult result) throws IOException {
        if (result == null) { return false; }

        fw.write(StringEscapeUtils.escapeCsv(result._filename) + "," + //
                StringEscapeUtils.escapeCsv(result._datetime) + "," + //
                StringEscapeUtils.escapeCsv(result._comment) + "," + //
                result._numRules + "," + //
                result._numRoles + "," + //
                result._numTimeslots + "," + //
                result._numAdminRoles + "," + //
                format(result._diameterInitial, diameterPrecision) + "," + //
                StringEscapeUtils.escapeCsv(result._diameterInitialStr) + "," + //
                format(result._diameterTightening01, diameterPrecision) + "," + //
                StringEscapeUtils.escapeCsv(result._diameterTightening01Str) + "," + //
                result._timingTightening01 + "," + //
                format(result._diameterTightening02, diameterPrecision) + "," + //
                StringEscapeUtils.escapeCsv(result._diameterTightening02Str) + "," + //
                result._timingTightening02 + "," + //
                format(result._diameterTightening03, diameterPrecision) + "," + //
                StringEscapeUtils.escapeCsv(result._diameterTightening03Str) + "," + //
                result._timingTightening03 + "," + //
                result._totalTiming + "," + //
                "\n");

        fw.flush();// flushes just in-case this function is run once in a while!
        return true;
    }

    public DiameterResult getLastResult() {
        return lastTestResult;
    }

    public void add(DiameterResult result) {
        history.add(result);
        lastTestResult = result;
    }

    public Boolean writeOut(File results) throws IOException {
        return writeOut(results, false);
    }

    public Boolean writeOut(File results, Boolean forceOverwrite) throws IOException {
        FileWriter fw = getFileWriter(results, forceOverwrite);

        if (fw == null) {
            logger.warning("[DIAMETER_RESULTS] Unable to create the filewriter");
            return false;
        }

        for (DiameterResult result : history) {
            if (writeOutSingle(fw, result) == false) {
                logger.warning("[DIAMETER_RESULTS] Unable to write the row: " + result.toString());
                return false;
            }
        }
        fw.close();

        return true;
    }

    public FileWriter getFileWriter(File results, Boolean forceOverwrite) throws IOException {
        Boolean writeHeader = forceOverwrite;
        if (!results.exists()) {
            if (!results.createNewFile()) {
                logger.warning("[DIAMETER_RESULTS] Unable to create the file: " + results.getAbsolutePath());
                return null;
            }
            writeHeader = true;
        }

        if (!results.isFile()) {
            logger.warning("[DIAMETER_RESULTS] Parameter pointing to something that is not a file: "
                    + results.getAbsolutePath());
            return null;
        }

        FileWriter fw = new FileWriter(results, !forceOverwrite);

        if (writeHeader) {
            writeHeader(fw);
        }
        return fw;
    }

    public Boolean writeOutLast(FileWriter fw) throws IOException {
        return writeOutSingle(fw, lastTestResult);
    }

    public void writeHeader(FileWriter fw) throws IOException {
        fw.write(StringUtils.join(heading, ",") + "\n");
    }

    @Override
    public String toString() {
        StringBuilder t = new StringBuilder();
        int i = 0;
        for (DiameterResult result : history) {
            if (i != 0) {
                t.append("\n");
            }
            i++;
            t.append(result.toString());
        }
        return t.toString();
    }
}
