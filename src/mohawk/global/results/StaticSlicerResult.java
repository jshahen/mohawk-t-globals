package mohawk.global.results;

import java.io.File;
import java.time.LocalDateTime;

import mohawk.global.optimization.slicing.MohawkTSize;

public class StaticSlicerResult {
    /** The name of the file that was passed to the solver */
    public String _filename;
    /** Any comments that should be attached to this testing result */
    public String _comment;
    /** the date and time that the testing result was created */
    public String _datetime;

    public Integer _numOrigRules;
    public Integer _numOrigRoles;
    public Integer _numOrigTimeslots;
    public Integer _numOrigAdminRoles;

    public Integer _numSlicedRules;
    public Integer _numSlicedRoles;
    public Integer _numSlicedTimeslots;
    public Integer _numSlicedAdminRoles;

    // Procedurally generated values
    public int _diffRules;
    public int _diffRoles;
    public int _diffTimeslots;
    public int _diffAdminRoles;
    public float _percentRules = 0;
    public float _percentRoles = 0;
    public float _percentTimeslots = 0;
    public float _percentAdminRoles = 0;

    // Set by each of the slicing functions
    public String _slice01Str;
    public String _slice02Str;

    // Timing for each of the slicing functions
    public Long _timingSlice01;
    public Long _timingSlice02;
    public Long _totalTiming;

    public StaticSlicerResult(String filename, String comment) {
        _filename = filename;
        _comment = comment;
        _datetime = LocalDateTime.now().toString();
    }

    public StaticSlicerResult(File specFile, String comment) {
        this(specFile.getAbsolutePath(), comment);
    }

    public void addSizes(MohawkTSize orig, MohawkTSize sliced) {
        _numOrigRoles = orig.numRoles;
        _numOrigRules = orig.numRules;
        _numOrigTimeslots = orig.numTimeslots;
        _numOrigAdminRoles = orig.numAdminRoles;

        _numSlicedRoles = sliced.numRoles;
        _numSlicedRules = sliced.numRules;
        _numSlicedTimeslots = sliced.numTimeslots;
        _numSlicedAdminRoles = sliced.numAdminRoles;

        _diffRoles = _numOrigRoles - _numSlicedRoles;
        _diffRules = _numOrigRules - _numSlicedRules;
        _diffAdminRoles = _numOrigAdminRoles - _numSlicedAdminRoles;
        _diffTimeslots = _numOrigTimeslots - _numSlicedTimeslots;

        // Protected against division by zero
        if (!_numOrigRules.equals(0)) {
            _percentRules = ((float) _diffRules / _numOrigRules) * 100;
        }
        if (!_numOrigRoles.equals(0)) {
            _percentRoles = ((float) _diffRoles / _numOrigRoles) * 100;
        }
        if (!_numOrigTimeslots.equals(0)) {
            _percentTimeslots = ((float) _diffTimeslots / _numOrigTimeslots) * 100;
        }
        if (!_numOrigAdminRoles.equals(0)) {
            _percentAdminRoles = ((float) _diffAdminRoles / _numOrigAdminRoles) * 100;
        }
    }

    @Override
    public String toString() {
        return "StaticSlicerResult {" +//
                "File: " + _filename + //
                "; Date: " + _datetime + //
                "; Rules: " + _numOrigRules + " - " + _numSlicedRules + " = " + _diffRules +//
                "; Roles: " + _numOrigRoles + " - " + _numSlicedRoles + " = " + _diffRoles + //
                "; Timelsots: " + _numOrigTimeslots + " - " + _numSlicedTimeslots + " = " + _diffTimeslots + //
                "; AdminRoles: " + _numOrigAdminRoles + " - " + _numSlicedAdminRoles + " = " + _diffAdminRoles + "}";
    }
}
