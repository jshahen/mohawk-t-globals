#Mohawk-T Globals 

####Table of Contents

1. [Overview](#Overview)
1. [Setup](#Setup)
    1. What this affects
    1. Setup Requirements
    1. How to Start
1. [Usage - Configuration options and additional functionality](#Usage)
1. [Reference - An under-the-hood peek at what the module is doing and how](#Reference)

##Overview

This repository contains shared Java files that Mohawk+T repositories require to run.

##Setup

###What this affects

* This repository cannot be run, and only contains overlapping code that is used by other repositories

###Setup Requirements

* Java 1.7+
* __Folder name needs to be "Mohawk-T Globals"__ and in at the same folder level as the other repositories or the other repositories cannot find where their code is a
* (Optional) Eclipse (allows for any folder name and very easy building/running)
	
###How to Start

All of the other repositories are setup to be linked to the this repository. If you have a problem linking the codes together to build any of the application please use Eclipse as it is much easier to 

##Usage

Put the classes, types, and resources for customizing, configuring, and doing the fancy stuff with your module here. 

##Reference

* Maintainers: Jonathan Shahen < jmshahen [AT] UWATERLOO [DOT] CA >
* License: Please see the file called LICENSE
* Documentation: Please run javadoc on this project to get the documentation

###Classes
* **mohawk.global**
    * **comparator**
        * AlphanumericComparator.java
    * **formatter**
        * MohawkConsoleFormatter.java
        * MohawkCSVFileFormatter.java
    * **generators**
        * ASAPTimeSARandomGenerator.java
        * MohawkRandomGenerator.java
        * MohawkTRandomGenerator.java
    * **helper**
        * FileHelper.java
        * RoleHelper.java
        * TimeIntervalHelper.java
    * **pieces**
        * **reduced**
            * **query**
                * ASAPTimeNSA_Query.java
                * ASAPTimeSA_Query.java
                * Mohawk_Query.java
                * TRole_Query.java
            * **rules**
                * ASAPTimeNSA_Rule.java
                * ASAPTimeSA_Rule.java
                * MohawkCA_Rule.java
                * MohawkCR_Rule.java
                * TRole_Rule.java
                * TRule_Rule.java
            * ASAPTimeSA.java
        * CanAssign.java
        * CanBlock.java
        * CanDisable.java
        * CanEnable.java
        * CanRevoke.java
        * ExpectedResult.java
        * LimitedRole.java
        * MohawkT.java
        * Query.java
        * Role.java
        * Rule.java
        * RuleType.java
        * TimeInterval.java
        * TimeIntervalSplit.java
        * TimeSlot.java
    * **results**
        * ExecutionResult.java
        * MohawkResults.java
        * TestingResult.java
    * **testing**
        * DebugMsg.java
        * RoleHelperTests.java
        * TimeIntervalHelperRegressionTests.java
    * **timing**
        * MohawkTiming.java
        * TimingEvent.java
    * FileExtensions.java 
